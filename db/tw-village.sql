-- Adminer 4.8.1 MySQL 5.7.34 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `audit_log`;
CREATE TABLE `audit_log` (
  `audit_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `village_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `before` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `after` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`audit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `dataset`;
CREATE TABLE `dataset` (
  `dataset_name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `village_code` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `village_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `village_code_alias` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `village_code` (`village_code`),
  KEY `parent_code` (`town_code`),
  KEY `parent_name` (`town_name`),
  KEY `village_code_alias` (`village_code_alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `dataset_list`;
CREATE TABLE `dataset_list` (
  `dataset_name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source_url` text COLLATE utf8mb4_unicode_ci,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `logging`;
CREATE TABLE `logging` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `short_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `last_error` text COLLATE utf8mb4_unicode_ci,
  `action` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `short_name_name_entity` (`short_name`,`name`,`entity`(200))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `main_tb`;
CREATE TABLE `main_tb` (
  `town_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `village_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `village_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `village_name_alias` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `wikidata_id` bigint(20) unsigned DEFAULT NULL,
  `wikidata_id_candidate` bigint(20) unsigned DEFAULT NULL,
  `added_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `village_code` (`village_code`),
  KEY `parent_code` (`town_code`),
  KEY `parent_name` (`town_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `towncode`;
CREATE TABLE `towncode` (
  `town_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `town_code01` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `parent_code` (`town_code`),
  KEY `parent_name` (`town_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `town_and_village_test`;
CREATE TABLE `town_and_village_test` (
  `town_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `village_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `village_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wikidata_id` bigint(20) unsigned DEFAULT NULL,
  `wikidata_id_candidate` bigint(20) unsigned DEFAULT NULL,
  `added_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_error` text COLLATE utf8mb4_unicode_ci,
  UNIQUE KEY `village_code` (`village_code`),
  KEY `parent_code` (`town_code`),
  KEY `parent_name` (`town_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `wikidata_entry`;
CREATE TABLE `wikidata_entry` (
  `wikidata_id` bigint(20) unsigned NOT NULL COMMENT 'without Q',
  `labels` text COLLATE utf8mb4_unicode_ci,
  `descriptions` text COLLATE utf8mb4_unicode_ci,
  `search_snippet` text COLLATE utf8mb4_unicode_ci,
  `osm_id` text COLLATE utf8mb4_unicode_ci COMMENT 'openstreetmap ID',
  `coordinate` text COLLATE utf8mb4_unicode_ci,
  `url` text COLLATE utf8mb4_unicode_ci,
  `wikidata_created_at` timestamp NULL DEFAULT NULL,
  `wikidata_created_at_raw` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'api: cirrusbuilddoc. create_timestamp',
  `wikidata_updated_at` timestamp NULL DEFAULT NULL,
  `wikidata_updated_at_raw` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'api: cirrusbuilddoc.timestamp',
  `is_village` tinyint(2) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `wikidata_id` (`wikidata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `wikidata_search`;
CREATE TABLE `wikidata_search` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `village_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wikidata_result` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `wikidata_totalhits` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyword` (`keyword`),
  KEY `village_code` (`village_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `wikidata_village`;
CREATE TABLE `wikidata_village` (
  `wikidata_id` bigint(20) unsigned NOT NULL COMMENT 'without Q',
  `town_wikidata_qid` text COLLATE utf8mb4_unicode_ci,
  `town_labels` text COLLATE utf8mb4_unicode_ci,
  `town_descriptions` text COLLATE utf8mb4_unicode_ci,
  `labels` text COLLATE utf8mb4_unicode_ci,
  `descriptions` text COLLATE utf8mb4_unicode_ci,
  `village_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '戶役政資訊系統資料代碼',
  `osm_id` text COLLATE utf8mb4_unicode_ci COMMENT 'openstreetmap ID',
  `coordinate` text COLLATE utf8mb4_unicode_ci,
  `search_snippet` text COLLATE utf8mb4_unicode_ci,
  `url` text COLLATE utf8mb4_unicode_ci,
  `wikidata_created_at` timestamp NULL DEFAULT NULL,
  `wikidata_created_at_raw` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'api: cirrusbuilddoc. create_timestamp',
  `wikidata_updated_at` timestamp NULL DEFAULT NULL,
  `wikidata_updated_at_raw` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'api: cirrusbuilddoc.timestamp',
  `is_village` tinyint(2) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `wikidata_id` (`wikidata_id`),
  KEY `village_code` (`village_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2021-12-31 09:32:57
