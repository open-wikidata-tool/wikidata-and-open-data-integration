-- Adminer 4.8.1 MySQL 5.7.34 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `wikidata_village`;
CREATE TABLE `wikidata_village` (
  `wikidata_id` bigint(20) unsigned NOT NULL COMMENT 'without Q',
  `town_wikidata_qid` text COLLATE utf8mb4_unicode_ci,
  `town_labels` text COLLATE utf8mb4_unicode_ci,
  `town_descriptions` text COLLATE utf8mb4_unicode_ci,
  `labels` text COLLATE utf8mb4_unicode_ci,
  `descriptions` text COLLATE utf8mb4_unicode_ci,
  `village_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '戶役政資訊系統資料代碼',
  `osm_id` text COLLATE utf8mb4_unicode_ci COMMENT 'openstreetmap ID',
  `coordinate` text COLLATE utf8mb4_unicode_ci,
  `search_snippet` text COLLATE utf8mb4_unicode_ci,
  `url` text COLLATE utf8mb4_unicode_ci,
  `wikidata_created_at` timestamp NULL DEFAULT NULL,
  `wikidata_created_at_raw` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'api: cirrusbuilddoc. create_timestamp',
  `wikidata_updated_at` timestamp NULL DEFAULT NULL,
  `wikidata_updated_at_raw` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'api: cirrusbuilddoc.timestamp',
  `is_village` tinyint(2) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `wikidata_id` (`wikidata_id`),
  KEY `village_code` (`village_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2021-12-31 09:32:53
