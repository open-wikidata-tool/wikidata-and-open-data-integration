-- Adminer 4.8.1 MySQL 5.7.34 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `dataset`;
CREATE TABLE `dataset` (
  `dataset_name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `village_code` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `village_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `village_code_alias` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `village_code` (`village_code`),
  KEY `parent_code` (`town_code`),
  KEY `parent_name` (`town_name`),
  KEY `village_code_alias` (`village_code_alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2021-12-26 04:38:57
