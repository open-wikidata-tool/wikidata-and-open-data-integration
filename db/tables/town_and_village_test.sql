-- Adminer 4.8.1 MySQL 5.7.34 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `town_and_village_test`;
CREATE TABLE `town_and_village_test` (
  `town_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `village_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `village_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wikidata_id` bigint(20) unsigned DEFAULT NULL,
  `wikidata_id_candidate` bigint(20) unsigned DEFAULT NULL,
  `added_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_error` text COLLATE utf8mb4_unicode_ci,
  UNIQUE KEY `village_code` (`village_code`),
  KEY `parent_code` (`town_code`),
  KEY `parent_name` (`town_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `town_and_village_test` (`town_code`, `town_name`, `village_code`, `village_name`, `wikidata_id`, `wikidata_id_candidate`, `added_at`, `deleted_at`, `created_at`, `updated_at`, `last_error`) VALUES
('09007010',	'福建省連江縣南竿鄉',	'09007010003',	'福沃村',	NULL,	21449461,	NULL,	NULL,	'2021-07-07 06:22:50',	'2021-11-02 13:10:36',	NULL),
('10001150',	'臺灣省臺北縣五股鄉',	'10001150020',	'臺北縣五股鄉福德村',	NULL,	NULL,	'2005-11-30 16:00:00',	NULL,	'2021-11-14 09:52:47',	'2021-11-14 10:22:15',	NULL),
(NULL,	NULL,	'empty',	'',	NULL,	NULL,	NULL,	NULL,	NULL,	'2021-11-18 14:16:49',	NULL);

-- 2021-11-18 14:26:48
