<?php

date_default_timezone_set("Asia/Taipei");

$file_path = __DIR__ . '/config.php';
if(file_exists($file_path)){
    require_once $file_path;
}

require_once __DIR__ . '/scripts/CrawlerClass.php';
require_once __DIR__ . '/scripts/DbClass.php';
require_once __DIR__ . '/scripts/ReaderClass.php';
require_once __DIR__ . '/scripts/WikidataClass.php';
require_once __DIR__ . '/scripts/OSMClass.php';
require_once __DIR__ . '/scripts/SchoolClass.php';
require_once __DIR__ . '/scripts/TimeClass.php';

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/vendor/simplehtmldom/simplehtmldom/simple_html_dom.php';
