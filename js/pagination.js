$('#page_size').change(function() {
    var debug = false;
    //var debug = true;
    selected_size = $(this).val();
    if(debug){
        console.log($(this).val());
    }

    var url = window.location.href;
    if(debug){
        console.log(url);
    }

    let uri = new URL(url);
    let params = new URLSearchParams(uri.search);
    if(debug){
        //console.log(params);
        console.log("params: " + params.toString());
    }
    params.set('size', selected_size);
    
    // Delete the page_number parameter.
    params.delete('page_number');
    if(debug){
        //console.log(params);
        console.log("params: " + params.toString());
    }
    
    var path = url.split('?')[0];
    var new_url = path + "?" + params.toString();
    if(debug){
        console.log("path: " + path);
        console.log("new_url: " + new_url);
    }

    window.location = new_url;

});

$('a.pagination').click(function() {
    var debug = false;
    //var debug = true;
    var target_page_number = $(this).data("page");
    //var target_offset = $(this).data("offset");
    if(debug){
        console.log("target_page_number: " + target_page_number);
        //console.log("target_offset: " + target_offset);
    }

    var url = window.location.href;
    if(debug){
        console.log(url);
    }

    let uri = new URL(url);
    let params = new URLSearchParams(uri.search);
    if(debug){
        //console.log(params);
        console.log("params: " + params.toString());
    }
    params.set('page_number', target_page_number);

    if(debug){
        //console.log(params);
        console.log("params: " + params.toString());
    }

    var path = url.split('?')[0];
    var new_url = path + "?" + params.toString();
    if(debug){
        console.log("path: " + path);
        console.log("new_url: " + new_url);
    }

    window.location = new_url;
});
