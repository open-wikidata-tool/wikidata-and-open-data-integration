function setUrlParameter(parameter_name, new_parameter_value){
    var debug = false;
    //var debug = true;

    var url = window.location.href;
    if(debug){
        console.log("url: " + url);
    }

    const queryString = window.location.search;
    if(debug){
        console.log("queryString: " + queryString);
    }

    // get parameter from Url
    //var pattern = "/[?&]" + parameter_name + "=([^&]*)/";
    var pattern = "[?&]" + parameter_name + "=([^&]*)";
    var parameter_elements = url.match(pattern);
    if(debug){
        console.log("pattern: " + pattern);
        console.log("parameter_elements: ");
        console.log(parameter_elements);
    }

    if (parameter_elements != null)
    {
        var current_parameter_value = url.match(pattern)[1];
        if(debug){
            console.log(current_parameter_value);
        }

    }

    var new_url;
    if(queryString === ""){
        new_url = "?" + parameter_name + "=" + new_parameter_value;
    }else if(queryString !== "" && parameter_elements === null
    ){
        new_url = "&" + parameter_name + "=" + new_parameter_value;
    }else{
        new_url = url.replace(parameter_name + "=" + current_parameter_value, parameter_name + "=" + new_parameter_value);
    }

    if(debug){
        console.log("new_url: " + new_url);
    }
    return new_url;
}

/**
 *
 * @param url
 * @param parameter_name
 * @returns {null|*}
 */
function getUrlParameter(url, parameter_name){
    var debug = false;
    //var debug = true;

    //var pattern = "/[?&]" + parameter_name + "=([^&]*)/";
    var pattern = "[?&]" + parameter_name + "=([^&]*)";
    var parameter_elements = url.match(pattern);
    if(debug){
        console.log(pattern);
        console.log(parameter_elements);
    }

    if (parameter_elements != null)
    {
        var current_parameter_value = url.match(pattern)[1];
        if(debug){
            console.log(current_parameter_value);
        }
        return current_parameter_value;
    }

    return null;
}