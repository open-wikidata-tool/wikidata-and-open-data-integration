$( document ).ready(function() {
    var debug = false;
    //var debug = true;

    //console.log( "ready!" );


    // change the icon of sorting column
    var url = window.location.href;

    var keyword = getUrlParameter(url, "q");
    if(debug){
        console.log("keyword: " + keyword);
    }

    if(keyword
    ){
        keyword = decodeURI(keyword);
        $('#search-input').val(keyword);

    }

});

$('#search-button').click(function() {
    var debug = false;
    //var debug = true;
    var keyword = $('#search-input').val();
    searchKeyword(keyword)

});

$('#search-input').keydown(function(event){
    if (event.which == 13){
        //body or action to be performed
        var keyword = $('#search-input').val();
        searchKeyword(keyword)
    }
});

function searchKeyword(keyword){
    var debug = false;
    //var debug = true;
    if (!keyword || keyword.trim() === "" || (keyword.trim()).length === 0) {
        return;
    }

    new_url = setUrlParameterForSearch(keyword);
    if(debug){
        console.log("new_url: " + new_url);
    }

    window.location = new_url;
}

function setUrlParameterForSearch(keyword){
    var debug = false;
    //var debug = true;

    var url = window.location.href;
    if(debug){
        console.log(url);
    }

    let uri = new URL(url);
    let params = new URLSearchParams(uri.search);
    if(debug){
        //console.log(params);
        console.log("params: " + params.toString());
    }
    params.set('q', keyword);

    // Delete the page_number parameter.
    params.delete('page_number');
    if(debug){
        //console.log(params);
        console.log("params: " + params.toString());
    }

    var path = url.split('?')[0];
    var new_url = path + "?" + params.toString();
    if(debug){
        console.log("path: " + path);
        console.log("new_url: " + new_url);
    }

   return new_url;
}