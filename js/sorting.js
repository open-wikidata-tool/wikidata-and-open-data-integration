$( document ).ready(function() {
    var debug = false;
    //var debug = true;

    // change the icon of sorting column
    var url = window.location.href;
    var current_sort_column_from_url = getUrlParameter(url, "sort");
    var current_sort_order_from_url = getUrlParameter(url, "sort_order");
    if(debug){
        console.log("current_sort_column_from_url: " + current_sort_column_from_url);
        console.log("current_sort_order_from_url: " + current_sort_order_from_url);
    }

    if(current_sort_column_from_url
        && current_sort_order_from_url
        && (current_sort_order_from_url === "desc" || current_sort_order_from_url === "asc")
    ){
        current_sort_column_from_url = decodeURI(current_sort_column_from_url);
        $('div.sortable').filter('[data-sort="'+ current_sort_column_from_url + '"]').removeClass('both').addClass(current_sort_order_from_url);
        $('div.sortable').filter('[data-sort="'+ current_sort_column_from_url + '"]').removeClass('desc').addClass(current_sort_order_from_url);
        $('div.sortable').filter('[data-sort="'+ current_sort_column_from_url + '"]').removeClass('asc').addClass(current_sort_order_from_url);
        if(debug){
            console.log("after active: " + current_sort_column_from_url);
        }
    }


});

$('div.sortable').click(function() {
    var debug = false;
    //var debug = true;

    // todo: get parameters from url
    var url = window.location.href;
    if(debug){
        console.log(url);
    }
    var current_sort_column_from_url = getUrlParameter(url, "sort");
    var current_sort_order_from_url = getUrlParameter(url, "sort_order");
    if(debug){
        console.log("current_sort_column_from_url: " + current_sort_column_from_url);
        console.log("current_sort_order_from_url: " + current_sort_order_from_url);
    }

    var selected_sort_column = $(this).data("sort");
    var current_sort_order = $(this).prop("class");
    if(debug){
        console.log("selected_sort_column: " + selected_sort_column);
        console.log("current_sort_order: " + current_sort_order);
    }

    switch (current_sort_order) {
        case "sortable both":
        case "sortable asc":
            var target_class = "sortable desc";
            var target_sort_order = "desc";
            break;
        case "sortable desc":
            var target_class = "sortable asc";
            var target_sort_order = "asc";
            break;
    }

    if(debug){
        console.log("target_class: " + target_class);
        console.log("target_sort_order: " + target_sort_order);
    }




    let uri = new URL(url);
    let params = new URLSearchParams(uri.search);
    if(debug){
        //console.log(params);
        console.log("params: " + params.toString());
    }
    params.set('sort', selected_sort_column);
    params.set('sort_order', target_sort_order);

    // Delete the page_number parameter.
    params.delete('page_number');
    if(debug){
        //console.log(params);
        console.log("params: " + params.toString());
    }
    
    var path = url.split('?')[0];
    var new_url = path + "?" + params.toString();
    if(debug){
        console.log("path: " + path);
        console.log("new_url: " + new_url);
    }

    window.location = new_url;

});

$('a.pagination').click(function() {
    var debug = false;
    //var debug = true;
    var target_page_number = $(this).data("page");
    //var target_offset = $(this).data("offset");
    if(debug){
        console.log("target_page_number: " + target_page_number);
        //console.log("target_offset: " + target_offset);
    }

    var url = window.location.href;
    if(debug){
        console.log(url);
    }

    let uri = new URL(url);
    let params = new URLSearchParams(uri.search);
    if(debug){
        //console.log(params);
        console.log("params: " + params.toString());
    }
    params.set('page_number', target_page_number);

    if(debug){
        //console.log(params);
        console.log("params: " + params.toString());
    }

    var path = url.split('?')[0];
    var new_url = path + "?" + params.toString();
    if(debug){
        console.log("path: " + path);
        console.log("new_url: " + new_url);
    }

    window.location = new_url;
});
