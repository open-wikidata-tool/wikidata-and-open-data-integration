<?php
?>

<div class="container">
    <br />
    <hr />
    Powered by
        <a href="https://query.wikidata.org/">Wikidata Query Service</a>,
        <a href="https://www.ris.gov.tw/documents/html/5/1/167.html">政府公開資料</a>
    <div>Icons made by <a href="https://www.flaticon.com/authors/iconjam" title="Iconjam">Iconjam</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="js/commons.min.js"></script>
<script src="js/pagination.min.js"></script>
<script src="js/sorting.min.js"></script>
<script src="js/search.min.js"></script>
