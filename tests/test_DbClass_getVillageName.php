<?php

$debug = true;

//---
require_once __DIR__ . '/../load.php';

$db_go = new DbClass();
$db_go->debug = $debug;
$db_go->target_tb = 'town_and_village_test';

$village_code = '09007010003';
$village_name = $db_go->getVillageNameGivenVillageCode($village_code);
var_dump($village_name);

$village_code = '404';
$village_name = $db_go->getVillageNameGivenVillageCode($village_code);
var_dump($village_name);

$village_code = 'empty';
$village_name = $db_go->getVillageNameGivenVillageCode($village_code);
var_dump($village_name);

