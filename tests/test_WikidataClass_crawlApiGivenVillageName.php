<?php

$debug = true;
$enforce_update = true;
$city_name = '臺南市柳營區';
$village_name = '重溪里';
$village_code = '67000040008';

/*$city_name = '臺南市新營區';
$village_name = '新北里';
$village_code = '67000010027';
*/
//---
require_once __DIR__ . '/../load.php';

$wikidata_go = new WikidataClass();
$wikidata_go->debug = $debug;
$wikidata_go->enforce_update_downloaded_file = $enforce_update;
$result = $wikidata_go->crawlApiGivenVillageName($city_name, $village_name, $village_code);
var_dump($result);

