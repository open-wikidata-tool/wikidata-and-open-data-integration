<?php
/**
 * @var array $config
 */

use Simplon\Mysql\Mysql;
use Simplon\Mysql\PDOConnector;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../config.php';

$config = getenv('DB_CONFIG');
$config = json_decode($config, true);
$pdoConnector = new PDOConnector($config['host'], $config['user'], $config['password'], $config['database']);
$pdoConn = $pdoConnector->connect('utf8mb4', ['port' => 8889]); // charset, options
$dbh = new Mysql($pdoConn);

// ############################################

echo "<h1>IN with integers</h1>";
$conds = ['ids' => [1, 2, 3, 4, 5]];
//$query = 'SELECT id, email FROM users WHERE id IN (:ids)';
$query = 'SELECT * FROM `main_tb`';
//var_dump($dbh->fetchRowMany($query, $conds));
var_dump($dbh->fetchRowMany($query));

// ############################################

echo "<h1>IN with strings</h1>";
$conds = ['emails' => ['tino@beatguide.me', 'marin@underplot.com']];
//$query = 'SELECT id, email FROM users WHERE email IN (:emails)';
$query = 'SELECT * FROM `main_tb` LIMIT 1';
//var_dump($dbh->fetchRowMany($query, $conds));
