<?php

require_once __DIR__ . '/../scripts/SchoolClass.php';
require_once __DIR__ . '/../scripts/ExcelClass.php';

use PHPUnit\Framework\TestCase;
use wikidata_tw\SchoolClass;

class SchoolClassTest extends TestCase
{

    private $myClass;

    protected function setUp(): void
    {
        $this->myClass = new SchoolClass();
    }

    public function test_getMaxCountOfFieldNumber(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;
        $file_path = __DIR__ . '/depart.moe.edu.tw/112_BasicChange.xlsx';
        $excel_content = (new ExcelClass())->getExcelContent($file_path);
        $actual = $this->myClass->getMaxCountOfFieldNumber($excel_content);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $expected = 4;
        $this->assertEquals($expected, $actual);
    }

    public function test_guessFieldIndex(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;
        $file_path = __DIR__ . '/depart.moe.edu.tw/112_BasicChange.xlsx';
        $excel_content = (new ExcelClass())->getExcelContent($file_path);
        $actual = $this->myClass->guessFieldIndex($excel_content);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $expected = 4;
        $this->assertEquals($expected, $actual);
    }

    public function test_combineFieldsAndValues(){
        $fields = array("name", "amount");
        $values = array("apple", 123);
        $actual = $this->myClass->combineFieldsAndValues($fields, $values);
        $actual_item = $actual["name"];
        $expected_item = "apple";
        $this->assertEquals($expected_item, $actual_item);
    }

    public function test_parseDiffExcelFile_of_element_school(){
        $debug = false;
        $this->myClass->debug = $debug;
        $file_path = __DIR__ . '/depart.moe.edu.tw/112_BasicChange.xlsx';
        $excel_content = (new ExcelClass())->getExcelContent($file_path);
        $actual = $this->myClass->parseDiffExcelFile($excel_content);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0]["學校代碼"];
        $expected_item = "013603";
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]["status"];
        $expected_item = "新設";
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]["created_at"];
        $expected_item = "2023-08-01";
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]["updated_at"];
        $expected_item = "2023-08-10";
        $this->assertEquals($expected_item, $actual_item);
    }

    public function test_parseDiffExcelFile_of_senior_school(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;
        $file_path = __DIR__ . '/depart.moe.edu.tw/112_HighChange.xlsx';
        $excel_content = (new ExcelClass())->getExcelContent($file_path);
        $actual = $this->myClass->parseDiffExcelFile($excel_content);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0]["學校代碼"];
        $expected_item = "110409";
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]["status"];
        $expected_item = "改制、改隸";
        $this->assertEquals($expected_item, $actual_item);
    }

    public function test_parseDiffExcelFile_of_university(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;
        $file_path = __DIR__ . '/depart.moe.edu.tw/112_HigherChange.xlsx';
        $excel_content = (new ExcelClass())->getExcelContent($file_path);
        $actual = $this->myClass->parseDiffExcelFile($excel_content);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0]["學校代碼"];
        $expected_item = "1185";
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]["status"];
        $expected_item = "變更校名 ( 1 所)";
        $this->assertEquals($expected_item, $actual_item);
    }

    public function test_removeReturnSymbolsFromElements(){
        $input = array("apple\n", "orange\n");
        $actual = $this->myClass->removeReturnSymbolsFromElements($input);
        $expected = array("apple", "orange");
        $this->assertEquals($expected, $actual);
    }

    public function test_getExcelFileUrlsFromHtmlPage(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;
        $file_path = __DIR__ . '/depart.moe.edu.tw/list_of_schools_and_changes_for_the_112th_academic_year.html';
        $html_content = file_get_contents($file_path);
        $actual = $this->myClass->getExcelFileUrlsFromHtmlPage($html_content);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0];
        $expected_item = "https://stats.moe.gov.tw/files/school/112/112_BasicChange.xlsx";
        $this->assertEquals($expected_item, $actual_item);
    }

    public function test_parseSchoolListByYearPage(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;
        $file_path = __DIR__ . '/depart.moe.edu.tw/entrance.html';
        $html_content = file_get_contents($file_path);
        $actual = $this->myClass->parseSchoolListByYearPage($html_content);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0]["txt"];
        $expected_item = "112學年各級學校名錄及異動一覽表";
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]["href"];
        $expected_item = "https://depart.moe.edu.tw/ED4500/News_Content.aspx?n=63F5AB3D02A8BBAC&sms=1FF9979D10DBF9F3&s=E23C5A6CA17DB8E2";
        $this->assertEquals($expected_item, $actual_item);
    }

}

