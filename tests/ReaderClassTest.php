<?php

require_once __DIR__ . '/../scripts/ReaderClass.php';

use PHPUnit\Framework\TestCase;

class ReaderClassTest extends TestCase
{

    private $myClass;

    protected function setUp(): void
    {
        $this->myClass = new ReaderClass();
    }

    function test_clean(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;


        $input = <<<EOT

　

EOT;

        $input = trim($input);
        $actual = $this->myClass->clean($input);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $expected = '';
        $this->assertEquals($expected, $actual);
    }

    function test_getFileContentFromDocx(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;

        $file_path = __DIR__ . '/village_new_added/2013-12-01.docx';
        $actual = $this->myClass->getFileContentFromDocx($file_path);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $expected = '新增村里

南投縣仁愛鄉

10008130016,都達村';
        $this->assertEquals($expected, $actual);
    }




    function test_detectEncoding_where_big5_encoding(){
        $file_path = __DIR__ . '/village-code-big5.txt';
        $file_content = file_get_contents($file_path);
        $actual = $this->myClass->detectEncoding($file_content);
        $expected = 'BIG-5';
        $this->assertEquals($expected, $actual);
    }

    function test_detectEncoding_where_utf8_encoding(){
        $file_path = __DIR__ . '/village-code-utf8.txt';
        $file_content = file_get_contents($file_path);
        $actual = $this->myClass->detectEncoding($file_content);
        $expected = 'UTF-8';
        $this->assertEquals($expected, $actual);
    }

    function test_parseCsvFile_where_big5_encoding(){
        $file_path = __DIR__ . '/village-code-big5.txt';
        $file_content = file_get_contents($file_path);
        $actual = $this->myClass->parseCsvFile($file_content);
        $actual_item = $actual[0];
        $expected_item = array(
            '09007010001',
            '09007010',
            '介壽村'
        );
        $this->assertEquals($expected_item, $actual_item);
    }


    function test_parseCsvFile_where_utf8_encoding(){
        $file_path = __DIR__ . '/village-code-utf8.txt';
        $file_content = file_get_contents($file_path);
        $actual = $this->myClass->parseCsvFile($file_content);
        $actual_item = $actual[0];
        $expected_item = array(
            '09007010001',
            '09007010',
            '介壽村'
        );
        $this->assertEquals($expected_item, $actual_item);
    }

    function test_getFileList(){
        $debug = false;
        //$debug = true;

        $folder_path = __DIR__ . '/test_folder/';
        $actual = $this->myClass->getFileList($folder_path, null);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }

        $expected_item = __DIR__ . '/test_folder/dummy.docx';
        $this->assertContains($expected_item, $actual);

        $expected_item = __DIR__ . '/test_folder/dummy.docx.bak';
        $this->assertContains($expected_item, $actual);

        $expected_item = __DIR__ . '/test_folder/dummy.txt';
        $this->assertContains($expected_item, $actual);

        $folder_path = __DIR__ . '/test_folder/';
        $pattern = "/^.*\.(txt|docx)$/i";
        $actual = $this->myClass->getFileList($folder_path, $pattern);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }

        $expected_item = __DIR__ . '/test_folder/dummy.docx';
        $this->assertContains($expected_item, $actual);

        $expected_item = __DIR__ . '/test_folder/dummy.docx.bak';
        $this->assertNotContains($expected_item, $actual);

        $expected_item = __DIR__ . '/test_folder/dummy.txt';
        $this->assertContains($expected_item, $actual);

        $folder_path = __DIR__ . '/test_folder/';
        $pattern = "/^.*\.(txt)$/i";
        $actual = $this->myClass->getFileList($folder_path, $pattern);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }

        $expected_item = __DIR__ . '/test_folder/dummy.docx';
        $this->assertNotContains($expected_item, $actual);

        $expected_item = __DIR__ . '/test_folder/dummy.docx.bak';
        $this->assertNotContains($expected_item, $actual);

        $expected_item = __DIR__ . '/test_folder/dummy.txt';
        $this->assertContains($expected_item, $actual);
    }

}
