<?php

require_once __DIR__ . '/../scripts/CrawlerClass.php';

use PHPUnit\Framework\TestCase;

class CrawlerClassTest extends TestCase
{

    private $myClass;

    protected function setUp(): void
    {
        $this->myClass = new CrawlerClass();
    }

    function test_parseParentCodeFile(){
        $this->myClass->town_code_file_path = __DIR__ . '/parent-code-utf8.txt';
        $actual = $this->myClass->parseTownCodeFile();
        $actual_item = $actual[0];
        $expected_item = array(
            'town_code' => '00000000',
            'town_name' => '中華民國內政部',
        );
        $this->assertEquals($expected_item, $actual_item);
    }


    function test_parseHtmlRawPage(){
        $file_path = __DIR__ . '/entrance_2106.html';
        $html_content = file_get_contents($file_path);
        $xpath_query = '//div[@class = "panel-heading"]';
        $actual = $this->myClass->parseHtmlRawPage($html_content, $xpath_query);
        $actual_item = $actual[1]['txt'];
        $actual_item = preg_match("/(新增村里代碼)/iu", $actual_item, $matches);
        $expected_item = true;
        $this->assertEquals($expected_item, $actual_item);

        $html_content = '  ';
        $xpath_query = '//div[@class = "panel-heading"]';
        $actual = $this->myClass->parseHtmlRawPage($html_content, $xpath_query);
        $expected = null;
        $this->assertEquals($expected, $actual);
    }

    function test_getVillageCodeDataFromRawHtml(){
        $debug = false;
        //$debug = true;
        $save_log = false;
        $this->myClass->debug = $debug;
        $this->myClass->save_log = $save_log;

        $file_path = __DIR__ . '/entrance_2106.html';
        $html_content = file_get_contents($file_path);

        $actual = $this->myClass->getVillageCodeDataFromRawHtml($html_content);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }

        $actual_item = $actual['txt'];
        $expected_item = 'UNI（UTF8）';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual['date'];
        $expected_item = '2021-06-01';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual['href'];
        $expected_item = 'https://www.ris.gov.tw/documents/data/5/1/11006VillageUTF8.txt';
        $this->assertEquals($expected_item, $actual_item);


        $file_path = __DIR__ . '/entrance_2107.html';
        $html_content = file_get_contents($file_path);

        $actual = $this->myClass->getVillageCodeDataFromRawHtml($html_content);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }

        $actual_item = $actual['txt'];
        $expected_item = 'UNI（UTF8）';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual['date'];
        $expected_item = '2021-07-01';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual['href'];
        $expected_item = 'https://www.ris.gov.tw/documents/data/5/1/11007VillageUTF8.txt';
        $this->assertEquals($expected_item, $actual_item);
    }

    function test_getAddedVillageCodeDataFromRawHtml_new_added_part(){
        $save_log = false;
        $this->myClass->save_log = $save_log;
        $file_path = __DIR__ . '/entrance_2106.html';
        $html_content = file_get_contents($file_path);
        $needle = "新增村里代碼";
        $xpath_query = '//div[@class = "panel-heading"]';
        $actual = $this->myClass->getVillageRevisedCodeDataFromRawHtml($html_content, $needle, $xpath_query);

        $actual_item = $actual[0]['txt'];
        $expected_item = '11006版';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]['date'];
        $expected_item = '2021-06-01';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]['href'];
        $expected_item = 'https://www.ris.gov.tw/documents/data/5/1/11006_Village_a.txt';
        $this->assertEquals($expected_item, $actual_item);
    }

    function test_getAddedVillageCodeDataFromRawHtml_deleted_part(){
        $save_log = false;
        $this->myClass->save_log = $save_log;
        $file_path = __DIR__ . '/entrance_2106.html';
        $html_content = file_get_contents($file_path);
        $needle = "刪除村里代碼";
        $xpath_query = '//div[@class = "panel-body"]';
        $actual = $this->myClass->getVillageRevisedCodeDataFromRawHtml($html_content, $needle, $xpath_query);
        //var_dump($actual);

        $actual_item = $actual[0]['txt'];
        $expected_item = '11006版';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]['date'];
        $expected_item = '2021-06-01';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]['href'];
        $expected_item = 'https://www.ris.gov.tw/documents/data/5/1/11006_Village_d.txt';
        $this->assertEquals($expected_item, $actual_item);
    }

    function test_parseVillageCodeFile(){
        $this->myClass->village_code_file_path = __DIR__ . '/village-code-utf8.txt';
        $actual = $this->myClass->parseVillageCodeFile();
        $actual_item = $actual[0]['village_code'];
        $expected_item = '09007010001';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]['town_code'];
        $expected_item = '09007010';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]['village_name'];
        $expected_item = '介壽村';
        $this->assertEquals($expected_item, $actual_item);
    }

    function test_verifyVillageCodeData_where_normal_input(){
        $row_data = array(
            '09007010001',
            '09007010',
            '介壽村'
        );
        $actual = $this->myClass->verifyVillageCodeData($row_data);
        $expected = true;
        $this->assertEquals($expected, $actual);
    }

    function test_verifyVillageCodeData_where_illnormal_input(){
        $row_data = array(
            'not number',
            '09007010',
            '介壽村'
        );
        try {
            $this->myClass->verifyVillageCodeData($row_data);
        } catch (\Exception $e) {
            $errorHappened = true;
            $this->assertEquals('預期第 1 欄是數字，請檢查檔案格式是否變動！', $e->getMessage());
            $this->assertEquals($e->getCode(), 0);
        }
        $this->assertTrue($errorHappened);


        $row_data = array(
            '09007010001',
            'not number',
            '介壽村'
        );
        try {
            $this->myClass->verifyVillageCodeData($row_data);
        } catch (\Exception $e) {
            $errorHappened = true;
            $this->assertEquals('預期第 2 欄是數字，請檢查檔案格式是否變動！', $e->getMessage());
            $this->assertEquals($e->getCode(), 0);
        }
        $this->assertTrue($errorHappened);


        $row_data = array(
            '09007010001',
            '09007010',
            'not chinese'
        );
        try {
            $this->myClass->verifyVillageCodeData($row_data);
        } catch (\Exception $e) {
            $errorHappened = true;
            $this->assertEquals('預期第 3 欄至少包含一個中文字，請檢查檔案格式是否變動！', $e->getMessage());
            $this->assertEquals($e->getCode(), 0);
        }
        $this->assertTrue($errorHappened);
    }

    function test_parseVillageHistoryFile_where_txt_file_contains_separator(){
        $debug = false;
        $save_log = false;
        $this->myClass->save_log = $save_log;
        //$debug = true;

        // big5
        $file_path = __DIR__ . '/village_new_added/2002-01-01.txt';
        $file_name = '2002-01-01.txt';
        $actual = $this->myClass->parseVillageHistoryFile($file_path, 'added_at', $file_name);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0];
        $expected_item = array(
            'village_code' => '10012090018',
            'city_and_village_name' => '三德村',
            'added_at' => '2002-01-01',
        );
        $this->assertEquals($expected_item, $actual_item);


        // utf8
        $file_path = __DIR__ . '/village_new_added/2021-06-01.txt';
        $file_name = '2021-06-01.txt';
        $actual = $this->myClass->parseVillageHistoryFile($file_path, 'added_at', $file_name);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0];
        $expected_item = array(
            'village_code' => '10014010047',
            'city_and_village_name' => '臺東縣臺東市仁和里',
            'added_at' => '2021-06-01',
        );
        $this->assertEquals($expected_item, $actual_item);

        $file_path = __DIR__ . '/village_new_added/2021-07-01.txt';
        $file_name = '2021-07-01.txt';
        $actual = $this->myClass->parseVillageHistoryFile($file_path, 'added_at', $file_name);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0];
        $expected_item = array(
            'village_code' => '10008130017',
            'city_and_village_name' => '南投縣仁愛鄉德鹿谷村',
            'added_at' => '2021-07-01',
        );
        $this->assertEquals($expected_item, $actual_item);
    }

    function test_parseVillageHistoryFile_where_txt_file_not_contains_separator(){
        $debug = false;
        $save_log = false;
        $this->myClass->save_log = $save_log;
        //$debug = true;

        // big5
        $file_path = __DIR__ . '/village_new_added/2002-01-01.txt';
        $file_name = '2002-01-01.txt';
        $actual = $this->myClass->parseVillageHistoryFile($file_path, 'added_at', $file_name);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0];
        $expected_item = array(
            'village_code' => '10012090018',
            'city_and_village_name' => '三德村',
            'added_at' => '2002-01-01',
        );
        $this->assertEquals($expected_item, $actual_item);

        $file_path = __DIR__ . '/village_new_added/2005-01-01.txt';
        $file_name = null;
        $actual = $this->myClass->parseVillageHistoryFile($file_path, 'added_at', $file_name);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0];
        $expected_item = array(
            'village_code' => '10012080036',
            'city_and_village_name' => '仁義里',
            'added_at' => '2005-01-01',
        );
        $this->assertEquals($expected_item, $actual_item);

    }

    function test_parseVillageHistoryFile_where_docx_file(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;

        $save_log = false;
        $this->myClass->save_log = $save_log;

        $file_path = __DIR__ . '/village_new_added/2013-12-01.docx';
        $file_name = '2003-12-01.docx';
        $actual = $this->myClass->parseVillageHistoryFile($file_path, 'added_at', $file_name);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0];
        $expected_item = array(
            'village_code' => '10008130016',
            'city_and_village_name' => '都達村',
            'added_at' => '2003-12-01',
        );
        $this->assertEquals($expected_item, $actual_item);


        $file_path = __DIR__ . '/village_deleted/2014-03-01.docx';
        $file_name = '2014-03-01.docx';
        $actual = $this->myClass->parseVillageHistoryFile($file_path, 'deleted_at', $file_name);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0];
        $expected_item = array(
            'village_code' => '10002010003',
            'city_and_village_name' => '民生里',
            'deleted_at' => '2014-03-01',
        );
        $this->assertEquals($expected_item, $actual_item);

    }

    function test_parseVillageHistoryFile_where_doc_file(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;

        $save_log = false;
        $this->myClass->save_log = $save_log;

        $file_path = __DIR__ . '/village_new_added/2013-12-01.doc';
        $file_name = '2003-12-01.doc';


        try {
            $actual = $this->myClass->parseVillageHistoryFile($file_path, 'added_at', $file_name);
            if ($debug) {
                echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
            }
        } catch (\Exception $e) {
            $errorHappened = true;
            $this->assertEquals('「新增村里代碼」或「刪除村里代碼」檔案非預期的 TXT (txt) 或 Word (docx) 檔案，請聯繫管理員處理。檔案名稱：2003-12-01.doc', $e->getMessage());
            $this->assertEquals($e->getCode(), 0);
        }

        $this->assertTrue($errorHappened);



    }

    function test_parseVillageHistoryFileContent_where_time_column_is_add(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;
        $file_name = "2013-12-01.txt";
        $input = <<<EOT

宜蘭縣宜蘭市
10002010003,民生里
10002010004,昇平里
10002010005,大東里


EOT;
        $actual = $this->myClass->parseVillageHistoryFileContent($file_name, $input, "added_at");
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0]["village_code"];
        $expected_item = "10002010003";
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]["city_and_village_name"];
        $expected_item = "民生里";
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]["added_at"];
        $expected_item = "2013-12-01";
        $this->assertEquals($expected_item, $actual_item);
    }

    function test_parseVillageHistoryFileContent_where_time_column_is_delete(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;
        $file_name = "2014-03-01.txt";
        $input = <<<EOT

宜蘭縣宜蘭市
10002010003,10002010,民生里　,
10002010004,10002010,昇平里　,
10002010005,10002010,大東里　,


EOT;
        $actual = $this->myClass->parseVillageHistoryFileContent($file_name, $input, "deleted_at");
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0];
        $expected_item = array(
            'village_code' => '10002010003',
            'city_and_village_name' => '民生里',
            'deleted_at' => '2014-03-01',
        );
        $this->assertEquals($expected_item, $actual_item);

    }

    function test_trimString(){
        $input = "10002010003,10002010,民生里　";
        $actual = $this->myClass->trimString($input);
        $expected = "10002010003,10002010,民生里";
        $this->assertEquals($expected, $actual);

    }

    function test_getKeywordStrategy(){
        $city_name = '臺南市柳營區';
        $village_name = '重溪里';
        $actual = $this->myClass->getKeywordStrategy($city_name, $village_name);
        $expected_item = '臺南市柳營區重溪里';
        $this->assertContains($expected_item, $actual);

        $expected_item = '"臺南市柳營區重溪里"';
        $this->assertContains($expected_item, $actual);

        $expected_item = '台南市柳營區重溪里';
        $this->assertContains($expected_item, $actual);

        $expected_item = '"台南市柳營區重溪里"';
        $this->assertContains($expected_item, $actual);

        $expected_item = '重溪里';
        $this->assertContains($expected_item, $actual);



        $city_name = '福建省連江縣南竿鄉';
        $village_name = '復興村';
        $actual = $this->myClass->getKeywordStrategy($city_name, $village_name);
        $expected_item = '福建省連江縣南竿鄉復興村';
        $this->assertContains($expected_item, $actual);

        $expected_item = '連江縣南竿鄉復興村';
        $this->assertContains($expected_item, $actual);
    }

    function test_verifyParentCodeData_where_normal_input(){
        $row_data = array(
            '00000000',
            '中華民國內政部'
        );
        $actual = $this->myClass->verifyParentCodeData($row_data);
        $expected = true;
        $this->assertEquals($expected, $actual);

        $row_data = array(
            '1000107A',
            '臺灣省臺北縣樹林鎮'
        );
        $actual = $this->myClass->verifyParentCodeData($row_data);
        $expected = true;
        $this->assertEquals($expected, $actual);
    }

    function test_verifyParentCodeData_where_illnormal_input(){
        $row_data = array(
            'not number',
            '中華民國內政部'
        );
        try {
            $this->myClass->verifyParentCodeData($row_data);
        } catch (\Exception $e) {
            $errorHappened = true;
            $this->assertEquals('預期第 1 欄是數字或英文組合，請檢查檔案格式是否變動！', $e->getMessage());
            $this->assertEquals($e->getCode(), 0);
        }
        $this->assertTrue($errorHappened);


        $row_data = array(
            '00000000',
            'not chinese'
        );
        try {
            $this->myClass->verifyParentCodeData($row_data);
        } catch (\Exception $e) {
            $errorHappened = true;
            $this->assertEquals('預期第 2 欄至少包含一個中文字，請檢查檔案格式是否變動！', $e->getMessage());
            $this->assertEquals($e->getCode(), 0);
        }
        $this->assertTrue($errorHappened);
    }

    function test_replaceProvinceFromCityName(){
        $city_name = '臺中市中區';
        $actual = $this->myClass->replaceProvinceFromCityName($city_name);
        $expected = '臺中市中區';
        $this->assertEquals($expected, $actual);

        $city_name = '臺灣省新竹市東區';
        $actual = $this->myClass->replaceProvinceFromCityName($city_name);
        $expected = '新竹市東區';
        $this->assertEquals($expected, $actual);

        $city_name = '臺灣省新竹市東省區';
        $actual = $this->myClass->replaceProvinceFromCityName($city_name);
        $expected = '新竹市東省區';
        $this->assertEquals($expected, $actual);

        $city_name = '福建省連江縣南竿鄉';
        $actual = $this->myClass->replaceProvinceFromCityName($city_name);
        $expected = '連江縣南竿鄉';
        $this->assertEquals($expected, $actual);
    }

    function test_replaceProvinceAndTownFromVillageName(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;

        $town_name = '臺南市中西區';
        $village_name = '臺南市中西區三合里';
        $actual = $this->myClass->replaceProvinceAndTownFromVillageName($town_name, $village_name);
        $expected = '三合里';
        $this->assertEquals($expected, $actual);

        $town_name = '臺灣省臺南市中西區';
        $village_name = '臺南市中西區三合里';
        $actual = $this->myClass->replaceProvinceAndTownFromVillageName($town_name, $village_name);
        $expected = '三合里';
        $this->assertEquals($expected, $actual);

        $town_name = '臺灣省臺南市中西區';
        $village_name = '臺南市中西區三合里';
        $actual = $this->myClass->replaceProvinceAndTownFromVillageName($town_name, $village_name);
        $expected = '三合里';
        $this->assertEquals($expected, $actual);

        $town_name = '臺灣省臺北縣淡水鎮';
        $village_name = '台北縣淡水鎮新義里';
        $actual = $this->myClass->replaceProvinceAndTownFromVillageName($town_name, $village_name);
        $expected = '新義里';
        $this->assertEquals($expected, $actual);

        $town_name = '臺灣省南投縣仁愛鄉';
        $village_name = '德鹿谷村';
        $actual = $this->myClass->replaceProvinceAndTownFromVillageName($town_name, $village_name);
        $expected = '德鹿谷村';
        $this->assertEquals($expected, $actual);

        $town_name = '臺灣省彰化縣大城鄉';
        $village_name = '臺灣省彰化縣大城鄉臺西村';
        $actual = $this->myClass->replaceProvinceAndTownFromVillageName($town_name, $village_name);
        $expected = '臺西村';
        $this->assertEquals($expected, $actual);
    }

    function test_replaceAdministrativeName(){
        $input = '臺灣省彰化縣大城鄉臺西村';
        $actual = $this->myClass->replaceAdministrativeName($input);
        $expected = '臺灣省彰化縣大城鄉臺西村';
        $this->assertEquals($expected, $actual);
    }


    function test_getCityLevel1Name(){
        $debug = false;
        $this->myClass->debug = $debug;
        $input = '臺灣省彰化縣大城鄉臺西村';
        $actual = $this->myClass->getCityLevel1Name($input);
        $expected = '彰化縣';
        $this->assertEquals($expected, $actual);

        $input = '福建省金門縣金湖鎮';
        $actual = $this->myClass->getCityLevel1Name($input);
        $expected = '金門縣';
        $this->assertEquals($expected, $actual);

        $input = '臺灣省屏東縣屏東市';
        $actual = $this->myClass->getCityLevel1Name($input);
        $expected = '屏東縣';
        $this->assertEquals($expected, $actual);

        $input = '屏東縣屏東市';
        $actual = $this->myClass->getCityLevel1Name($input);
        $expected = '屏東縣';
        $this->assertEquals($expected, $actual);
    }

    function test_getCityLevel2Name(){
        $debug = false;
        $this->myClass->debug = $debug;
        $input = '臺灣省彰化縣大城鄉臺西村';
        $actual = $this->myClass->getCityLevel2Name($input);
        $expected = '大城鄉';
        $this->assertEquals($expected, $actual);

        $input = '福建省金門縣金湖鎮';
        $actual = $this->myClass->getCityLevel2Name($input);
        $expected = '金湖鎮';
        $this->assertEquals($expected, $actual);

        $input = '臺灣省屏東縣屏東市';
        $actual = $this->myClass->getCityLevel2Name($input);
        $expected = '屏東市';
        $this->assertEquals($expected, $actual);

        $input = '臺灣省屏東縣';
        $actual = $this->myClass->getCityLevel2Name($input);
        $expected = null;
        $this->assertEquals($expected, $actual);

        $input = '臺灣省';
        $actual = $this->myClass->getCityLevel2Name($input);
        $expected = null;
        $this->assertEquals($expected, $actual);

        $input = '臺灣省屏東縣屏東市頂柳里';
        $actual = $this->myClass->getCityLevel2Name($input);
        $expected = '屏東市';
        $this->assertEquals($expected, $actual);
    }

    function test_getAdministrativeData(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;
        $input = '臺灣省彰化縣大城鄉臺西村';
        $actual = $this->myClass->getAdministrativeData($input);
        $actual_item = $actual['level1'];
        $expected_item = '彰化縣';
        $this->assertEquals($expected_item, $actual_item);

        $input = '福建省金門縣金湖鎮';
        $actual = $this->myClass->getAdministrativeData($input);
        $actual_item = $actual['level1'];
        $expected_item = '金門縣';
        $this->assertEquals($expected_item, $actual_item);

        $input = '臺灣省屏東縣屏東市';
        $actual = $this->myClass->getAdministrativeData($input);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual['level1'];
        $expected_item = '屏東縣';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual['level2'];
        $expected_item = '屏東市';
        $this->assertEquals($expected_item, $actual_item);

        $input = '臺灣省屏東縣屏東市頂柳里';
        $actual = $this->myClass->getAdministrativeData($input);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual['level1'];
        $expected_item = '屏東縣';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual['level2'];
        $expected_item = '屏東市';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual['level3'];
        $expected_item = '頂柳里';
        $this->assertEquals($expected_item, $actual_item);
    }

}

