<?php

$debug = true;
$time_column = 'added_at';
//---
require_once __DIR__ . '/../load.php';

$db_go = new DbClass();
$db_go->debug = $debug;
$record = array(
    'village_code' => 'xxx',
    'city_and_village_name' => 'not exists 1',
    'added_at' => '2021-07-01',
);
$result = $db_go->saveVillageHistoryRecord($record, 'added_at');
var_dump($result);

$record = array(
    'village_code' => 'xxx2',
    'city_and_village_name' => 'not exists 2',
    'added_at' => '2021-07-01',
);
$result = $db_go->saveVillageHistoryRecord($record, 'added_at');
var_dump($result);

$record = array(
    'village_code' => '09007010001',
    'city_and_village_name' => '福建省連江縣南竿鄉介壽村',
    'added_at' => '2021-07-01',
);
$result = $db_go->saveVillageHistoryRecord($record, 'added_at');
var_dump($result);
