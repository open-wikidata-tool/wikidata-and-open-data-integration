<?php

$debug = true;
// Wikidata Query Service https://w.wiki/4KQi
//$file_path = __DIR__ . '/../files/wikidata_query.json';

//$save_to_db = false;
$save_to_db = true;
//----
require_once __DIR__ . '/../load.php';
$endpointUrl = 'https://query.wikidata.org/sparql';
$wikidata_go = new WikidataClass($endpointUrl);
$wikidata_go->debug = $debug;
$total_affected_count = $wikidata_go->crawlAndSaveQueryResult($save_to_db);
if ($debug) {
    echo '$total_affected_count is: ' . print_r($total_affected_count, true) . PHP_EOL;
}
