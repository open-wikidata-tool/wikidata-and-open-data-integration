<?php

require_once __DIR__ . '/../scripts/SchoolClass.php';
require_once __DIR__ . '/../scripts/ExcelClass.php';

use PHPUnit\Framework\TestCase;
use wikidata_tw\SchoolClass;

class SchoolClass_postProcessOfStatusFieldTest extends TestCase
{

    private $myClass;

    protected function setUp(): void
    {
        $this->myClass = new SchoolClass();
    }

    public function testPostProcessOfStatusField()
    {
        // Test removing the '※' character
        $this->assertEquals('停辦', $this->myClass->postProcessOfStatusField('※停辦'), 'The prefix ※ should be removed.');

        // Test with an empty string
        $this->assertEquals('', $this->myClass->postProcessOfStatusField(''), 'The result should be an empty string if input is empty.');

        // Test with no '※' in the string
        $this->assertEquals('正常', $this->myClass->postProcessOfStatusField('正常'), 'The status should remain unchanged if it does not start with ※.');

        // Test with multiple '※' in the string
        $this->assertEquals('※停辦', $this->myClass->postProcessOfStatusField('※※停辦'), 'Only the first prefix ※ should be removed.');
    }

}

