<?php

$debug = true;
//$query_data = array('village_code' => '123');
$query_data = array('village_code' => '67000040008');

$table_name = 'wikidata_village';
$insert_rows = array();
$insert_rows[] = array(
    'wikidata_id' => 123,
    'search_snippet' => '123-2',
    'wikidata_updated_at' => '2021-10-31 17:56:00',
    'wikidata_updated_at_raw' => 'test123',
);
$insert_rows[] = array(
    'wikidata_id' => 555,
    'search_snippet' => '555-2',
    'wikidata_updated_at' => date('Y-m-d H:i:s'),
    'wikidata_updated_at_raw' => 'test555',
);
$unique_column = 'wikidata_id';
$pk_column = 'wikidata_id';
$ignore_fields = array('created_at', 'wikidata_id');
//---
require_once __DIR__ . '/../load.php';

$db_go = new DbClass();
$db_go->debug = $debug;
$total_affected_count = $db_go->insertOrUpdateMany($table_name, $insert_rows, $unique_column, $pk_column, $ignore_fields);
var_dump($total_affected_count);
