<?php

require_once __DIR__ . '/../scripts/OSMClass.php';

use PHPUnit\Framework\TestCase;

class OSMClassTest extends TestCase
{

    private $myClass;

    protected function setUp(): void
    {
        $this->myClass = new OSMClass();
    }

    function test_getCoordinateDataFromPoint(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;

        $input = 'Point(120.644 23.1625)';
        $actual = $this->myClass->getCoordinateDataFromPoint($input);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $expected = array('120.644', '23.1625');
        $this->assertEquals($expected, $actual);

        $input = null;
        $actual = $this->myClass->getCoordinateDataFromPoint($input);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $expected = null;
        $this->assertEquals($expected, $actual);

    }

}

