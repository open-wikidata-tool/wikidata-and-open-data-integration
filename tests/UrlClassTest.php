<?php

require_once __DIR__ . '/../load.php';

use PHPUnit\Framework\TestCase;
use wikidata_tw\UrlClass;

class UrlClassTest extends TestCase
{
    private $urlCombiner;

    protected function setUp(): void {
        $this->urlCombiner = new UrlClass();
    }

    public function testCombinesUrlCorrectly_where_base_url_ends_with_a_slash() {
        $baseUrl = 'https://depart.moe.edu.tw/ED4500/';
        $partialUrl = 'News_Content.aspx?n=63F5AB3D02A8BBAC&sms=1FF9979D10DBF9F3&s=E23C5A6CA17DB8E2';
        $expected = 'https://depart.moe.edu.tw/ED4500/News_Content.aspx?n=63F5AB3D02A8BBAC&sms=1FF9979D10DBF9F3&s=E23C5A6CA17DB8E2';
        $this->assertEquals($expected, $this->urlCombiner->combineUrl($baseUrl, $partialUrl));
    }

    public function testCombinesUrlCorrectly_where_base_url_not_ends_with_a_slash() {
        $baseUrl = 'https://depart.moe.edu.tw/ED4500';
        $partialUrl = '/News_Content.aspx?n=63F5AB3D02A8BBAC&sms=1FF9979D10DBF9F3&s=E23C5A6CA17DB8E2';
        $expected = 'https://depart.moe.edu.tw/ED4500/News_Content.aspx?n=63F5AB3D02A8BBAC&sms=1FF9979D10DBF9F3&s=E23C5A6CA17DB8E2';
        $this->assertEquals($expected, $this->urlCombiner->combineUrl($baseUrl, $partialUrl));
    }

    public function testCombinesUrlCorrectly_where_partial_url_with_leading_slash() {
        $baseUrl = 'https://depart.moe.edu.tw/ED4500';
        $partialUrl = '/News_Content.aspx?n=63F5AB3D02A8BBAC&sms=1FF9979D10DBF9F3&s=E23C5A6CA17DB8E2';
        $expected = 'https://depart.moe.edu.tw/ED4500/News_Content.aspx?n=63F5AB3D02A8BBAC&sms=1FF9979D10DBF9F3&s=E23C5A6CA17DB8E2';
        $this->assertEquals($expected, $this->urlCombiner->combineUrl($baseUrl, $partialUrl));
    }
}
