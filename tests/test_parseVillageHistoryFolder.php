<?php

$debug = true;
$folder_path = __DIR__ . '/../files/village_new_added/';
$time_column = 'added_at';
$save_db = true;
//---
require_once __DIR__ . '/../load.php';

$crawler_go = new CrawlerClass();
$crawler_go->debug = $debug;
$crawler_go->save_db = $save_db;
$total_affected_count = $crawler_go->parseAndSaveVillageHistoryFolder($folder_path, $time_column);
echo '$total_affected_count of parseAndSaveVillageHistoryFolder: ' . $total_affected_count.  PHP_EOL;
