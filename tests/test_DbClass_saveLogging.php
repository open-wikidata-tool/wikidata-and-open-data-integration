<?php

$debug = true;
$query_data = array(
    'village_code' => '09007010003',
    'village_name' => '福沃村',
);
$table_name = 'main_tb';
$column_name = 'village_code';
//---
require_once __DIR__ . '/../load.php';

$db_go = new DbClass();
$db_go->debug = $debug;
$insert_data = array(
    'short_name' => 'testSaveLogging',
    'name' => 'test save logging',
    'entity' => 'entity 1',
    'last_error' => 'error 1'
);
$ignore_fields = array('short_name');
$save_result = $db_go->saveLogging($insert_data, $ignore_fields);
var_dump($save_result);

$insert_data = array(
    'short_name' => 'testSaveLogging',
    'name' => 'test save logging',
    'entity' => 'entity 2',
    'last_error' => 'error 2'
);
$ignore_fields = array('short_name');
$save_result = $db_go->saveLogging($insert_data, $ignore_fields);
var_dump($save_result);
