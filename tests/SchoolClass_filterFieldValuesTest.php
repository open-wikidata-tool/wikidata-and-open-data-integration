<?php

require_once __DIR__ . '/../scripts/SchoolClass.php';
require_once __DIR__ . '/../scripts/ExcelClass.php';

use PHPUnit\Framework\TestCase;
use wikidata_tw\SchoolClass;

class SchoolClass_filterFieldValuesTest extends TestCase
{

    private $myClass;

    protected function setUp(): void
    {
        $this->myClass = new SchoolClass();
    }

    public function testFilterFieldValuesWithNullValues()
    {
        $input = ['apple', null, ' banana ', ' ', null];
        $expected = ['apple', 'banana'];
        $result = $this->myClass->filterFieldValues($input);
        $this->assertEquals($expected, array_values($result), "Should remove null values and trim strings");
    }

    public function testFilterFieldValuesWithEmptyArray()
    {
        $input = [];
        $expected = [];
        $result = $this->myClass->filterFieldValues($input);
        $this->assertEquals($expected, $result, "Should handle empty array correctly");
    }

    public function testFilterFieldValuesWithAllNullValues()
    {
        $input = [null, null, null];
        $expected = [];
        $result = $this->myClass->filterFieldValues($input);
        $this->assertEquals($expected, $result, "Should return an empty array when all values are null");
    }

    public function testFilterFieldValuesWithWhitespaceStrings()
    {
        $input = ['  ', ' pear ', '  ', 'apple', ''];
        $expected = ['pear', 'apple'];
        $result = $this->myClass->filterFieldValues($input);
        $this->assertEquals($expected, array_values($result), "Should trim and remove empty or whitespace-only strings");

        $input = ['', ' ', '', '112.08.01公告', ''];
        $expected = ['112.08.01公告'];
        $result = $this->myClass->filterFieldValues($input);
        $this->assertEquals($expected, array_values($result), "Should trim and remove empty or whitespace-only strings");
    }
}

