<?php

require_once __DIR__ . '/../scripts/TimeClass.php';

use PHPUnit\Framework\TestCase;

class TimeClassTest extends TestCase
{

    private $myClass;

    protected function setUp(): void
    {
        $this->myClass = new TimeClass();
    }

    function test_preProcessTwYearMonth(){
        $input = ' 11005版';
        $actual = $this->myClass->preProcessTwYearMonth($input);
        $expected = '11005';
        $this->assertEquals($expected, $actual);

        $input = ' 11006';
        $actual = $this->myClass->preProcessTwYearMonth($input);
        $expected = '11006';
        $this->assertEquals($expected, $actual);

        $input = '/documents/data/5/1/11004VillageBIG5.txt';
        $actual = $this->myClass->preProcessTwYearMonth($input);
        $expected = '11004';
        $this->assertEquals($expected, $actual);

        $input = '110.06.01';
        $actual = $this->myClass->preProcessTwYearMonth($input);
        $expected = '110.06.01';
        $this->assertEquals($expected, $actual);
    }

    function test_convertDateFromTwEra(){
        $input = ' 11006版';
        $actual = $this->myClass->convertDateFromTwEra($input);
        $expected = '2021-06-01';
        $this->assertEquals($expected, $actual);

        $input = ' 11006';
        $actual = $this->myClass->convertDateFromTwEra($input);
        $expected = '2021-06-01';
        $this->assertEquals($expected, $actual);
    }

    function test_convertDateFromTwEra_where_separate_by_dot(){
        $input = '110.06.01';
        $actual = $this->myClass->convertDateFromTwEra($input);
        $expected = '2021-06-01';
        $this->assertEquals($expected, $actual);
    }

    function test_convertDateFromTwEra_where_input_is_url(){
        $input = '/documents/data/5/1/11006VillageBIG5.txt';
        $actual = $this->myClass->convertDateFromTwEra($input);
        $expected = '2021-06-01';
        $this->assertEquals($expected, $actual);

        $input = '/documents/data/5/1/11004VillageBIG5.txt';
        $actual = $this->myClass->convertDateFromTwEra($input);
        $expected = '2021-04-01';
        $this->assertEquals($expected, $actual);
    }


}

