<?php

require_once __DIR__ . '/../scripts/WikidataClass.php';

use PHPUnit\Framework\TestCase;

class WikidataClassTest extends TestCase
{

    private $myClass;

    protected function setUp(): void
    {
        $endpointUrl = 'https://query.wikidata.org/sparql';
        $this->myClass = new WikidataClass($endpointUrl);
    }


    function test_getWikidataIdFromUrl(){
        $url = "http://www.wikidata.org/entity/Q17038291";
        $actual = $this->myClass->getWikidataIdFromUrl($url);
        $expected = '17038291';
        $this->assertEquals($expected, $actual);

        $url = "https://www.wikidata.org/entity/Q17038291";
        $actual = $this->myClass->getWikidataIdFromUrl($url);
        $expected = '17038291';
        $this->assertEquals($expected, $actual);

        $url = "https://www.wikidata.org/entity/17038291";
        $actual = $this->myClass->getWikidataIdFromUrl($url);
        $expected = null;
        $this->assertEquals($expected, $actual);
    }

    function test_parseApiIdsResult_where_normal_input(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;

        $file_path = __DIR__ . '/wikidata_api/ids_Q98000157,Q11083338.json';
        $json_content = file_get_contents($file_path);
        $label = null;
        $save_logging = false;
        $actual = $this->myClass->parseApiIdsResult($json_content, $label, $save_logging);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0]['wikidata_id'];
        $expected_item = '98000157';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]['labels'];
        $expected_item = '臺北市客家文化主題公園';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]['descriptions'];
        $expected_item = '臺北市的展覽館';
        $this->assertEquals($expected_item, $actual_item);


    }

    function test_parseApiIdsResult_where_error_input(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;

        $file_path = __DIR__ . '/wikidata_api/ids_error.json';
        $json_content = file_get_contents($file_path);
        $label = null;
        $save_logging = false;
        $actual = $this->myClass->parseApiIdsResult($json_content, $label, $save_logging);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $expected = null;
        $this->assertEquals($expected, $actual);


    }

    function test_parseWikiDataApiResult(){
        $file_path = __DIR__ . '/wikidata_api/wikidata_api_result_of_search.json';
        $file_content = file_get_contents($file_path);
        $actual = $this->myClass->parseWikiDataKeywordSearchResult($file_content);

        $actual_item = $actual[0]['wikidata_id'];
        $expected_item = '64451271';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]['wikidata_updated_at_raw'];
        $expected_item = '2020-08-17T20:45:13Z';
        $this->assertEquals($expected_item, $actual_item);

        $actual_item = $actual[0]['search_snippet'];
        $expected_item = '中華民國連江縣南竿鄉轄下一里行政區';
        $this->assertEquals($expected_item, $actual_item);
    }
}

