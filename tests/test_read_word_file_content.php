<?php

$debug = true;

// 另存成
// RTF 檔案內容是亂碼
// HTML 檔案內容是亂碼
// Word2007: Word 打開說格式有問題
// ODS: LibreOffice 打開說格式有問題

//$source = __DIR__ . '/village_new_2013-12-01.doc';
$source = __DIR__ . '/village_new_2013-12-01.docx';
//$source = __DIR__ . '/village_new_2017-12-01.docx';


//-----
require_once __DIR__ . '/../load.php';



// https://github.com/PHPOffice/PHPWord/blob/develop/samples/Sample_11_ReadWord97.php
// Read contents

echo date('H:i:s'), " Reading contents from `{$source}`" . PHP_EOL;
//$phpWord = \PhpOffice\PhpWord\IOFactory::load($source, 'MsDoc');
$phpWord = \PhpOffice\PhpWord\IOFactory::load($source);

// yii2 - how to get content from Doc file in php - Stack Overflow https://stackoverflow.com/questions/44602170/how-to-get-content-from-doc-file-in-php
//var_dump($phpWord);

// Creating the new document...
//$phpWord = new \PhpOffice\PhpWord\PhpWord();

// Save file
//echo write($phpWordReader, basename(__FILE__, '.php'), $writers);

$tempfile = __DIR__ . '/tmp.html';
if(file_exists($tempfile)){
    rename($tempfile, $tempfile . '.bak');
}

// Saving the document as HTML file...
// if doc file
//$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'ODText');

// if docx file
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');

$objWriter->save($tempfile);

if(!file_exists($tempfile)){
    echo 'The file was NOT converted: ' . $tempfile . PHP_EOL;
}else{
    echo 'The file was converted: ' . $tempfile . PHP_EOL;
}


$file_content = file_get_contents($tempfile); // contains HTML tags
//$file_content = strip_tags($file_content); // contains <HEAD> part

if ($debug) {
    echo '$file_content is: ' . print_r($file_content, true) . PHP_EOL;
}


// Create a DOM object from a HTML file
//$file_content = file_get_html('http://www.google.com/')->plaintext;
//$file_content = file_get_html($tempfile)->plaintext;
//$doc = new HtmlWeb();
//echo $doc->load('https://www.google.com/')->plaintext;
//echo file_get_html($tempfile)->plaintext;
//echo file_get_html('https://www.google.com/')->plaintext;
//$file_content2 = str_get_html($file_content); // object
$file_content2 = str_get_html($file_content)->plaintext;


if ($debug) {
    echo '$file_content2 is: ' . print_r($file_content2, true) . PHP_EOL;
}

