<?php

$debug = true;
//---
require_once __DIR__ . '/../load.php';

$db_go = new DbClass();
//$db_go->debug = true;

$wikidata_go = new WikidataClass();
$wikidata_go->debug = true;


$ids = array();
//$ids[] = 'Q98000157';
$ids[] = 'Q11083338';
$total_affected_count = $wikidata_go->crawlApiGivenIds($ids);
echo '$total_affected_count of crawlApiGivenIds: ' . print_r($total_affected_count, true) . PHP_EOL;


$ids = $db_go->getWikidataIds();
if ($debug) {
    echo '$ids is: ' . print_r($ids, true) . PHP_EOL;
}
$total_affected_count = $wikidata_go->crawlApiGivenIds($ids);
echo '$total_affected_count of crawlApiGivenIds: ' . print_r($total_affected_count, true) . PHP_EOL;

