-- Adminer 4.8.1 MySQL 5.7.30 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `wikidata_entry`;
CREATE TABLE `wikidata_entry` (
  `wikidata_id` bigint(20) unsigned NOT NULL COMMENT 'without Q',
  `labels` text COLLATE utf8mb4_unicode_ci,
  `descriptions` text COLLATE utf8mb4_unicode_ci,
  `search_ snippet` text COLLATE utf8mb4_unicode_ci,
  `wikidata_created_at` timestamp NULL DEFAULT NULL,
  `wikidata_created_at_raw` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'api: cirrusbuilddoc. create_timestamp',
  `wikidata_updated_at` timestamp NULL DEFAULT NULL,
  `wikidata_updated_at_raw` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'api: cirrusbuilddoc.timestamp',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `wikidata_id` (`wikidata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2021-10-16 10:00:33
