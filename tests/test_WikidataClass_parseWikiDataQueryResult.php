<?php

$debug = true;
// Wikidata Query Service https://w.wiki/4KQi
$file_path = __DIR__ . '/../files/wikidata_query.json';
require_once __DIR__ . '/../load.php';
//----
$wikidata_go = new WikidataClass();
$file_content = file_get_contents($file_path);


$parsed_content = $wikidata_go->parseWikiDataQueryResult($file_content);
if ($debug) {
    echo '$parsed_content is: ' . print_r($parsed_content, true) . PHP_EOL;
}
