<?php

require_once __DIR__ . '/../scripts/ExcelClass.php';

use PHPUnit\Framework\TestCase;

class ExcelClassTest extends TestCase
{

    private $myClass;

    protected function setUp(): void
    {
        $this->myClass = new ExcelClass();
    }

    public function test_getExcelContent(){
        $debug = false;
        //$debug = true;
        $this->myClass->debug = $debug;
        $file_path = __DIR__ . '/depart.moe.edu.tw/112_BasicChange.xlsx';
        $actual = $this->myClass->getExcelContent($file_path);
        if ($debug) {
            echo '$actual is: ' . print_r($actual, true) . PHP_EOL;
        }
        $actual_item = $actual[0][0];
        $expected_item = '112學年度國中小異動一覽表';
        $this->assertEquals($expected_item, $actual_item);
    }
}

