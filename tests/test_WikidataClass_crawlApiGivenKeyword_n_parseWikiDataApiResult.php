<?php

$debug = true;
$keyword = '連江縣南竿鄉介壽村';
$village_code = '09007010001';
//---
require_once __DIR__ . '/../load.php';

$wikidata_go = new WikidataClass();
$wikidata_go->debug = $debug;
$result = $wikidata_go->crawlApiGivenKeyword($keyword, $village_code);
var_dump($result);

$file_path = $wikidata_go->getFilePathOfWikiDataApiResultGivenKeyword($keyword);
$json_content = file_get_contents($file_path);
//var_dump($json_content);

$json_data = $wikidata_go->parseWikiDataKeywordSearchResult($json_content);
var_dump($json_data);

