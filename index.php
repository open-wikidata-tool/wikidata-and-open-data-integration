<?php
    header("X-Frame-Options: SAMEORIGIN");
    header('X-Content-Type-Options: nosniff');
    header('X-XSS-Protection: 1; mode=block');

    require_once __DIR__ . '/load.php';


    $crawler_go = new CrawlerClass();

    /*$json_file_path = $crawler_go->integrated_file_path;
    $json_file_content = file_get_contents($json_file_path);
    $table_data = json_decode($json_file_content, true);
    */
    $db_go = new DbClass();
    $table_data = $db_go->getMainTableData($_GET);
    $total_size = $db_go->getTotalSizeOfTownAndVillageCode($_GET);


?>
<!doctype html>
<html lang="en">
<head>
<?php
    require_once __DIR__ . '/inc_header.php';
?>

<title>WikiData 與 戶役政資訊系統資料代碼 整合</title>
</head>
<body>

<div class="container">
    <h1><a href="./">WikiData 與 戶役政資訊系統資料代碼 整合</a></h1>

    <div style="">

        <div class="input-group" style="float: right; width: 300px; padding-bottom: 10px;">
            <input type="search" class="form-control rounded" placeholder="輸入關鍵字" aria-label="Search"
                   aria-describedby="search-addon" id="search-input" />
            <button type="button" class="btn btn-outline-primary" id="search-button">搜尋</button>
        </div>
    </div>

    <div style="float: left; width: 100%; padding-top: 10px;">

        狀態：
        <img src="images/label.png"> <a href="./?q=已建立鄉鎮條目">已建立鄉鎮條目</a>
        <!-- <img src="images/label.png"> <a href="./?q=電腦選的對應鄉鎮條目">電腦選的對應鄉鎮條目</a> -->
        <img src="images/label.png"> <a href="./?q=未建立且無推薦鄉鎮條目">未建立且無推薦鄉鎮條目</a>
    </div>


<?php

    $table_html = $crawler_go->printTable($table_data, $_GET);
    echo $table_html;

    // 每頁顯示筆數
    $size_options = array(10, 50, 100, 200, 500, 1000);
    $size_default = $crawler_go->page_size;
    if(isset($_GET)
        && array_key_exists('size', $_GET)
    ){
        $size_default = $_GET['size'];
        $size_default = (int) $size_default;
    }

    $size_html = array();
    foreach ($size_options AS $size_option){
        if($size_option == $size_default){
            $size_html[] = "<option selected>{$size_option}</option>";
        }else{
            $size_html[] = "<option>{$size_option}</option>";
        }

    }
    $size_html = implode(PHP_EOL, $size_html);

    // p1: offset=0
    // if size = 10
    //   p2: offset=10

    $pagination_html = $crawler_go->printPageNumbers($_GET, $size_default, $total_size);



?>

    <div class="footer">
        <div class="page-list left">
            每頁顯示筆數
            <select id="page_size">
                <?php echo $size_html?>
            </select>，

            共 <?php echo number_format($total_size)?> 筆

        </div>

        <div class="pagination right">
            <!-- <a href="#">&laquo;</a> -->
            <?php echo $pagination_html?>
            <!-- <a href="#">&raquo;</a> -->
        </div>
    </div>

</div>

<?php
    require_once __DIR__ . '/inc_footer.php';
?>
</body>
</html>
