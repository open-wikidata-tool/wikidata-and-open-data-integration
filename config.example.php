<?php

$db_config = array(
    'host' => 'localhost',
    'user' => 'user',
    'password' => 'xxx',
    'database' => 'tw-village', // database name
    'port' => 3306,
    'charset' => 'utf8mb4',
);

$db_config = json_encode($db_config);
putenv("DB_CONFIG={$db_config}");