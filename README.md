# tw-village-wikidata


## Requirements

* `PHP` >= 8
* ext-pdo for `simplon/mysql`
* cURL extension
* XML Parser extension for `PHPOffice/PHPWord`
* Laminas Escaper component for `PHPOffice/PHPWord`

## Setup Instructions

1. Begin by downloading and setting up [Composer](https://getcomposer.org/), type `curl -s https://getcomposer.org/installer | php`

2. Launch your terminal and execute the following commands:
   * For the [Simplon MySQL Library](https://packagist.org/packages/simplon/mysql), type: `composer require simplon/mysql`.
   * For [PHPOffice/PHPWord](https://github.com/PHPOffice/PHPWord), enter: `composer require phpoffice/phpword`.
   * For [PHPOffice/PhpSpreadsheet](https://github.com/PHPOffice/PhpSpreadsheet), type `composer require phpoffice/phpspreadsheet`
   * For the [PHP Simple HTML DOM Parser](https://simplehtmldom.sourceforge.io/), use: `composer require simplehtmldom/simplehtmldom`.
   
3. Set up a new database and then import the SQL file found at `db/tw-village.sql`.

4. Create a database user, ensuring you provide them with the necessary permissions for the database. 

5. Duplicate the `config.example.php` file, renaming it as `config.php`. Then, modify the database settings within this file to match your configuration.

6. Introduce a cronjob with the command: `/usr/local/bin/php -q /path/to/public_html/wikidata/cron_job.php`. Note: Running this command through a web browser may lead to a timeout error.

## Data flow



| Purpose                                                      | Related functions                                            | Dependency                                                   | Notes              | Navigation path                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------ | ------------------------------------------------------------ |
| 下載「[省市縣市鄉鎮市區代碼](https://www.ris.gov.tw/documents/html/5/1/168.html)」 | `downloadFileMappingCode`                                    |                                                              | TXT 網址似乎固定   | (1) [戶役政資訊系統資料代碼內容清單](https://www.ris.gov.tw/documents/html/5/1/168.html): 省市縣市鄉鎮市區代碼 -> 下載 TXT<br />(2) [連結介面戶役政資料代碼內容](https://www.ris.gov.tw/documents/html/5/1/167.html) -> [戶役政資訊系統資料代碼內容清單](https://www.ris.gov.tw/documents/html/5/1/168.html): 省市縣市鄉鎮市區代碼 -> 下載 TXT |
| 下載「[連結介面戶役政資料代碼內容](https://www.ris.gov.tw/documents/html/5/1/167.html)」網頁 | `downloadEntrancePage`                                       |                                                              |                    | [連結介面戶役政資料代碼內容](https://www.ris.gov.tw/documents/html/5/1/167.html) |
| 下載「村里代碼檔」                                           | (1) `downloadFileVillageCode` -> `downloadFileVillageCodeGivenUrl`  (2) `saveVillageCode` | `downloadFileVillageCode` depends on the HTML file crawled by `downloadEntrancePage` | TXT 網址會隔月變動 | [連結介面戶役政資料代碼內容](https://www.ris.gov.tw/documents/html/5/1/167.html): 村里代碼檔 -> 下載 TXT |
| 下載「新增村里代碼」                                         | `downloadNewAddedVillageCodeFiles`                           | Depends on the HTML file crawled by `downloadEntrancePage`   | TXT 網址會隔月新增 |                                                              |
|                                                              |                                                              |                                                              |                    |                                                              |
|                                                              |                                                              |                                                              |                    |                                                              |
|                                                              |                                                              |                                                              |                    |                                                              |
|                                                              |                                                              |                                                              |                    |                                                              |


## Integration of Wikidata Query

URL: https://w.wiki/4RDh

```sql
SELECT DISTINCT ?ref ?town ?townLabel ?townDescription ?startdate ?enddate ?start2 ?end2 ?new ?newLabel ?new2 ?openstreetmap ?coordinate WHERE {
  ?town wdt:P31 wd:Q7930614;
    (wdt:P131+) wd:Q865.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE], zh-tw, zh". }
  OPTIONAL { ?town wdt:P402 ?openstreetmap. }
  OPTIONAL { ?town wdt:P625 ?coordinate. }
  OPTIONAL { ?town wdt:P5020 ?ref. }
  OPTIONAL { ?town wdt:P131 ?place. }
  OPTIONAL { ?town wdt:P571 ?startdate. }
  OPTIONAL { ?town wdt:P576 ?enddate. }
  OPTIONAL {
    ?town p:P31 _:b0.
    _:b0 pq:P580 ?start2.
  }
  OPTIONAL {
    ?town p:P31 _:b1.
    _:b1 pq:P582 ?end2.
  }
  OPTIONAL { ?town wdt:P1366 ?new. }
  OPTIONAL {
    ?town p:P1366 _:b2.
    _:b2 pq:P585 ?new2.
  }
}
ORDER BY (?ref)
```

Query result

| API field of wikidata query | Ref         | town                                     | townLabel | townDescription          | openstreetmap | Coordinate                        |
| --------------------------- | ----------- | ---------------------------------------- | --------- | ------------------------ | ------------- | --------------------------------- |
| DB field                    | (skip)      | url                                      | labels    | descriptions             | openstreetmap | coordinate                        |
| Example value               | 10002040001 | http://www.wikidata.org/entity/Q17038291 | 石城里    | 臺灣宜蘭縣頭城鎮轄下的里 | 4513494       | Point(121.940277777 24.979722222) |
|                             |             |                                          |           |                          |               |                                   |




## Technical issues

### Reading the content of DOC file (MS Word 97)

The file format of earlier village files `村里代碼變動` were DOC format.The content were became garbled text after reading by [PHPOffice/PHPWord](https://github.com/PHPOffice/PHPWord) but DOCX files work well. I use the software [LIBREOFFICE](https://zh-tw.libreoffice.org/) instead. The batch commands to convert DOC to DOCX on Mac:
```bash
## Switch to the folder where doc files located e.g. 
## cd files/village_new_added 
## or cd files/village_deleted
/Applications/LibreOffice.app/Contents/MacOS/soffice --headless --convert-to docx *doc
```

### Could not find a version of package simplehtmldom/simplehtmldom matching your minimum-stability (stable)

error
```bash
% composer require simplehtmldom/simplehtmldom
You are running Composer with SSL/TLS protection disabled.


  [InvalidArgumentException]
  Could not find a version of package simplehtmldom/simplehtmldom matching your minimum-stability (stable). Require it with an
   explicit version constraint allowing its desired stability.
```

solution

```
composer config minimum-stability dev
```

References
* [php \- Composer/Packagist could not find package for minimum stability \- Stack Overflow](https://stackoverflow.com/questions/41149552/composer-packagist-could-not-find-package-for-minimum-stability)