<?php

require_once __DIR__ . '/../load.php';

class TimeClass
{

    /**
     * @param $tw_year_month_day
     * @return string
     * @throws Exception
     */
    public function convertDateFromTwEra($tw_year_month_day = ''){
        $debug = $this->debug;

        $tw_year_month_day = trim($tw_year_month_day);
        $tw_year_month_day = $this->preProcessTwYearMonth($tw_year_month_day);
        if ($debug) {
            echo '$tw_year_month_day after cleaned is: ' . print_r($tw_year_month_day, true) . PHP_EOL;
        }

        // e.g. 11006
        $pattern = "/^(\d+)(\d\d)$/";
        preg_match($pattern, $tw_year_month_day, $matches);
        if(isset($matches[1])
            && isset($matches[2])
        ){
            $tw_year = $matches[1];
            $year = (int) $tw_year + 1911;
            $month = $matches[2];
            $day = '01';
            $date = "{$year}-{$month}-{$day}";
            if (strtotime($date)) {
                return $date;
            }
        }

        $year = null;
        $month = null;
        $day = null;
        // e.g. 110.06.01
        $pattern = "/^(\d+)\.(\d{2})\.(\d{2})$/";
        preg_match($pattern, $tw_year_month_day, $matches);
        if (isset($matches[1]) && isset($matches[2]) && isset($matches[3])) {
            $tw_year = $matches[1];
            $year = (int) $tw_year + 1911;
            $month = $matches[2];
            $day = $matches[3];  // 捕捉日
        }

        $date = "{$year}-{$month}-{$day}";
        if (strtotime($date) === false) {
            $error = '日期時間轉換遭遇錯誤。';
            throw new Exception($error);
        }

        return $date;
    }

    /**
     * @param $tw_year_month
     * @return array|string|string[]|null
     */
    public function preProcessTwYearMonth($tw_year_month = ''){
        $debug = $this->debug;

        $tw_year_month = trim($tw_year_month);
        $tw_year_month = preg_replace("/(版)$/u", '', $tw_year_month);

        $pattern = "/^(\d{2,3}\.\d{2}\.\d{2})$/";
        preg_match($pattern, $tw_year_month, $matches);
        if(isset($matches[1])){
            return $matches[1];
        }

        // if contains / symbol
        $tmp = explode('/', $tw_year_month);
        if(count($tmp) > 0){
            $tw_year_month = end($tmp);
            $tw_year_month = preg_replace("/(\.txt)$/iu", '', $tw_year_month);
            $pattern = "/^(\d{3,})/"; // at least 3 digits
            preg_match($pattern, $tw_year_month, $matches);
            if(isset($matches)){
                return $matches[1];
            }
        }

        $pattern = "/^(\d+)$/";
        preg_match($pattern, $tw_year_month, $matches);
        if(isset($matches[1])){
            return $matches[1];
        }

        return $tw_year_month;
    }



}

