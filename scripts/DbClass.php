<?php


use Simplon\Mysql\Mysql;
use Simplon\Mysql\PDOConnector as PDOConnector;

require_once __DIR__ . '/../load.php';

class DbClass
{
    public $debug = false;
    public $save_log = true;
    public $page_size = 50;


    /**
     * @return Mysql
     * @throws Exception
     */
    public function getDbh(){

        $db_config = getenv('DB_CONFIG');
        $db_config = json_decode($db_config, true);

        $db_config['fetchMode'] = \PDO::FETCH_ASSOC;
        $db_config['unixSocket'] = null;
        $charset = $db_config['charset'];
        $port = $db_config['port'];

        //$pdo = PDOConnector(
        $pdo = new PDOConnector(
            $db_config['host'],     // server
            $db_config['user'],     // user
            $db_config['password'], // password
            $db_config['database']  // database
        );

        $pdoConn = $pdo->connect($charset, ['port' => $port]); // charset, options

        //
        // you could now interact with PDO for instance setting attributes etc:
        // $pdoConn->setAttribute($attribute, $value);
        //

        $dbh = new Mysql($pdoConn);

        return $dbh;

    }

    /**
     * @return array|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function getTownAndVillageNameListNeedToUpdate($requests = null){
        $debug = $this->debug;
        $dbh = $this->getDbh();

        list($sql_condition, $query_data) = $this->parseTownAndVillageCodeRequests($requests);
        if ($debug) {
            echo '$sql_condition is: ' . print_r($sql_condition, true) . PHP_EOL;
            echo '$query_data is: ' . print_r($query_data, true) . PHP_EOL;
        }


        $query = "
            SELECT * 
            FROM `main_tb`
            WHERE 1
                /*AND `village_code` NOT IN (
                    SELECT `village_code`
                    FROM `wikidata_search`
                    WHERE `status` = 1  
                        -- AND `updated_at` >= (NOW() - INTERVAL 1 MONTH)
                )*/
            {$sql_condition}
        ";
        if ($debug) {
            echo '$query is: ' . print_r($query, true) . PHP_EOL;
        }

        return $dbh->fetchRowMany($query, $query_data);
    }

    /**
     * @param string $village_code
     * @return false|string|null
     */
    public function getTownCodeFromVillageCode($village_code = ''){
        $village_code = trim($village_code);

        if($village_code === ''){
            return null;
        }

        if(strlen($village_code) !== 11){
            return null;
        }

        $pattern = "/^(\d+)/";
        preg_match($pattern, $village_code, $matches);
        if(!isset($matches[1])){
    
            $name = '從 village code 取得 town code';
            $error = 'village code 非已知格式. $village_code: ' . gettype($village_code) . print_r($village_code, true);
            $action = '檢查 village code 格式： ' . $village_code;

            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'entity' => $village_code,
                'last_error' => $error,
                'action' => $action,

            );
            $this->saveLogging($log_data);
            
        
            return null;
        }


        return substr($village_code, 0, 8);
    }



    /**
     * @param $query_data
     * @param string $tb_name
     * @param string $unique_column_name
     * @param null $return_column
     * @return mixed|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function getPkColumnValue($query_data, $tb_name = 'logging', $unique_column_name = '', $return_column = null){
        if(!array_key_exists($unique_column_name, $query_data)){
            $error = 'The element specified is not exists. $unique_column_name: ' . print_r($unique_column_name, true);
            $error .= ' $query_data: ' . print_r($query_data, true);
            throw new Exception($error);
        }

        $value_of_unique_column = $query_data[$unique_column_name];

        if(is_null($return_column)){
            $return_column = $unique_column_name;
        }

        $dbh = $this->getDbh();
        $query = "
            SELECT `{$return_column}` AS id 
            FROM `{$tb_name}` 
            WHERE `{$unique_column_name}` = :uuu
        ";
        $condition = array('uuu' => $value_of_unique_column);

        $result = $dbh->fetchRow($query, $condition);
        if(is_array($result)
            && array_key_exists('id', $result)
        ){
            return $result['id'];
        }

        return null;
    }

    /**
     * @param $query_data
     * @param string $tb_name
     * @param string $unique_columns_name
     * @param null $return_column
     * @return mixed|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function getPkColumnsValue($query_data, $tb_name = 'logging', $unique_columns_name = array(), $return_column = null){
        $debug = $this->debug;

        $sql_part_data = array();
        $conditions = array();

        if(!array_key_exists('entity', $query_data)){
            $query_data['entity'] = '';
        }

        foreach ($unique_columns_name AS $index => $unique_column_name){
            if(!array_key_exists($unique_column_name, $query_data)){
                $error = 'The element specified is not exists. $unique_column_name: ' . print_r($unique_column_name, true);
                $error .= ' $query_data: ' . print_r($query_data, true);
                throw new Exception($error);
            }

            // ASCII value of a = 97
            $ascii_value = $index + 97;
            $tmp_alias_name = chr($ascii_value);
            $alias_name = str_repeat($tmp_alias_name, 3);
            $sql_part_data[] = "`{$unique_column_name}` = :{$alias_name}";
            $conditions[$alias_name] = $query_data[$unique_column_name];
        }

        $sql_part = "AND (" . implode(" AND ", $sql_part_data) . ")";

        if ($debug) {
            echo '$sql_part is: ' . print_r($sql_part, true) . PHP_EOL;
            echo '$conditions is: ' . print_r($conditions, true) . PHP_EOL;
        }

        if(is_null($return_column)){
            $error = 'The $return_column is not specified!';
            throw new Exception($error);
        }

        $dbh = $this->getDbh();
        $query = "
            SELECT `{$return_column}` AS id 
            FROM `{$tb_name}` 
            WHERE 1
                $sql_part
        ";

        $result = $dbh->fetchRow($query, $conditions);
        if(is_array($result)
            && array_key_exists('id', $result)
        ){
            return $result['id'];
        }

        return null;
    }

    /**
     * @param array $village_data
     * @return string|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function getVillageCodeFromVillageCodeAndVillageName($village_data = array()){
        $debug = $this->debug;
        $dbh = $this->getDbh();


        if(!array_key_exists('village_code', $village_data)){
            return null;
        }

        if(!array_key_exists('village_name', $village_data)){
            //return null;
        }

        $city_and_village_name = $village_data['city_and_village_name'];

        $administrative_data = (new CrawlerClass())->getAdministrativeData($city_and_village_name);
        $village_name = $administrative_data['level3'];
        $partail_village_name = (new CrawlerClass())->replaceAdministrativeName($village_name);

        $query = "
            SELECT `village_code`
            FROM `main_tb`
            WHERE `village_code` LIKE :village_code
                AND `village_name` LIKE :village_name
        ";
        $query_data = array(
            'village_code' => $village_data['village_code'],
            'village_name' => $village_name
        );
        if ($debug) {
            echo '$query is: ' . print_r($query, true) . PHP_EOL;
            echo '$query_data is: ' . print_r($query_data, true) . PHP_EOL;
        }

        $output = $dbh->fetchColumn($query, $query_data);
        if(!is_null($output)){
            return $output;
        }

        $query = "
            SELECT `village_code`
            FROM `main_tb`
            WHERE `village_code` LIKE :village_code
                AND `village_name` LIKE :village_name
        ";
        $query_data = array(
            'village_code' => $village_data['village_code'],
            'village_name' => "%$partail_village_name%"
        );
        if ($debug) {
            echo '$query is: ' . print_r($query, true) . PHP_EOL;
            echo '$query_data is: ' . print_r($query_data, true) . PHP_EOL;
        }

        return $dbh->fetchColumn($query, $query_data);
    }

    /**
     * @param $query_data
     * @param string $unique_column_name
     * @return mixed|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function getWikidataCandidateDataFromEntity($query_data, $save_log = true){
        $debug = $this->debug;
        $dbh = $this->getDbh();

        // case 1
        $condition = array();
        $sql = "";
        if(array_key_exists('village_code', $query_data)
            && trim($query_data['village_code']) !== ''
        ){
            $sql .= "AND `village_code` LIKE :village_code" . PHP_EOL;
            $condition['village_code'] = $query_data['village_code'];
        }

        $query = "
            SELECT `wikidata_id`
            FROM `wikidata_village` 
            WHERE 1
                AND `is_village` = 1
                {$sql}
        ";

        if ($debug) {
            echo '$query is: ' . print_r($query, true) . PHP_EOL;
            echo '$condition is: ' . print_r($condition, true) . PHP_EOL;
        }

        $result = $dbh->fetchRowMany($query, $condition);

        if(is_array($result)
            && count($result) === 1
            && array_key_exists('wikidata_id', $result[0])
        ){
            return $result[0]['wikidata_id'];
        }


        // case 2
        $condition = array();
        $sql = "";
        if(array_key_exists('village_name', $query_data)
            && trim($query_data['village_name']) !== ''
        ){
            $sql .= "AND `labels` LIKE :labels" . PHP_EOL;
            $condition['labels'] = "%" . $query_data['village_name'] . "%";
        }

        if(array_key_exists('town_name', $query_data)
            && trim($query_data['town_name']) !== ''
        ){
            $sql .= "AND `descriptions` LIKE :descriptions" . PHP_EOL;
            $condition['descriptions'] = "%" . $query_data['town_name'] . "%";
        }

        $query = "
            SELECT `wikidata_id`
            FROM `wikidata_village` 
            WHERE 1
                AND `is_village` = 1
                {$sql}
        ";

        if ($debug) {
            echo '$query is: ' . print_r($query, true) . PHP_EOL;
            echo '$condition is: ' . print_r($condition, true) . PHP_EOL;
        }

        $result = $dbh->fetchRowMany($query, $condition);

        if(is_array($result)
            && count($result) === 1
            && array_key_exists('wikidata_id', $result[0])
        ){
            return $result[0]['wikidata_id'];
        }

        // case 3

        $condition = array();
        $sql = "";
        if(array_key_exists('village_name', $query_data)
            && trim($query_data['village_name']) !== ''
        ){
            $sql .= "AND `labels` LIKE :labels" . PHP_EOL;
            $condition['labels'] = "%" . $query_data['village_name'] . "%";
        }

        if(array_key_exists('town_name', $query_data)
            && trim($query_data['town_name']) !== ''
        ){
            $sql .= "AND `descriptions` LIKE :descriptions" . PHP_EOL;
            $condition['descriptions'] = "%" . $query_data['town_name'] . "%";
        }

        $query = "
            SELECT `wikidata_id`
            FROM `wikidata_village` 
            WHERE 1
                {$sql}
        ";

        if ($debug) {
            echo '$query is: ' . print_r($query, true) . PHP_EOL;
            echo '$condition is: ' . print_r($condition, true) . PHP_EOL;
        }

        $result = $dbh->fetchRowMany($query, $condition);

        if(!is_array($result)) {
            $error = '讀取「getWikidataCandidateDataFromEntity」遇到問題';
            $action = '檢查 SQL 查詢語法：'
                . print_r($query, true) . PHP_EOL
                . print_r($condition, true) . PHP_EOL
            ;

            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => null,
                'last_error' => $error,
                'action' => $action,

            );

            if($save_log){
                $this->saveLogging($log_data);
            }
        }

        if(is_array($result)
            && count($result) !== 1
        ){
            return null;
        }


        if(is_array($result)
            && array_key_exists('wikidata_id', $result[0])
        ){
            return $result[0]['wikidata_id'];
        }

    }

    /**
     * @return array|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function getWikidataIds(){
        $debug = $this->debug;
        $dbh = $this->getDbh();

        //$condition = array();

        $query = "
            SELECT CONCAT('Q', `wikidata_id`) AS qid
            FROM `wikidata_village` 
            WHERE (`labels` IS NULL
                    OR `descriptions` IS NULL
                )
        ";

        if ($debug) {
            echo '$query is: ' . print_r($query, true) . PHP_EOL;
            //echo '$condition is: ' . print_r($condition, true) . PHP_EOL;
        }

        //$result = $dbh->fetchRowMany($query, $condition);
        $result = $dbh->fetchRowMany($query);
        if ($debug) {
            echo '$result is: ' . print_r($result, true) . PHP_EOL;
        }

        if(count($result) === 0){
            return null;
        }


        $output = array();
        foreach ($result AS $row){
            $output[] = $row['qid'];
        }
        return $output;

    }

    /**
     * @param $query_data
     * @param string $unique_column_name
     * @return mixed|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function getWikidataCandidateDataFromKeywordSearch($query_data, $unique_column_name = 'village_code'){
        $debug = $this->debug;

        if(!array_key_exists($unique_column_name, $query_data)){
            $error = 'The element specified is not exists. $unique_column_name: ' . print_r($unique_column_name, true);
            $error .= ' $query_data: ' . print_r($query_data, true);
            throw new Exception($error);
        }

        $value_of_village_code = $query_data[$unique_column_name];
        $dbh = $this->getDbh();
        $query = "
            SELECT `wikidata_result` 
            FROM `wikidata_search` 
            WHERE `{$unique_column_name}` = :village_code
                AND `wikidata_totalhits` = 1
                ORDER BY CHAR_LENGTH(`keyword`) DESC 
            LIMIT 1
        ";
        $condition = array(
            'village_code' => $value_of_village_code
        );

        $result = $dbh->fetchRow($query, $condition);
        if(is_array($result)
            && array_key_exists('wikidata_result', $result)
        ){
            //return $result['wikidata_result'];


            $json_content = $result['wikidata_result'];
            if(is_null($json_content)){
                return null;
            }

            $wikidata_go = new WikidataClass();
            $wikidata_go->debug = $debug;
            $parsed_data = $wikidata_go->parseWikiDataKeywordSearchResult($json_content);

            if ($debug) {
                echo '$parsed_data is: ' . print_r($parsed_data, true) . PHP_EOL;
            }

            if(count($parsed_data) === 1){
                return $parsed_data[0]['wikidata_id'];
            }
        }

        return null;
    }

    /**
     * @param string $table_name
     * @param $insert_rows
     * @param string $unique_column
     * @param null $pk_column
     * @param array $ignore_fields
     * @return int|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function insertOrUpdateMany($table_name = '', $insert_rows = array(), $unique_column = '', $pk_column = null, $ignore_fields = array()){
        $debug = $this->debug;
        //var_dump($insert_data);

        if(trim($table_name) === ''){
            return null;
        }

        if(empty($insert_rows)){
            return 0;
        }

        $dbh = $this->getDbh();
        $total_affected_count = 0;
        $insert_data_reduced = array();
        foreach ($insert_rows AS $insert_data){
            if ($debug) {
                echo '$table_name is: ' . print_r($table_name, true) . PHP_EOL;
                echo '$unique_column is: ' . print_r($unique_column, true) . PHP_EOL;
                echo '$pk_column is: ' . print_r($pk_column, true) . PHP_EOL;
            }

            $id = $this->getPkColumnValue($insert_data, $table_name, $unique_column, $pk_column);
            if ($debug) {
                echo '$id is: ' . print_r($id, true) . PHP_EOL;
                echo 'type of $id is: ' . gettype($id) . PHP_EOL;
                echo '$insert_data is: ' . print_r($insert_data, true) . PHP_EOL;
            }

            $is_insert = false;
            if(is_null($id)){
                $is_insert = true;
            }

            if($is_insert){
                $insert_data['created_at'] = date('Y-m-d H:i:s');
                //$last_id = $dbh->insert($table_name, $insert_data, true);
                //return $last_id;
                //$insert_data_reduced[] = $insert_data;

                $last_id = $dbh->insert($table_name, $insert_data);
                if ($debug) {
                    echo '$last_id of insert is: ' . print_r($last_id, true) . PHP_EOL;
                }
                //return $last_id;

                $total_affected_count += count($last_id);

            }else{
                foreach ($ignore_fields AS $ignore_field){
                    if(in_array($ignore_field, $insert_data)){
                        $index = array_search($ignore_field, $insert_data);
                        unset($insert_data[$index]);
                    }
                }

                $conds = array($unique_column => $insert_data[$unique_column]);
                $result = $dbh->update($table_name, $conds, $insert_data);
                if ($debug) {
                    echo '$result of update is: ' . gettype($result) . ' ' . print_r($result, true) . PHP_EOL;
                }

                if($result){
                    $total_affected_count++;
                }
            }


        }


        return $total_affected_count;
    }


    /**
     * @param $log_data
     * @return bool|int|void
     * @throws \Simplon\Mysql\MysqlException
     */
    public function saveWikiDataResult($insert_data, $ignore_fields = array('keyword')){
        //var_dump($insert_data);

        $id = $this->getPkColumnValue($insert_data, 'wikidata_search', 'keyword', 'id');

        $insert_or_update = false;
        if(is_null($id)){
            $insert_or_update = true;
        }

        $dbh = $this->getDbh();

        if($insert_or_update){
            $insert_data['created_at'] = date('Y-m-d H:i:s');;
            $last_id = $dbh->insert('wikidata_search', $insert_data, true);
            return $last_id;
        }

        foreach ($ignore_fields AS $ignore_field){
            if(in_array($ignore_field, $insert_data)){
                $index = array_search($ignore_field, $insert_data);
                unset($insert_data[$index]);
            }
        }

        $conds = array('id' => $id);
        $result = $dbh->update('wikidata_search', $conds, $insert_data);
    }

    /**
     * @param $conditions
     * @return int|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function saveWikidataCandidateEntriesData($conditions){
        $debug = $this->debug;
        list($sql_condition, $query_data) = $this->parseTownAndVillageCodeRequests($conditions);

        $dbh = $this->getDbh();

        // AND COALESCE (`wikidata_id`, `wikidata_id_candidate`, '') LIKE ''
        $query = "
            SELECT `town_name`, `village_name`, `village_code`
            FROM `main_tb`
            WHERE 1
                {$sql_condition}
                
        ";
        $query_result = $dbh->fetchRowMany($query, $query_data);
        if ($debug) {
            echo 'count of $query_result is: ' . print_r(count($query_result), true) . PHP_EOL;
        }
        $total_affected_count = 0;
        foreach ($query_result AS $row){
            $total_affected_count += $this->saveWikidataCandidateEntryData($row, 'village_code');
        }

        return $total_affected_count;
    }

    /**
     * @param $query_data
     * @param string $unique_column_name
     * @return int|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function saveWikidataCandidateEntryData($query_data, $unique_column_name = 'village_code'){
        $debug = $this->debug;

        if(!array_key_exists($unique_column_name, $query_data)){
            $error = 'The element specified is not exists. $unique_column_name: ' . print_r($unique_column_name, true);
            $error .= ' $query_data: ' . print_r($query_data, true);
            throw new Exception($error);
        }

        $wikidata_id_candidate = $this->getWikidataCandidateDataFromEntity($query_data);

        if(is_null($wikidata_id_candidate)){
            return 0;
        }

        $dbh = $this->getDbh();

        $conds = array($unique_column_name => $query_data[$unique_column_name]);
        $insert_data = array(
            'wikidata_id_candidate' => $wikidata_id_candidate
        );

        if ($debug) {
            echo '$conds is: ' . print_r($conds, true) . PHP_EOL;
            echo '$insert_data is: ' . print_r($insert_data, true) . PHP_EOL;
        }

        $total_affected_count = 0;
        // save to main_tb
        $result = $dbh->update('main_tb', $conds, $insert_data);
        if($result){
            $total_affected_count++;
        }

        // save to wikidata_village
        /*
        $table_name = 'wikidata_village';
        $insert_rows = $parsed_data;
        $unique_column = 'wikidata_id';
        $pk_column = 'wikidata_id';
        $ignore_fields = array('created_at', $unique_column_name);
        $total_affected_count += $this->insertOrUpdateMany($table_name, $insert_rows, $unique_column, $pk_column, $ignore_fields);

        */
        return $total_affected_count;
    }


    /**
     * @param null $requests
     * @return array
     */
    public function parseTownAndVillageCodeRequests($requests = array(), $table_alias = '', $is_limit_size = true){

        if(trim($table_alias) !== ''){
            $table_alias = "{$table_alias}.";
        }

        $size = $this->page_size;
        if(array_key_exists('size', $requests)
            && trim($requests['size']) !== ''
        ){
            $size = trim($requests['size']);
            $size = (int) $size;
        }

        $sql_limit = "";
        if($is_limit_size
            && !is_null($size)
            && is_int($size)
            && $size > 0
        ){
            $sql_limit = "LIMIT {$size}";
        }

        $offset = null;
        if(array_key_exists('page_number', $requests)
            && trim($requests['page_number']) !== ''
        ){
            $page_number = trim($requests['page_number']);
            $page_number = (int) $page_number;
            $offset = ($page_number - 1) * $size;
        }
        /*if(array_key_exists('offset', $requests)
            && trim($requests['offset']) !== ''
        ){
            $offset = trim($requests['offset']);
            $offset = (int) $offset;
        }*/

        $sql_offset = "";
        //$sql_offset = "OFFSET 0";
        if($is_limit_size
            && !is_null($offset)
            && is_int($offset)
            && $offset > 0
        ){
            $sql_offset = "OFFSET {$offset}";
        }


        $sql_city = "";
        $query_data = array();
        if(array_key_exists('city', $requests)
            && trim($requests['city']) !== ''
        ){
            $city = trim($requests['city']);
            $city_with_simple_tw = str_replace('臺', '台', $city);
            $city_with_complex_tw = str_replace('台', '臺', $city);
            $sql_city = "
                AND ({$table_alias}`town_name` LIKE :city 
                    OR {$table_alias}`town_name` LIKE :city_with_simple_tw
                    OR {$table_alias}`town_name` LIKE :city_with_complex_tw
                    )";
            $query_data['city'] = "%{$city}%";
            $query_data['city_with_simple_tw'] = "%{$city_with_simple_tw}%";
            $query_data['city_with_complex_tw'] = "%{$city_with_complex_tw}%";
        }


        $sql_search = "";
        if(array_key_exists('q', $requests)
            && trim($requests['q']) !== ''
        ){
            $q = trim($requests['q']);
            $q_with_simple_tw = str_replace('臺', '台', $q);
            $q_with_complex_tw = str_replace('台', '臺', $q);

            $qid_not_begin_with_q = str_replace('Q', '', $q);
            $qid_begin_with_q = 'Q' . str_replace('Q', '', $q);

            $sql_search = "
                AND (
                    {$table_alias}`town_code` LIKE :q_city_code
                    
                    OR {$table_alias}`town_name` LIKE :q_city 
                    OR {$table_alias}`town_name` LIKE :q_city_with_simple_tw
                    OR {$table_alias}`town_name` LIKE :q_city_with_complex_tw
                    
                    OR {$table_alias}`village_code` LIKE :q_village_code
                    OR {$table_alias}`village_name` LIKE :q_village_name
                    
                    OR {$table_alias}`wikidata_id_candidate` LIKE :q_wikidata_id_not_begin_with_q
                    OR {$table_alias}`wikidata_id_candidate` LIKE :q_wikidata_id_begin_with_q
                 
                    OR DATE_FORMAT(a.`added_at`, '%Y/%m') LIKE :q_added_at
                    OR DATE_FORMAT(a.`deleted_at`, '%Y/%m') LIKE :q_deleted_at
                    
                    OR w.`labels` LIKE :q_wikidata_label
                    OR w.`coordinate` LIKE :q_coordinate
                    OR w.`osm_id` LIKE :q_osm_id
                    
                    OR CASE
                    WHEN w.`is_village` = 1 THEN '已建立鄉鎮條目'
                    WHEN w.`is_village` IS NULL
                    AND       a.`wikidata_id_candidate` IS NOT NULL THEN '電腦選的對應鄉鎮條目'
                    ELSE '未建立且無推薦鄉鎮條目'
          END             LIKE :q_status
                    
                    )";
            $query_data['q_city_code'] = $q;
            $query_data['q_city'] = "%{$q}%";
            $query_data['q_city_with_simple_tw'] = "%{$q_with_simple_tw}%";
            $query_data['q_city_with_complex_tw'] = "%{$q_with_complex_tw}%";

            $query_data['q_village_code'] = $q;
            $query_data['q_village_name'] = "%{$q}%";

            $query_data['q_wikidata_id_not_begin_with_q'] = $qid_not_begin_with_q;
            $query_data['q_wikidata_id_begin_with_q'] = $qid_begin_with_q;



            $query_data['q_added_at'] = $q;
            $query_data['q_deleted_at'] = $q;

            $query_data['q_wikidata_label'] = "%{$q}%";
            $query_data['q_coordinate'] = "%{$q}%";
            $query_data['q_osm_id'] = $q;
            $query_data['q_status'] = "%{$q}%";
        }

        $order_by_column = 'added_at';
        $allowed_order_by_column = array(
            'city', 'city_code', 'village', 'village_code', 'add', 'delete'
        );

        if(array_key_exists('sort', $requests)
            && trim($requests['sort']) !== ''
        ){
            $order_by_column = trim($requests['sort']);
            //$order_by_column = strtolower($order_by_column);
        }

        if(!is_null($order_by_column)
            //&& in_array($order_by_column, $allowed_order_by_column)
        ){
            switch ($order_by_column) {
                case "省市縣市代碼":
                    $order_by_column = 'a.`town_code`';
                    break;
                case "省市縣市":
                    $order_by_column = 'a.`town_name`';
                    break;
                case "村里代碼":
                    $order_by_column = 'a.`village_code`';
                    break;
                case "村里名稱":
                    $order_by_column = 'a.`village_name`';
                    break;
                case "維基數據ID":
                    $order_by_column = 'a.`wikidata_id_candidate`';
                    break;
                case "維基數據標籤":
                    $order_by_column = 'w.`labels`';
                    break;
                case "新增月份":
                    $order_by_column = "a.`added_at`";
                    break;
                case "刪除月份":
                    $order_by_column = "a.`deleted_at`";
                    break;
                case "經緯度":
                    $order_by_column = "w.`coordinate`";
                    break;
                case "開放街圖id":
                case "開放街圖ID":
                    $order_by_column = 'w.osm_id';
                    break;
                case "狀態":
                    $order_by_column =
                        "
                    CASE
                        WHEN w.`is_village` = 1 THEN '已建立鄉鎮條目'
                        WHEN w.`is_village` IS NULL AND a.`wikidata_id_candidate` IS NOT NULL  THEN '電腦選的對應鄉鎮條目'  
                        ELSE '未建立且無推薦鄉鎮條目' 
                    END
                    ";
                    break;
                case "city":
                    $order_by_column = 'town_name';
                    break;
                case "city_code":
                    $order_by_column = 'town_code';
                    break;
                case "village":
                    $order_by_column = 'village_code';
                    break;
                case "village_code":
                    $order_by_column = 'village_name';
                    break;
                case "add":
                    $order_by_column = 'added_at';
                    break;
                case "delete":
                    $order_by_column = 'deleted_at';
                    break;

            }
        }

        $order_by = 'DESC';
        if(array_key_exists('sort_order', $requests)
            && trim($requests['sort_order']) !== ''
        ){
            $order_by_from_requests = trim($requests['sort_order']);
            $order_by_from_requests = strtolower($order_by_from_requests);
            if(in_array($order_by_from_requests, array('desc', 'asc'))
            ){
                $order_by = $order_by_from_requests;
            }
        }


        $sql_order_by_column_is_not_null = "";
        if($order_by_column !== 'added_at'
            && $order_by !== 'desc'
        ){
            $sql_order_by_column_is_not_null = "AND {$order_by_column} IS NOT NULL";
        }

        $sql_year = '';
        if(array_key_exists('year', $requests)
            && trim($requests['year']) !== ''
        ){
            $year = trim($requests['year']);
            $year = (int) $year;
            $sql_year = "
                AND (
                    YEAR({$table_alias}`added_at`) = :year_for_add
                    OR YEAR({$table_alias}deleted_at) = :year_for_delete
                )
            ";
            $query_data['year_for_add'] = $year;
            $query_data['year_for_delete'] = $year;
        }


        $sql_condition = "
            {$sql_city}
            {$sql_search}
            {$sql_year}
            {$sql_order_by_column_is_not_null}
            
            ORDER BY {$order_by_column} {$order_by}
            {$sql_limit}
            {$sql_offset}
        ";

        return array(
            $sql_condition,
            $query_data,
        );
    }

    /**
     * @param array $insert_data
     * @return bool|int
     * @throws \Simplon\Mysql\MysqlException
     */
    public function saveAuditLog($insert_data = array()){

        $columns_name = array('village_code', 'column_name', 'before', 'after', 'created_by');
        $columns_name_not_null = array('village_code', 'column_name', 'created_by');

        foreach ($columns_name AS $column_name){
            if(!array_key_exists($column_name, $insert_data)){
                $error = 'The $column_name element is not exists in $insert_data.';
                throw new Exception($error);
            }
        }

        foreach ($columns_name_not_null AS $column_name){
            if(array_key_exists($column_name, $insert_data)
                && trim($insert_data[$column_name]) === ''
            ){
                $error = 'The $column_name element of $insert_data is empty.';
                throw new Exception($error);
            }
        }

        $dbh = $this->getDbh();

        if(!array_key_exists('created_at', $insert_data)){
            $insert_data['created_at'] = date('Y-m-d H:i:s');
        }

        $last_id = $dbh->insert('audit_log', $insert_data, true);
        return $last_id;
    }

    /**
     * @param $insert_data
     * @param string[] $ignore_fields
     * @return bool|int
     * @throws \Simplon\Mysql\MysqlException
     */
    public function saveLogging($insert_data, $ignore_fields = array('short_name')){
        //var_dump($insert_data);

        $unique_columns_name = array('short_name', 'name', 'entity');
        $id = $this->getPkColumnsValue($insert_data, 'logging', $unique_columns_name, 'id');

        $insert_or_update = false;
        if(is_null($id)){
            $insert_or_update = true;
        }

        $dbh = $this->getDbh();

        if($insert_or_update){
            $insert_data['created_at'] = date('Y-m-d H:i:s');;
            $last_id = $dbh->insert('logging', $insert_data, true);
            return $last_id;
        }

        foreach ($ignore_fields AS $ignore_field){
            if(in_array($ignore_field, $insert_data)){
                $index = array_search($ignore_field, $insert_data);
                unset($insert_data[$index]);
            }
        }

        $conds = array('id' => $id);
        $result = $dbh->update('logging', $conds, $insert_data);
        return $result;
    }

    /**
     * @return int
     * @throws \Simplon\Mysql\MysqlException
     */
    public function saveTownCodeFromFile(){
        $crawler_go = new CrawlerClass();
        $insert_data = $crawler_go->parseTownCodeFile();

        //var_dump($insert_data);

        $dbh = $this->getDbh();
        $last_id = $dbh->insertMany('towncode', $insert_data, true);
        $total_affected_count = count($last_id);
        //var_dump($last_id); // 50 || bool
        return $total_affected_count;
    }

    /**
     * @throws \Simplon\Mysql\MysqlException
     */
    public function saveTownCodeFromVillageCode(){
        $debug = $this->debug;
        $dbh = $this->getDbh();

        $query = "
            SELECT `village_code`
            FROM `main_tb`
            WHERE `town_code` IS NULL
                -- AND `village_code` REGEXP '^[0-9]+'
        ";
        $records = $dbh->fetchRowMany($query);
        if ($debug) {
            echo '$records is: ' . print_r($records, true) . PHP_EOL;
        }

        if(empty($records)){
            return 0;
        }

        $total_affected_count = 0;
        foreach ($records AS $record){
            $village_code = trim($record['village_code']);
            $town_code = $this->getTownCodeFromVillageCode($village_code);
            if(!is_null($town_code)){
                $conds = [
                    'village_code' => $record['village_code'],
                ];
                $insert_data = array(
                    'town_code' => $town_code,
                );
                $result = $dbh->update('main_tb', $conds, $insert_data);
                if($result){
                    $total_affected_count++;
                }
            }

        }

        return $total_affected_count;
    }

    /**
     * @return int
     * @throws \Simplon\Mysql\MysqlException
     */
    public function saveVillageCodeFromFile(){
        $crawler_go = new CrawlerClass();
        $insert_data = $crawler_go->parseVillageCodeFile();

        //var_dump($insert_data);

        $dbh = $this->getDbh();
        $last_id = $dbh->insertMany('main_tb', $insert_data, true);
        $total_affected_count = count($last_id);
        //var_dump($last_id); // 50 || bool
        return $total_affected_count;
    }

    /**
     * @param array $records
     * @param string $time_column
     * @return int|void
     * @throws \Simplon\Mysql\MysqlException
     */
    public function saveVillageHistoryRecord($record = array(), $time_column = 'added_at', $label = null){
        $debug = $this->debug;
        $save_log = $this->save_log;

        if(is_null($record)){
            return 0;
        }

        if(empty($record)){
            return 0;
        }

        if ($debug) {
            echo '$record is: ' . print_r($record, true) . PHP_EOL;
        }

        if(!array_key_exists($time_column, $record)){
            $error = 'The element is not exists in $row: ' . print_r($time_column, true);
            throw new Exception($error);
        }

        $village_code = $this->getPkColumnValue($record, 'main_tb', 'village_code', 'village_code');
        if ($debug) {
            echo '$village_code is: ' . gettype($village_code) . ' ' . print_r($village_code, true) . PHP_EOL;
        }

        $dbh = $this->getDbh();

        if(is_null($village_code)){

            $insert_data = array();
            $town_code = $this->getTownCodeFromVillageCode($record['village_code']);
            if(!is_null($town_code)){
                $insert_data['town_code'] = $town_code;
                $insert_data['village_code'] = $record['village_code'];
                $insert_data['village_name'] = $record['city_and_village_name'];
                $insert_data[$time_column] = $record[$time_column];
                $insert_data['created_at'] = date('Y-m-d H:i:s');
                $result = $dbh->insert('main_tb', $insert_data);
                if($result){
                    return 1;
                }
            }
        }

        $village_code_verified = $this->getVillageCodeFromVillageCodeAndVillageName($record);
        if(!is_null($village_code_verified)){
            $conds = array('village_code' => $record['village_code']);

            $insert_data = array();
            $insert_data['village_code'] = $record['village_code'];
            $insert_data[$time_column] = $record[$time_column];
            $result = $dbh->update('main_tb', $conds, $insert_data);
            if($result){
                return 1;
            }
        }else{
            $name = '儲存村里變動檔案';
            $error = '變動檔案的村里代碼與村里名稱對應不到總表的村里代碼.';
            $error .= ' ' . $time_column;
            $error .= " 檔名: " . $label;
            $action = '檢查變動檔案的村里代碼與村里名稱：' . print_r($record, true);

            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'entity' => $record['city_and_village_name'],
                'last_error' => $error,
                'action' => $action,

            );
            $this->saveLogging($log_data);
            //throw new Exception($error);
            return null;
        }


        return 0;
    }


    /**
     * @return array|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function getMainTableData($requests = array()){

        $query_data = array();
        $table_alias = 'a';
        list($sql_condition, $query_data) = $this->parseTownAndVillageCodeRequests($requests, $table_alias, true);

        $error = '$sql_condition: ' . print_r($sql_condition, true) . PHP_EOL;
        $error .= '$query_data: ' . print_r($query_data, true) . PHP_EOL;
        error_log($error);

        $dbh = $this->getDbh();

        // a.`town_code` AS 省市縣市代碼,
        //-- a.`wikidata_id`,
        $query = "
            SELECT  
                   a.`town_name` AS 省市縣市, 
                   a.`village_code` AS 村里代碼, 
                   a.`village_name` AS 村里名稱,
                   a.`wikidata_id_candidate` AS 維基數據ID,
                   w.`labels` AS 維基數據標籤,
                   DATE_FORMAT(a.`added_at`, '%Y/%m') AS 新增月份,
                   DATE_FORMAT(a.`deleted_at`, '%Y/%m/%d') AS 刪除月份,
                   w.`coordinate` AS 經緯度,
                   w.`search_snippet`,
                   w.`osm_id` AS 開放街圖ID,
                   w.`url`,
                   CASE
                        WHEN w.`is_village` = 1 THEN '已建立鄉鎮條目'
                        WHEN w.`is_village` IS NULL AND a.`wikidata_id_candidate` IS NOT NULL  THEN '電腦選的對應鄉鎮條目'  
                        ELSE '未建立且無推薦鄉鎮條目' 
                   END AS 狀態
            FROM `main_tb` AS a
            
            LEFT JOIN `wikidata_village` AS w
            ON a.`wikidata_id_candidate` = w.`wikidata_id`
            
            WHERE 1
                {$sql_condition}
        ";

        //$error = '$query: ' . print_r($query, true) . PHP_EOL;
        //error_log($error);
        //echo '$query: ' . $query . PHP_EOL;
        //echo '$query_data: ' . print_r($query_data, true) . PHP_EOL;
        
        return $dbh->fetchRowMany($query, $query_data);
    }

    /**
     * @param $requests
     * @return array|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function getTotalSizeOfTownAndVillageCode($requests = array()){

        $table_alias = 'a';
        list($sql_condition, $query_data) = $this->parseTownAndVillageCodeRequests($requests, $table_alias, false);

        $dbh = $this->getDbh();

        $query = "
            SELECT COUNT(*) AS count
            FROM `main_tb` AS a
            
            LEFT JOIN `wikidata_village` AS w
            ON a.`wikidata_id_candidate` = w.`wikidata_id`
            
            WHERE 1
                {$sql_condition}
        ";

        $result = $dbh->fetchColumn($query, $query_data);
        //var_dump($result);
        return $result;
    }

    /**
     * @return int
     * @throws \Simplon\Mysql\MysqlException
     */
    public function saveTownCodeAndTownName(){
        $crawler_go = new CrawlerClass();
        $parsed_data = $crawler_go->parseTownCodeFile();

        $dbh = $this->getDbh();

        $total_affected_count = 0;
        foreach ($parsed_data AS $row){

            $town_code = $row['town_code'];
            $conditions = array('town_code' => $town_code);

            $town_name = $row['town_name'];
            $insert_data = array('town_name' => $town_name);

            $result = $dbh->update('main_tb', $conditions, $insert_data);

            if($result){
                $total_affected_count++;
            }
        }

        return $total_affected_count;
    }

    /**
     * @return array|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function testFetchRowMany(){
        $dbh = $this->getDbh();

        $query = "SELECT * FROM `main_tb` LIMIT 5";
        return $dbh->fetchRowMany($query);

    }
}

