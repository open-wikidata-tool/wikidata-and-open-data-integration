<?php

require_once __DIR__ . '/../load.php';

class ReaderClass
{
    public $debug = false;
    public $save_log = true;

    public function clean($text_content = ''){
        return str_replace(json_decode('"\u3000"'), "", $text_content);;
    }

    /**
     * @param string $string
     * @return false|string
     */
    public function detectEncoding($string = ''){
        return mb_detect_encoding($string, "BIG5, UTF-8");
    }

    /**
     * @param string $file_path
     * @return mixed|string|void
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function getFileContentFromDocx($file_path = ''){
        $debug = $this->debug;
        $save_log = $this->save_log;

        $db_go = new DbClass();

        if(!file_exists($file_path)){
            $error = 'The $file_path is not exists. $file_path: ' . basename($file_path);
            throw new Exception($error);
        }

        $phpWord = \PhpOffice\PhpWord\IOFactory::load($file_path);

        $tmpHandle = tmpfile();
        $metaDatas = stream_get_meta_data($tmpHandle);
        $tempfile = $metaDatas['uri'];


        $folder_path_of_tempfile = dirname($tempfile);
        if(!is_writable($folder_path_of_tempfile)){
            $error = 'The $folder_path_of_tempfile was NOT writable: ' . $folder_path_of_tempfile . PHP_EOL;

            if($save_log){
                $name = 'DOCX 轉檔成 HTML';
                $action = '檢查暫存目錄權限';
                $log_data = array(
                    'short_name' => __FUNCTION__,
                    'name' => $name,
                    'last_error' => $error,
                    'action' => $action,

                );
                $db_go->saveLogging($log_data);
            }

            throw new Exception($error);
        }

        // Saving the document as HTML file...
        // if docx file
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');

        $objWriter->save($tempfile);

        if(!is_file($tempfile)){
            $error = 'The file was NOT converted: ' . $tempfile . PHP_EOL;

            if($save_log){
                $name = 'DOCX 轉檔成 HTML';
                $action = '檢查暫存檔案權限';
                $log_data = array(
                    'short_name' => __FUNCTION__,
                    'name' => $name,
                    'last_error' => $error,
                    'action' => $action,

                );
                $db_go->saveLogging($log_data);
            }

            throw new Exception($error);
        }

        $html_content = file_get_contents($tempfile); // contains HTML tags

        if ($debug) {
            echo '$html_content is: ' . print_r($html_content, true) . PHP_EOL;
        }

        // Create DOM from string
        $html = str_get_html($html_content);

        // Remove title tag because it generated by PHPWORD
        $html->find('title', 0)->innertext = '';

        if ($debug) {
            echo '$html_content edited is: ' . print_r($html, true) . PHP_EOL;
        }

        $text_content = $html->plaintext;
        $text_content = $this->clean($text_content);
        $text_content = trim($text_content);

        // Removes the tmp file
        fclose($tmpHandle);

        return $text_content;

    }

    /**
     * @param string $file_path
     * @return array|false|string
     */
    public function getFileContentFromText($file_path = ''){
        $debug = $this->debug;

        $file_content = file_get_contents($file_path);

        $encoding = $this->detectEncoding($file_content);

        if ($debug) {
            echo '$encoding is: ' . print_r($encoding, true) . PHP_EOL;
        }

        // convert to utf-8 from big5
        if($encoding === 'BIG-5'){
            $file_content = mb_convert_encoding($file_content, "UTF-8", "BIG-5");
        }

        $file_content = $this->clean($file_content);

        return $file_content;
    }

    /**
     * @param string $folder_path
     * @param null $pattern
     * @return array
     * @throws Exception
     */
    public function getFileList($folder_path = '', $pattern = null){
        if(!is_dir($folder_path)){
            $error = 'The $folder_path is not folder: ' . $folder_path;
            throw new Exception($error);
        }


        $file_list = array_diff(scandir($folder_path), array('..', '.'));

        $output = array();
        foreach ($file_list AS $file_path){
            $file_name = basename($file_path);
            if(is_null($pattern)){
                $output[] = $folder_path . $file_path;
            }else{
                preg_match($pattern, $file_name, $matches);
                if(isset($matches[1])){
                    $output[] = $folder_path . $file_path;
                }
            }
        }

        return $output;
    }

    /**
     * @param string $file_content
     * @return false|string[]
     */
    public function parseCsvFile($file_content = ''){

        // convert to utf-8 from big5
        $encoding = $this->detectEncoding($file_content);
        if($encoding === 'BIG-5'){
            $file_content = mb_convert_encoding($file_content, "UTF-8", "BIG-5");
        }

        $return_symbol_list = array("\n","\r","\r\n");
        $file_content = str_replace($return_symbol_list,PHP_EOL, $file_content);
        $file_data = explode(PHP_EOL, $file_content);

        $output = array();
        foreach ($file_data AS $row){
            if(trim($row) !== ''){
                $output[] = str_getcsv($row);
            }

        }

        return $output;
    }
}
