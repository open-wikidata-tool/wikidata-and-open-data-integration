<?php

require_once __DIR__ . '/../load.php';

class OSMClass
{
    public $debug = false;

    /**
     * @param $point
     * @return mixed|void|null
     */
    public function convertUrlFromId($id = ''){

        $id = trim($id);
        $pattern = "/^(\d+)$/";
        preg_match($pattern, $id, $matches);
        if(!isset($matches[1])){
            return null;
        }

        // https://www.openstreetmap.org/relation/4012321
        return "https://www.openstreetmap.org/relation/{$id}";
    }

    /**
     * @param $point
     * @return string|null
     */
    public function convertUrlFromPoint($point = ''){
        $coordinate_data = $this->getCoordinateDataFromPoint($point);
        if(is_null($coordinate_data)){
            return null;
        }

        // 緯度在前，經度在後
        list($latitude, $longitude) = $coordinate_data;

        // https://www.openstreetmap.org/search?query=26.15386%2C%20119.94974#map=10/26.15386/119.94974
        return "https://www.openstreetmap.org/search?query={$longitude}%2C%20{$latitude}#map=14/{$longitude}/{$latitude}";
    }

    /**
     * @param $point
     * @return array|void|null
     */
    public function getCoordinateDataFromPoint($point = ''){
        $debug = $this->debug;
        // e.g. Point(120.644 23.1625)
        $pattern = "/(point)/iu";
        preg_match($pattern, $point, $matches);
        if(!isset($matches[1])){
            return null;
        }

        $pattern = "/([\d\.]+)\s+([\d\.]+)/iu";
        preg_match($pattern, $point, $matches);
        if ($debug) {
            echo '$matches is: ' . print_r($matches, true) . PHP_EOL;
        }

        if(isset($matches[1])
            && isset($matches[2])
        ){

            return array($matches[1], $matches[2]);
        }
    }
}

