<?php

require_once __DIR__ . '/../load.php';

class WikidataClass
{
    public $debug = false;
    private $endpointUrl = 'https://query.wikidata.org/sparql';
    public $enforce_update_downloaded_file = false;

    // WikiData
    public $wikidata_folder_path_of_keyword_search = __DIR__ . '/../files/wikidata_cache_keyword_search/';
    public $wikidata_folder_path_of_ids = __DIR__ . '/../files/wikidata_cache_ids/';
    public $wikidata_file_path_of_query = __DIR__ . '/../files/wikidata_query.json';
    private $wikidata_request_size = 50;

    public function __construct(string $endpointUrl = '')
    {
        if(trim($endpointUrl) !== ''){
            $this->endpointUrl = $endpointUrl;
        }
    }

    /**
     * @return array|null
     */
    public function crawlAndSaveQueryResult($save_to_db = true){
        $debug = $this->debug;

        $file_path = __DIR__ . '/../files/wikidata_query.json';

        $db_go = new DbClass();
        $db_go->debug = $debug;

        $sparqlQueryString = <<< 'SPARQL'
SELECT DISTINCT ?village_code ?town ?townLabel ?townDescription ?village ?villageLabel ?villageDescription ?startdate ?enddate ?start2 ?end2 ?new ?newLabel ?new2 ?osm_id ?coordinate WHERE {
  ?village wdt:P31 wd:Q7930614;
    (wdt:P131+) wd:Q865.
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE], zh-tw, zh". }
  OPTIONAL { ?village wdt:P402 ?osm_id. }
  OPTIONAL { ?village wdt:P625 ?coordinate. }
  OPTIONAL { ?village wdt:P5020 ?village_code. }
  OPTIONAL { ?village wdt:P131 ?town. }
  OPTIONAL { ?village wdt:P571 ?startdate. }
  OPTIONAL { ?village wdt:P576 ?enddate. }
  OPTIONAL {
    ?village p:P31 _:b0.
    _:b0 pq:P580 ?start2.
  }
  OPTIONAL {
    ?village p:P31 _:b1.
    _:b1 pq:P582 ?end2.
  }
  OPTIONAL { ?village wdt:P1366 ?new. }
  OPTIONAL {
    ?village p:P1366 _:b2.
    _:b2 pq:P585 ?new2.
  }
}
ORDER BY (?village_code)
SPARQL;


        $is_update = (new CrawlerClass())->isOkToUpdate($file_path);
        if($is_update){
            $queryResult = $this->query($sparqlQueryString);

            //echo 'data type of $queryResult: ' . gettype($queryResult) . PHP_EOL;
            //var_export($queryResult);
            // > array

            $file_content = json_encode($queryResult);
            file_put_contents($file_path, $file_content);

        }else{
            $file_content = file_get_contents($file_path);
        }


        $parsed_data = $this->parseWikiDataQueryResult($file_content);
        if ($debug) {
            echo '$parsed_data is: ' . print_r($parsed_data, true) . PHP_EOL;
        }

        if($save_to_db === false){
            return 0;
        }

        // save to wikidata_village
        $table_name = 'wikidata_village';
        $insert_rows = $parsed_data;
        $unique_column = 'wikidata_id';
        $pk_column = 'wikidata_id';
        $ignore_fields = array('created_at', $unique_column);
        $total_affected_count = $db_go->insertOrUpdateMany($table_name, $insert_rows, $unique_column, $pk_column, $ignore_fields);
        return $total_affected_count;

    }

    public function query(string $sparqlQuery): array
    {

        $opts = [
            'http' => [
                'method' => 'GET',
                'header' => [
                    'Accept: application/sparql-results+json',
                    'User-Agent: WDQS-example PHP/' . PHP_VERSION, // TODO adjust this; see https://w.wiki/CX6
                ],
            ],
        ];
        $context = stream_context_create($opts);

        $url = $this->endpointUrl . '?query=' . urlencode($sparqlQuery);
        $response = file_get_contents($url, false, $context);
        return json_decode($response, true);
    }


    /**
     * @param $ids
     * @return mixed|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function crawlApiGivenIds($ids = array()){
        $debug = $this->debug;

        if ($debug) {
            echo '$ids is: ' . print_r($ids, true) . PHP_EOL;
        }

        if(empty($ids)){
            return null;
        }

        $db_go = new DbClass();

        if ($debug) {
            echo 'count of $ids is: ' . print_r(count($ids), true) . PHP_EOL;
        }

        $ids_chunks = array_chunk($ids, 50);
        $total_affected_count = 0;
        foreach ($ids_chunks AS $ids_chunk){
            if ($debug) {
                echo 'count of $ids_chunk is: ' . print_r(count($ids_chunk), true) . PHP_EOL;
            }
            $file_path = $this->getFilePathOfWikiDataApiResultGivenIds($ids_chunk);

            $encoded_ids = implode('|', $ids_chunk);
            $url = "https://www.wikidata.org/w/api.php?action=wbgetentities&format=json&ids={$encoded_ids}";
            if ($debug) {
                echo '$ids_chunk is: ' . print_r($ids_chunk, true) . PHP_EOL;
                echo '$url is: ' . print_r($url, true) . PHP_EOL;
            }

            $crawler_go = new CrawlerClass();
            $result = $crawler_go->downloadFile($url, $file_path, 'WikiData API 資料');


            $name = '下載 WikiData API 資料';


            if($result === false){
                $error = '未順利下載 WikiData API 資料！';
                $action = '檢查 WikiData API endpoint：' . $url;

                $log_data = array(
                    'short_name' => __FUNCTION__,
                    'name' => $name,
                    'entity' => $encoded_ids,
                    'last_error' => $error,
                    'action' => $action,

                );
                $db_go->saveLogging($log_data);
                //throw new Exception($error);
                return null;
            }

            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'entity' => $encoded_ids,
                'last_error' => null,
                'action' => null,

            );
            //$db_go->saveLogging($log_data);


            $random_seconds = rand(1, 5);
            if ($debug) {
                echo 'sleep $random_seconds is: ' . print_r($random_seconds, true) . PHP_EOL;
            }
            sleep($random_seconds);

            // save to db
            $label = $encoded_ids;
            $save_log = true;
            $parsed_data = $this->parseApiIdsResult($result, $label, $save_log);
            if ($debug) {
                echo '$parsed_data is: ' . print_r($parsed_data, true) . PHP_EOL;
            }

            if(is_array($parsed_data)
                && !empty($parsed_data)
            ){
                // save to wikidata_village
                $table_name = 'wikidata_village';
                $insert_rows = $parsed_data;
                $unique_column = 'wikidata_id';
                $pk_column = 'wikidata_id';
                $ignore_fields = array('created_at', $unique_column);
                $total_affected_count += $db_go->insertOrUpdateMany($table_name, $insert_rows, $unique_column, $pk_column, $ignore_fields);
            }


        }

        return $total_affected_count;
    }

    /**
     * @param $keyword
     * @param $village_code
     * @return mixed|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function crawlApiGivenKeyword($keyword = '', $village_code = ''){
        $debug = $this->debug;
        $file_path = $this->getFilePathOfWikiDataApiResultGivenKeyword($keyword);

        $encoded_keyword = urlencode($keyword);
        $url = "https://www.wikidata.org/w/api.php?action=query&list=search&srsearch={$encoded_keyword}&uselang=zh-tw&format=json";
        if ($debug) {
            echo '$keyword is: ' . print_r($keyword, true) . PHP_EOL;
            echo '$village_code is: ' . print_r($village_code, true) . PHP_EOL;
            echo '$url is: ' . print_r($url, true) . PHP_EOL;
        }

        $crawler_go = new CrawlerClass();
        $result = $crawler_go->downloadFile($url, $file_path, 'WikiData API 資料');
        if(is_null($result)){
            return null;
        }

        $db_go = new DbClass();
        $name = '下載 WikiData API 資料';


        if($result === false){
            $error = '未順利下載 WikiData API 資料！';
            $action = '檢查 WikiData API endpoint：' . $url;

            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'entity' => $keyword,
                'last_error' => $error,
                'action' => $action,

            );
            $db_go->saveLogging($log_data);
            //throw new Exception($error);
            return null;
        }

        $log_data = array(
            'short_name' => __FUNCTION__,
            'name' => $name,
            'entity' => $keyword,
            'last_error' => null,
            'action' => null,

        );
        //$db_go->saveLogging($log_data);


        // save to tb: wikidata_search
        if(is_string($result)){
            $json_data = json_decode($result, true);
        }else{
            $json_data = $result;
        }

        $wikidata_totalhits = null;
        if(is_array($json_data)
            && array_key_exists('query', $json_data)
            && array_key_exists('searchinfo', $json_data['query'])
            && array_key_exists('totalhits', $json_data['query']['searchinfo'])
        ){
            $wikidata_totalhits = $json_data['query']['searchinfo']['totalhits'];
        }
        $wikidata_result = $result;

        $insert_data = array(
            'keyword' => $keyword,
            'village_code' => $village_code,
            'wikidata_result' => $wikidata_result,
            'wikidata_totalhits' => $wikidata_totalhits,
        );
        $db_go->saveWikiDataResult($insert_data);

        $random_seconds = rand(1, 5);
        if ($debug) {
            echo 'sleep $random_seconds is: ' . print_r($random_seconds, true) . PHP_EOL;
        }
        sleep($random_seconds);

        return $json_data;
    }

    /**
     * @param $city_name
     * @param $village_name
     * @return void|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function crawlApiGivenVillageName($city_name, $village_name, $village_code){
        $debug = false;
        //$debug = $this->debug;
        $enforce_update_downloaded_file = $this->enforce_update_downloaded_file;

        $crawler_go = new CrawlerClass();
        $keyword_list = $crawler_go->getKeywordStrategy($city_name, $village_name);
        foreach ($keyword_list AS $keyword){
            if ($debug) {
                echo '$keyword is: ' . print_r($keyword, true) . PHP_EOL;
            }
            $result_from_crawl = 1;
            $crawl_result = $this->crawlApiGivenKeyword($keyword, $village_code);

            $file_content = null;
            if($enforce_update_downloaded_file === false){
                $file_path = $this->getFilePathOfWikiDataApiResultGivenKeyword($keyword);
                if(file_exists($file_path)){
                    $file_content = file_get_contents($file_path);
                    $result_from_crawl = 0;
                }
            }


            if(is_null($crawl_result)
                && !is_null($file_content)
            ){
                $crawl_result = json_decode($file_content, true);
            }

            if ($debug) {
                echo '$crawl_result or file_content is: ' . print_r($crawl_result, true) . PHP_EOL;
                echo '$result_from_crawl is: ' . print_r($result_from_crawl, true) . PHP_EOL;
            }

            // if totalhits = 1, stop to crawl other keywords
            if(is_array($crawl_result)
                && array_key_exists('query', $crawl_result)
                && array_key_exists('searchinfo', $crawl_result['query'])
                && array_key_exists('totalhits', $crawl_result['query']['searchinfo'])
                && $crawl_result['query']['searchinfo']['totalhits'] === 1
            ){
                return $crawl_result;
            }
        }

        return null;
    }

    /**
     * @return int|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function crawlApiGivenVillageNameList($requests = array()){
        $debug = false;
        //$debug = $this->debug;
        $wikidata_request_size = $this->wikidata_request_size;

        $db_go = new DbClass();
        $db_go->debug = $debug;

        if(!array_key_exists('size', $requests)){
            $requests['size'] = $wikidata_request_size;
        }

        $city_and_village_data = $db_go->getTownAndVillageNameListNeedToUpdate($requests);
        if ($debug) {
            echo '$city_and_village_data is: ' . print_r($city_and_village_data, true) . PHP_EOL;
        }

        if(empty($city_and_village_data)){
            return null;
        }

        if ($debug) {
            echo 'count of $city_and_village_data is: ' . print_r(count($city_and_village_data), true) . PHP_EOL;
        }

        foreach ($city_and_village_data AS $row){
            $city_name = $row['town_name'];
            $village_name = $row['village_name'];
            $village_code = $row['village_code'];
            $this->crawlApiGivenVillageName($city_name, $village_name, $village_code);
        }
        return count($city_and_village_data);
    }

    /**
     * @param string $json_content
     * @return array|null
     */
    public function parseWikiDataQueryResult($json_content = ''){
        $debug = $this->debug;

        $json_data = json_decode($json_content, true);
        if(!is_array($json_data)){
            return null;
        }

        if ($debug) {
            echo '$json_data is: ' . print_r($json_data, true) . PHP_EOL;
        }

        $output = array();
        foreach ($json_data['results']['bindings'] AS $row){

            $village_url = $row['village']['value'];

            $descriptions = null;
            if(array_key_exists('villageDescription', $row)){
                $descriptions = $row['villageDescription']['value'];
            }

            $town_wikidata_qid = null;
            $town_url = null;
            if(array_key_exists('town', $row)){
                $town_url = $row['town']['value'];
                $town_wikidata_qid = $this->getWikidataQidFromUrl($town_url);
            }

            $town_labels = null;
            if(array_key_exists('townLabel', $row)){
                $town_labels = $row['townLabel']['value'];
            }

            $town_descriptions = null;
            if(array_key_exists('townDescription', $row)){
                $town_descriptions = $row['townDescription']['value'];
            }

            $village_code = null;
            if(array_key_exists('village_code', $row)){
                $village_code = $row['village_code']['value'];
            }

            $coordinate = null;
            if(array_key_exists('coordinate', $row)){
                $coordinate = $row['coordinate']['value'];
            }

            $osm_id = null;
            if(array_key_exists('osm_id', $row)){
                $osm_id = $row['osm_id']['value'];
            }

            $output[] = array(
                'wikidata_id' => $this->getWikidataIdFromUrl($village_url),
                'url' => $village_url,
                'labels' => $row['villageLabel']['value'],
                'descriptions' => $descriptions,
                'town_wikidata_qid' => $town_wikidata_qid,
                'town_labels' => $town_labels,
                'town_descriptions' => $town_descriptions,
                'coordinate' => $coordinate,
                'village_code' => $village_code,
                'osm_id' => $osm_id,
                'is_village' => 1,
                'created_at' => date('Y-m-d H:i:s')
            );
        }

        return $output;
    }

    /**
     * @param $json_content
     * @param $label
     * @param $save_logging
     * @return array|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function parseApiIdsResult($json_content = '', $label = null, $save_logging = true){
        $debug = $this->debug;

        $json_data = json_decode($json_content, true);
        if(!is_array($json_data)){
            return null;
        }

        if ($debug) {
            echo '$json_data is: ' . print_r($json_data, true) . PHP_EOL;
        }

        if(!array_key_exists('entities', $json_data)){

            if($save_logging){
                $db_go = new DbClass();
                $name = '下載 WikiData API 資料';

                $error = '未順利解析 WikiData API 資料！';
                $action = '檢查 WikiData API endpoint：' . $label;

                $log_data = array(
                    'short_name' => __FUNCTION__,
                    'name' => $name,
                    'entity' => $label,
                    'last_error' => $error,
                    'action' => $action,

                );
                $db_go->saveLogging($log_data);
            }

            return null;
        }

        $output = array();
        foreach ($json_data['entities'] AS $row){

            $labels = null;
            if(array_key_exists('labels', $row)
                && array_key_exists('zh-tw', $row['labels'])
                && array_key_exists('value', $row['labels']['zh-tw'])
            ){
                $labels = $row['labels']['zh-tw']['value'];
            }

            $descriptions = null;
            if(array_key_exists('descriptions', $row)
                && array_key_exists('zh-tw', $row['descriptions'])
                && array_key_exists('value', $row['labels']['zh-tw'])
            ){
                $descriptions = $row['descriptions']['zh-tw']['value'];
            }

            $output[] = array(
                //'title' => $row['title'],
                'wikidata_id' => str_replace('Q', '', $row['title']),
                'url' => 'https://www.wikidata.org/entity/' . $row['title'],
                //'search_snippet' => $row['snippet'],
                'labels' => $labels,
                'descriptions' => $descriptions,
                'wikidata_updated_at' => date('Y-m-d H:i:s', strtotime($row['modified'])),
                'wikidata_updated_at_raw' => $row['modified'],
            );
        }

        return $output;

    }


    /**
     * @param string $json_content
     * @return array|false|string|null
     */
    public function parseWikiDataKeywordSearchResult($json_content = ''){
        $debug = $this->debug;

        $json_data = json_decode($json_content, true);
        if(!is_array($json_data)){
            return null;
        }

        if ($debug) {
            echo '$json_data is: ' . print_r($json_data, true) . PHP_EOL;
        }

        $output = array();
        foreach ($json_data['query']['search'] AS $row){
            $output[] = array(
                //'title' => $row['title'],
                'wikidata_id' => str_replace('Q', '', $row['title']),
                'url' => 'https://www.wikidata.org/entity/' . $row['title'],
                'search_snippet' => $row['snippet'],
                'wikidata_updated_at' => date('Y-m-d H:i:s', strtotime($row['timestamp'])),
                'wikidata_updated_at_raw' => $row['timestamp'],
            );
        }

        return $output;
    }

    /**
     * @param string $keyword
     * @return string
     */
    public function getFilePathOfWikiDataApiResultGivenKeyword($keyword = ''){
        $folder_path = $this->wikidata_folder_path_of_keyword_search;
        return  $folder_path . urlencode($keyword) . '.json';
    }

    /**
     * @param $ids
     * @return string
     */
    public function getFilePathOfWikiDataApiResultGivenIds($ids = array()){
        $folder_path = $this->wikidata_folder_path_of_ids;
        return  $folder_path . implode(',', $ids) . '.json';
    }


    /**
     * @param string $url
     * @return mixed|null
     */
    public function getWikidataIdFromUrl($url = ''){
        $pattern = "/\/entity\/Q(\d+)$/iu";
        preg_match($pattern, $url, $matches);
        if(isset($matches[1])){
            return $matches[1];
        }

        return null;
    }

    /**
     * @param $url
     * @return mixed|null
     */
    public function getWikidataQidFromUrl($url = ''){
        $pattern = "/\/entity\/(Q\d+)$/iu";
        preg_match($pattern, $url, $matches);
        if(isset($matches[1])){
            return $matches[1];
        }

        return null;
    }

}

