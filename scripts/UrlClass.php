<?php

namespace wikidata_tw;
class UrlClass
{
    /**
     * @param $baseUrl
     * @param $partialUrl
     * @return string
     */
    public function combineUrl($baseUrl = "", $partialUrl = "") {

        if (substr($baseUrl, -1) !== '/') {
            $baseUrl .= '/';
        }

        if (substr($partialUrl, 0, 1) === '/') {
            $partialUrl = substr($partialUrl, 1);
        }

        return $baseUrl . $partialUrl;
    }
}
