<?php

require_once __DIR__ . '/../load.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ExcelClass
{
    public $debug = false;


    /**
     * @param $file_path
     * @return void
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function getExcelContent($file_path = ""){
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load($file_path);
        return $spreadsheet->getActiveSheet()->toArray();
    }

}
