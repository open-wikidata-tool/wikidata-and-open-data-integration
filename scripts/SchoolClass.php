<?php

namespace wikidata_tw;
require_once __DIR__ . '/../load.php';

class SchoolClass
{
    public $debug = false;
    private $school_list_by_year_url = "https://depart.moe.edu.tw/ED4500/News.aspx?n=63F5AB3D02A8BBAC&sms=1FF9979D10DBF9F3";
    private $folder_path = __DIR__ . '/../files/depart.moe.edu.tw/';
    public $file_path_of_school_list_by_year = __DIR__ . '/../files/depart.moe.edu.tw/entrance.html';

    /**
     *
     */
    public function __construct() {

        if(!file_exists($this->folder_path)){
            mkdir($this->folder_path, 0777, true);
        }
    }

    /**
     * @param $fields
     * @param $values
     * @return array
     */
    public function combineFieldsAndValues($fields, $values){
        $output = array();
        foreach ($fields as $index => $field_name){
            if(trim($field_name) !== ""
                && array_key_exists($index, $values)
            ){
                $output[$field_name] = $values[$index];
            }
        }
        return $output;
    }

    /**
     * @return int
     */
    public function crawlSchoolListByYearPage(){
        $url = $this->school_list_by_year_url;
        $file_path = $this->file_path_of_school_list_by_year;

        $is_update = (new \CrawlerClass())->isOkToUpdate($file_path);
        if($is_update){
            if(file_exists($file_path)){
                rename($file_path, $file_path . '.bak');
            }

            (new \CrawlerClass())->crawlPage($url, $file_path);
            if(file_exists($file_path)){
                return 1;
            }
        }
        return 0;
    }

    /**
     * @return int
     */
    public function crawlSchoolListRecentYearPage(){
        $debug = $this->debug;

        $file_path = $this->file_path_of_school_list_by_year;
        if(file_exists($file_path)){
            $html_content = file_get_contents($file_path);
            $parsed_links_data = $this->parseSchoolListByYearPage($html_content);

            if(array_key_exists(0, $parsed_links_data)){
                $recent_link_data = $parsed_links_data[0];
                if ($debug) {
                    echo '$recent_link_data is: ' . print_r($recent_link_data, true) . PHP_EOL;
                }

                if(array_key_exists("href", $recent_link_data)
                    && is_string($recent_link_data["href"])
                ){
                    $url = $recent_link_data["href"];
                    $file_path = $this->folder_path . 'recent_school_list.html';

                    $is_update = (new \CrawlerClass())->isOkToUpdate($file_path);
                    if($is_update) {
                        if (file_exists($file_path)) {
                            rename($file_path, $file_path . '.bak');
                        }

                        (new \CrawlerClass())->crawlPage($url, $file_path);

                        if (file_exists($file_path)) {
                            return 1;
                        }
                    }

                }
            }
        }

        return 0;
    }

    /**
     * @param $row
     * @return array
     */
    public function filterFieldValues($row = array())
    {
        // Use array_filter with a custom callback to remove null elements
        $row_filtered = array_filter($row, function($value) {
            return !is_null($value);
        });

        // Use array_map with a custom callback to trim non-null elements
        $row_filtered = array_map(function($value) {
            return trim($value);
        }, $row_filtered);

        // remove empty elements
        $row_filtered = array_filter($row_filtered);

        return array_values($row_filtered);
    }


    /**
     * @param $excel_content
     * @return array
     */
    public function parseDiffExcelFile($excel_content = array()){
        $debug = $this->debug;

        // extract the field name list
        $field_index = $this->guessFieldIndex($excel_content);

        $fields = $excel_content[$field_index];
        $fields = array_map("trim", $fields);
        $fields = $this->removeReturnSymbolsFromElements($fields);
        if ($debug) {
            echo '$fields is: ' . print_r($fields, true) . PHP_EOL;
        }

        $output = array();

        $created_at = null;
        $updated_at = null;

        foreach ($excel_content as $index => $row){
            $row = array_map("trim", $row);

            $filtered_row = $this->filterFieldValues($row);
            if ($debug) {
                echo '## $row is: ' . print_r($row, true) . PHP_EOL;
                echo '$filtered_row is: ' . print_r($filtered_row, true) . PHP_EOL;
                echo 'count of $fields is: ' . count($fields) . PHP_EOL;
                echo 'count of $filtered_row is: ' . count($filtered_row) . PHP_EOL;
            }

            // get created_at (公告) & updated_at (更新)
            if(count($filtered_row) === 1
                && preg_match("/(公告|更新)$/iu", $filtered_row[0])
            ){
                if(preg_match("/(公告)$/iu", $filtered_row[0])){
                    $created_at = preg_replace("/(公告)$/iu", "", $filtered_row[0]);
                    $created_at = trim($created_at);
                    $created_at = (new \TimeClass())->convertDateFromTwEra($created_at);
                }

                if ($debug) {
                    echo '$created_at is: ' . print_r($created_at, true) . PHP_EOL;
                }

                if(preg_match("/(更新)$/iu", $filtered_row[0])){
                    $updated_at = preg_replace("/(更新)$/iu", "", $filtered_row[0]);
                    $updated_at = trim($updated_at);
                    $updated_at = (new \TimeClass())->convertDateFromTwEra($updated_at);
                }

                if ($debug) {
                    echo '$updated_at is: ' . print_r($updated_at, true) . PHP_EOL;
                }
            }

            // get status
            if(count($filtered_row) === 1
            ){
                $status = $row[0];
                $status = $this->postProcessOfStatusField($status);
            }

            if(count($row) === count($fields)
                && $index > $field_index
                && count($filtered_row) > 1
            ){

                // combine fields and values
                $associate_fields = $this->combineFieldsAndValues($fields, $row);
                $associate_fields["status"] = $status;
                $associate_fields["created_at"] = $created_at;
                $associate_fields["updated_at"] = $updated_at;
                if ($debug) {
                    echo '$associate_fields is: ' . print_r($associate_fields, true) . PHP_EOL;
                }

                $output[] = $associate_fields;
            }
        }
        return $output;
    }

    /**
     * @param $html_content
     * @return array|null
     * @throws Exception
     */
    public function parseSchoolListByYearPage($html_content = ""){

        if(!is_string($html_content)){
            throw new \InvalidArgumentException('html_content is empty or not string');
        }

        $xpath = '//tr/td/a[contains(@href, "News_Content")]';
        $elements = (new \CrawlerClass())->parseHtmlRawPage($html_content, $xpath);

        $output = array();
        foreach ($elements as $element){

            $single_line = $element;
            if(array_key_exists("href", $element)
                && is_string($element["href"])
            ){
                $url = $element["href"];
                $url = (new UrlClass())->combineUrl("https://depart.moe.edu.tw/ED4500/", $url);
                $single_line["href"] = $url;
            }

            $output[] = $single_line;
        }

        return $output;
    }

    public function postProcessOfStatusField($status = "")
    {
        // ※停辦 -> 停辦
        return preg_replace("/^(※)/iu", "", $status);
    }

    /**
     * @param $input
     * @return array|array[]|string[]|string[][]
     */
    public function removeReturnSymbolsFromElements($input = array()){
        return array_map(function($item) {
            return str_replace(array("\r\n", "\n", "\r"), "", $item);
        }, $input);
    }

    /**
     * @param $html_content
     * @param $label
     * @return array
     */
    public function getExcelFileUrlsFromHtmlPage($html_content = "", $label = null){

        $crawler_go = new \CrawlerClass();

        if(!is_string($html_content)){
            throw new \InvalidArgumentException('html_content is empty or not string');
        }

        $xpath = '//strong[contains(text(), "異動一覽表")]/parent::td/ol/li/a[contains(@href, "xls")]/@href';
        //$xpath = '//strong[contains(text(), "異動一覽表")]/parent::td/ol/li/a/@href';
        //$xpath = '//strong[contains(text(), "異動一覽表")]/parent::td/ol/li';
        $elements = $crawler_go->parseHtmlRawPageBase($html_content, $xpath);

        $output = array();
        foreach ($elements as $element){
            $output[] = $element["txt"];
        }
        return $output;
    }

    /**
     * @param $excel_content
     * @return mixed
     */
    public function getMaxCountOfFieldNumber($excel_content = array()){
        $output = array();
        foreach ($excel_content as $row){
            $row = array_map("trim", $row);
            $row = array_filter($row);
            $output[] = count($row);
        }
        return max($output);
    }

    /**
     * @param $excel_content
     * @return int|string|void
     */
    public function guessFieldIndex($excel_content = array()){
        $debug = $this->debug;

        $max_count_of_field_number = $this->getMaxCountOfFieldNumber($excel_content);
        if ($debug) {
            echo '$max_count_of_field_number is: ' . print_r($max_count_of_field_number, true) . PHP_EOL;
        }
        foreach ($excel_content as $index => $row){
            $row = array_map("trim", $row);
            $row = array_filter($row);

            // replace return symbols
            $row = $this->removeReturnSymbolsFromElements($row);

            if(count($row) === $max_count_of_field_number){
                return $index;
            }
        }
    }
}
