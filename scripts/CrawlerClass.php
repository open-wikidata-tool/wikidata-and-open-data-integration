<?php

require_once __DIR__ . '/../load.php';

class CrawlerClass
{
    public $debug = false;
    public $save_log = true;
    public $save_db = true;
    public $enforce_update_downloaded_file = false;
    public $target_tb = 'main_tb';
    private $created_by = 0;
    public $page_size = 50;

    private $update_interval = 7; // if the file was older than 7 day, allowed to download $entrance_url
    private $update_interval_in_hour = 1; // if the file was older than 1 hour, allowed to download $entrance_url


    // 連結介面戶役政資料代碼內容
    // https://www.ris.gov.tw/app/portal/164 -> click '戶役政資料代碼內容'
    private $entrance_url = 'https://www.ris.gov.tw/documents/html/5/1/167.html';

    public $base_folder_path = __DIR__ . '/../files/';
    public $entrance_file_path = __DIR__ . '/../files/entrance.html';
    public $code_added_by_month_folder_path = __DIR__ . '/../files/village_new_added/';
    public $code_deleted_by_month_folder_path = __DIR__ . '/../files/village_deleted/';

    // 連結介面戶役政資料代碼內容 -> 戶役政資訊系統資料代碼內容清單
    // https://www.ris.gov.tw/documents/html/5/1/168.html
    // 省市縣市鄉鎮市區代碼
    public $town_code_file_url = 'https://www.ris.gov.tw/documents/data/5/1/RSCD0103.txt';
    public $town_code_file_path = __DIR__ . '/../files/town_code.txt';

    // 村里代碼檔 110年6月
    //public $village_code_file_url = 'https://www.ris.gov.tw/documents/data/5/1/11006VillageUTF8.txt';
    public $village_code_file_path = __DIR__ . '/../files/village_code.txt';
    public $village_code_folder_path = __DIR__ . '/../files/village_code/';

    // 省市縣市鄉鎮市區代碼 + 村里代碼檔
    public $integrated_file_path = __DIR__ . '/../files/integrated.json';

    /**
     * @param $url
     * @param $file_path
     * @return int
     */
    public function crawlPage($url = "", $file_path = "")
    {
        $ch = curl_init();
        curl_setopt($ch , CURLOPT_URL , $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1);
        $result = curl_exec($ch);
        curl_close($ch);
        file_put_contents($file_path, $result);
        if(file_exists($file_path)){
            return 1;
        }
        return 0;
    }

    /**
     * @return bool|null
     * @throws Exception
     */
    public function downloadEntrancePage(){

        $save_log = $this->save_log;

        $url = $this->entrance_url;
        $file_path = $this->entrance_file_path;

        $db_go = new DbClass();
        $name = '下載「連結介面戶役政資料代碼內容」網頁';

        $result = $this->downloadFile($url, $file_path, '連結介面戶役政資料代碼內容');
        if(is_null($result)){
            return null;
        }

        if($result === false){
            $error = '未順利下載「連結介面戶役政資料代碼內容」網頁！';
            $action = '檢查「連結介面戶役政資料代碼內容」網址是否變動：' . $url;

            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => $error,
                'action' => $action,

            );

            if($save_log){
                $db_go->saveLogging($log_data);
            }

            throw new Exception($error);
        }

        if($save_log){
            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => null,
                'action' => null,

            );

            $db_go->saveLogging($log_data);
        }


        return $result;
    }

    /**
     * @return false
     * @throws Exception
     */
    public function downloadFile($url, $file_path, $label = null){

        $save_log = $this->save_log;
        $enforce_update_downloaded_file = $this->enforce_update_downloaded_file;

        $db_go = new DbClass();

        $folder_path = dirname($file_path);


        $name = "下載「{$label}」檔案";

        if(!is_writable($folder_path)){
            $error = "目錄無法寫入！";
            $action = "檢查目錄寫入權限" ;


            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => $error,
                'action' => $action,

            );

            if($save_log){
                $db_go->saveLogging($log_data);
            }

            return false;
        }

        if(trim($url) === ''){
            $error = "檔案網址是空值！";
            $action = "檢查網址" ;


            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => $error,
                'action' => $action,

            );

            if($save_log){
                $db_go->saveLogging($log_data);
            }

            return false;
            //throw new Exception($error);
        }

        if($enforce_update_downloaded_file === false){
            $is_ok_to_download_file = $this->isOkToUpdate($file_path);

            if($is_ok_to_download_file === false){
                return null;
            }
        }

        $db_go = new DbClass();
        $name = '下載 ' . $label;

        if(file_exists($file_path)){
            rename($file_path, $file_path . '.bak');
        }

        $file_handle = fopen($file_path, "w");

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch , CURLOPT_URL , $url);
        curl_setopt($ch, CURLOPT_FILE, $file_handle);
        //curl_setopt($ch, CURLOPT_VERBOSE, true);
        //curl_setopt($ch, CURLOPT_STDERR, fopen('php://stderr', 'w'));
        //curl_setopt($ch, CURLOPT_STDERR, fopen('error.log', 'w'));
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); // timeout is 30 seconds


        $result = curl_exec($ch);


        //file_put_contents($file_path, $result);

        // sleep 1 ~ 3 seconds
        $random_seconds = rand(1, 3);
        sleep($random_seconds);

        if($result === false){
            $error = "「{$label}」未順利下載檔案！";
            $error .= "curl 錯誤編號：" . curl_errno($ch) . "。";
            $error .= "curl 錯誤：" . curl_error($ch) . "。";
            $error .= "curl 狀態：" . print_r(curl_getinfo($ch), true) . "。";
            $action = "檢查「{$label}」網址是否變動：" . $url;


            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => $error,
                'action' => $action,

            );

            if($save_log){
                $db_go->saveLogging($log_data);
            }

            return false;
            //throw new Exception($error);
        }

        if($save_log){
            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => null,
                'action' => null,

            );
            $db_go->saveLogging($log_data);
        }

        curl_close($ch);
        fclose($file_handle);

        return $result;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function downloadFileTownCode(){

        $url = $this->town_code_file_url;
        $file_path = $this->town_code_file_path;

        $db_go = new DbClass();
        $name = '下載「省市縣市鄉鎮市區代碼」';

        $result = $this->downloadFile($url, $file_path, '省市縣市鄉鎮市區代碼');
        if(is_null($result)){
            return null;
        }

        if($result === false){
            $error = '「省市縣市鄉鎮市區代碼」未順利下載檔案！';
            $action = '檢查「省市縣市鄉鎮市區代碼」網址是否變動：' . $url;


            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => $error,
                'action' => $action,

            );
            $db_go->saveLogging($log_data);

            throw new Exception($error);
        }

        $log_data = array(
            'short_name' => __FUNCTION__,
            'name' => $name,
            'last_error' => null,
            'action' => null,

        );
        $db_go->saveLogging($log_data);

        return $result;
    }


    /**
     * @return int
     * @throws Exception
     */
    public function downloadFileVillageCode(){

        $entrance_file_path = $this->entrance_file_path;
        $save_log = $this->save_log;
        $base_folder_path = $this->base_folder_path;

        $this->downloadEntrancePage();

        $html_content = file_get_contents($entrance_file_path);
        $code_file_data = $this->getVillageCodeDataFromRawHtml($html_content);

        // 1/2 download file and save to files/village_code.txt
        $file_path = $base_folder_path . 'village_code.txt';
        if(array_key_exists('href', $code_file_data)){
            $url = $code_file_data['href'];
            $this->downloadFile($url, $file_path, '村里代碼檔');
        }

        // 2/2 download file and save to files/village_code/YYYY-MM-o1.txt
        $file_name = $code_file_data['date'] . '.txt';
        $new_file_path = $base_folder_path . 'village_code/' . $file_name;
        $is_ok_to_update = $this->isOkToUpdate($file_path);
        if(is_file($file_path)
            && $is_ok_to_update
        ){
            copy($file_path, $new_file_path);
        }


        $db_go = new DbClass();
        $name = "下載「新增村里代碼」檔案";

        if(!is_file($file_path)){
            $error = '下載「新增村里代碼」檔案，遇到問題. $code_file_data: ' . print_r($code_file_data, true);
            $action = '檢查「新增村里代碼」檔案網址';

            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => $error,
                'action' => $action,

            );

            if($save_log){
                $db_go->saveLogging($log_data);
            }

            throw new Exception($error);
        }

        if($save_log){
            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => null,
                'action' => null,

            );
            $db_go->saveLogging($log_data);
        }


        return true;
    }


    /**
     * @param $file_path
     * @param int $update_interval
     * @return bool
     */
    public function isOkToUpdate($file_path){
        //$update_interval = $this->update_interval;
        $update_interval_in_hour = $this->update_interval_in_hour;
        $update_interval_in_seconds = $update_interval_in_hour * 60 * 60;

        if (!file_exists($file_path)) {
            return true;
        }

        if(file_exists($file_path)
            && $update_interval_in_hour === 0
        ){
            return true;
        }

        if (file_exists($file_path)
            && filesize($file_path) === 0
        ){
            rename($file_path, $file_path . '.bak');
            return true;
        }

        $current_timestamp = time();
        clearstatcache();
        $file_time = filemtime($file_path);

        if($file_time < ($current_timestamp - $update_interval_in_seconds)){
            copy($file_path, $file_path . '.bak');
            return true;
        }

        return false;
    }


    /**
     * @return int
     * @throws \Simplon\Mysql\MysqlException
     */
    public function downloadVillageAddedCodeFiles(){
        $total_generated_files = $this->downloadVillageRevisedCodeFiles(true);
        return $total_generated_files;
    }

    /**
     * @return int
     * @throws \Simplon\Mysql\MysqlException
     */
    public function downloadVillageDeletedCodeFiles(){
        $total_generated_files = $this->downloadVillageRevisedCodeFiles(false);
        return $total_generated_files;
    }

    /**
     * @param bool $is_new_added
     * @return int
     * @throws \Simplon\Mysql\MysqlException
     */
    public function downloadVillageRevisedCodeFiles($is_new_added = true){

        if($is_new_added){
            $code_by_month_folder_path = $this->code_added_by_month_folder_path;
            $needle = "新增村里代碼"; // as the keyword of html content
            $xpath_query = '//div[@class = "panel-heading"]';
            $label = $needle; // the label of downloadFile for debug purpose
        }else{
            $code_by_month_folder_path = $this->code_deleted_by_month_folder_path;
            $needle = "刪除村里代碼";
            $xpath_query = '//div[@class = "panel-body"]';
            $label = $needle;
        }


        $entrance_file_path = $this->entrance_file_path;
        $html_content = file_get_contents($entrance_file_path);

        $new_added_village_data = $this->getVillageRevisedCodeDataFromRawHtml($html_content, $needle, $xpath_query);

        $errors = array();
        $total_generated_files = 0;
        foreach ($new_added_village_data as $row){
            $url = $row['href'];
            $date = $row['date'];
            $extension = pathinfo($url, PATHINFO_EXTENSION);
            $extension = strtolower($extension);
            $file_name = "{$date}.$extension";
            $file_path = $code_by_month_folder_path . $file_name;

            $is_ok_to_update = $this->isOkToUpdate($file_path);
            if($is_ok_to_update){
                $download_result = $this->downloadFile($url, $file_path, $label);
                if($download_result === false){
                    $errors[] = $date . ': ' . $url;
                }

                if($download_result
                    && file_exists($file_path)
                ){
                    $total_generated_files++;
                }
            }

        }

        $db_go = new DbClass();
        $name = "下載「{$label}」檔案";

        if(count($errors) > 0){
            $error = "下載「{$label}」檔案，遇到問題：" . implode(';', $errors);
            $action = "檢查「{$label}」檔案網址";

            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => $error,
                'action' => $action,

            );
            $db_go->saveLogging($log_data);

            throw new Exception($error);
        }

        $log_data = array(
            'short_name' => __FUNCTION__,
            'name' => $name,
            'last_error' => null,
            'action' => null,

        );
        $db_go->saveLogging($log_data);

        return $total_generated_files;
    }

    /**
     * @param $city_name
     * @param $village_name
     * @return array
     */
    public function getKeywordStrategy($city_name, $village_name){
        $output = array();

        // if contains 省
        if(preg_match("/(省)/u", $city_name)){
            $tmp_city_name = $this->replaceProvinceFromCityName($city_name);
            $output[] = "{$tmp_city_name}{$village_name}";
            $output[] = "\"{$tmp_city_name}{$village_name}\"";
        }

        // if contains 台 or 臺
        if(preg_match("/(臺)/u", $city_name)){
            $tmp_city_name = str_replace('臺', '台', $city_name);
            $output[] = "{$tmp_city_name}{$village_name}";
            $output[] = "\"{$tmp_city_name}{$village_name}\"";
        }

        if(preg_match("/(台)/u", $city_name)){
            $tmp_city_name = str_replace('台', '臺', $city_name);
            $output[] = "{$tmp_city_name}{$village_name}";
            $output[] = "\"{$tmp_city_name}{$village_name}\"";
        }

        $output[] = "\"{$city_name}{$village_name}\"";
        $output[] = "{$city_name}{$village_name}";
        $output[] = "{$village_name}";

        $output = array_unique($output);

        return $output;
    }




    /**
     * @param string $html_content
     * @return array
     * @throws Exception
     */
    public function getVillageRevisedCodeDataFromRawHtml($html_content = '', $needle = "新增村里代碼", $xpath_query = null){
        $save_log = $this->save_log;
        //$xpath_query = '//div[@class = "panel-heading"]';
        $parsed_elements = $this->parseHtmlRawPage($html_content, $xpath_query);
        // return 2 blocks: (1) 戶役政資訊系統資料代碼內容清單 (2) 新增村里代碼
        //var_dump($parsed_elements);

        $db_go = new DbClass();
        $name = "解析網頁「{$needle}」區塊";

        if(is_null($parsed_elements)){
            $error = '網頁結構變動，沒有找到「' . $needle . '」區塊 I。 $xpath_query: ' . $xpath_query;

            $action = "檢查「{$needle}」網頁結構變動";
            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => $error,
                'action' => $action,

            );

            if($save_log){
                $db_go->saveLogging($log_data);
            }

            throw new Exception($error);
        }

        // 新增村里代碼
        $is_page_contains_new_village = false;
        $html_part_content_contains_new_village = null;
        foreach ($parsed_elements as $row){
            $html_part_content = $row['html'];
            $pattern = "/({$needle})/iu";
            preg_match($pattern, $html_part_content, $matches);
            if(isset($matches[1])){
                $is_page_contains_new_village = true;
                $html_part_content_contains_new_village = $html_part_content;
            }
        }


        if($is_page_contains_new_village === false){
            $error = '網頁結構變動，沒有找到「' . $needle . '」區塊 II。';

            $action = '檢查網頁結構變動';
            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => $error,
                'action' => $action,

            );

            if($save_log){
                $db_go->saveLogging($log_data);
            }

            throw new Exception($error);
        }

        $xpath_query = '//a';
        $parsed_elements = $this->parseHtmlRawPage($html_part_content_contains_new_village, $xpath_query);

        if(is_null($parsed_elements)){
            $error = "「{$needle}」區塊內，找不到任何連結。";

            $action = '檢查網頁結構變動';
            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => $error,
                'action' => $action,

            );

            if($save_log){
                $db_go->saveLogging($log_data);
            }


            throw new Exception($error);
        }

        $output = array();
        foreach ($parsed_elements as $parsed_element){

            // 11006版 -> 2021-06-01
            $txt = $parsed_element['txt'];
            $date = (new \TimeClass())->convertDateFromTwEra($txt);
            $parsed_element['date'] = $date;

            // /documents/data/5/1/11006_Village_a.txt
            // ->
            // https://www.ris.gov.tw/documents/data/5/1/11006_Village_a.txt
            $href = $parsed_element['href'];
            if(trim($href) !== ''){
                $href = "https://www.ris.gov.tw" . $href;
            }
            $parsed_element['href'] = $href;
            $output[] = $parsed_element;
        }

        $log_data = array(
            'short_name' => __FUNCTION__,
            'name' => $name,
            'last_error' => null,
            'action' => null,

        );

        if($save_log){
            $db_go->saveLogging($log_data);
        }


        return $output;
    }

    /**
     * @param string $city_and_village_name
     * @return mixed|null
     */
    public function getAdministrativeData($city_and_village_name = ''){
        $debug = $this->debug;

        if ($debug) {
            echo '#### $city_and_village_name: : ' . print_r($city_and_village_name, true) . PHP_EOL;
        }

        $city_and_village_name  = $this->replaceProvinceFromCityName($city_and_village_name);
        if ($debug) {
            echo '$city_and_village_name after replaceProvinceFromCityName is: ' . print_r($city_and_village_name, true) . PHP_EOL;
        }

        // get city name
        $city_level1_name = $this->getCityLevel1Name($city_and_village_name);

        $city_level2_name = $this->getCityLevel2Name($city_and_village_name);
        if ($debug) {
            echo '$city_level2_name is: ' . print_r($city_level2_name, true) . PHP_EOL;
        }

        if(!is_null($city_level1_name)){
            $pattern = "/^(" . preg_quote($city_level1_name). ")/iu";
            $city_and_village_name = preg_replace($pattern, '', $city_and_village_name);
        }

        if(!is_null($city_level2_name)){
            $pattern = "/^(" . preg_quote($city_level2_name). ")/iu";
            $city_and_village_name = preg_replace($pattern, '', $city_and_village_name);
        }

        if ($debug) {
            echo '$city_and_village_name after replace $city_level1_name & $city_level2_name is: ' . print_r($city_and_village_name, true) . PHP_EOL;
        }

        return array(
            'level1' => $city_level1_name,
            'level2' => $city_level2_name,
            'level3' => $city_and_village_name
        );
    }

    /**
     * @param string $city_and_village_name
     * @return mixed|null
     */
    public function getCityLevel1Name($city_and_village_name = ''){
        $debug = $this->debug;
        $city_and_village_name  = $this->replaceProvinceFromCityName($city_and_village_name);
        if ($debug) {
            echo '$city_and_village_name after replaceProvinceFromCityName is: ' . print_r($city_and_village_name, true) . PHP_EOL;
        }

        // get city name
        $pattern = '/^([^縣|市]+[縣|市])/u';
        preg_match($pattern, $city_and_village_name, $matches);
        if ($debug) {
            echo '$matches is: ' . print_r($matches, true) . PHP_EOL;
        }

        if(isset($matches[1])){
            return $matches[1];
        }

        return null;
    }

    /**
     * @param string $city_and_village_name
     * @return mixed|null
     */
    public function getCityLevel2Name($city_and_village_name = ''){
        $debug = $this->debug;
        $city_and_village_name  = $this->replaceProvinceFromCityName($city_and_village_name);
        if ($debug) {
            echo '$city_and_village_name after replaceProvinceFromCityName is: ' . print_r($city_and_village_name, true) . PHP_EOL;
        }

        $city_level1_name = $this->getCityLevel1Name($city_and_village_name);
        if(!is_null($city_level1_name)){
            $pattern = "/^(" . preg_quote($city_level1_name). ")/iu";
            $city_and_village_name = preg_replace($pattern, '', $city_and_village_name);
        }

        if ($debug) {
            echo '$city_and_village_name after replace $city_level1_name is: ' . print_r($city_and_village_name, true) . PHP_EOL;
        }

        // get city name
        $pattern = '/^([^區|鄉|鎮|市]+[區|鄉|鎮|市])/u';
        preg_match($pattern, $city_and_village_name, $matches);
        if ($debug) {
            echo '$matches is: ' . print_r($matches, true) . PHP_EOL;
        }

        if(isset($matches[1])){
            return $matches[1];
        }

        return null;
    }

    /**
     * @param string $html_content
     * @return mixed|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function getVillageCodeDataFromRawHtml($html_content = ''){
        $debug = $this->debug;
        $save_log = $this->save_log;
        $xpath_query = '//div[@class = "panel-heading strong color-black"]/following-sibling::div[@class = "panel-body"]/div[@class = "panel panel-default"]';
        $parsed_elements = $this->parseHtmlRawPage($html_content, $xpath_query);
        // return 1 blocks

        if ($debug) {
            echo '$parsed_elements is: ' . print_r($parsed_elements, true) . PHP_EOL;
        }

        $db_go = new DbClass();
        $name = '解析網頁「村里代碼檔」的檔案';

        if(is_null($parsed_elements)){
            $error = '網頁結構變動，沒有找到「村里代碼檔」區塊 I。 $xpath_query: ' . $xpath_query;

            $action = '檢查網頁結構變動';
            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => $error,
                'action' => $action,

            );

            if($save_log){
                $db_go->saveLogging($log_data);
            }

            throw new Exception($error);
        }


        // check if contains 下載：村里代碼檔
        $is_page_contains_village_code = false;
        $html_part_content_contains_village_code = null;
        foreach ($parsed_elements as $row){
            $html_part_content = $row['html'];
            $pattern = "/(下載：村里代碼檔)/iu";
            preg_match($pattern, $html_part_content, $matches);
            if(isset($matches[1])){
                $is_page_contains_village_code = true;
                $html_part_content_contains_new_village = $html_part_content;
            }
        }


        if($is_page_contains_village_code === false){
            $error = '網頁結構變動 II。預期網頁文字區塊包含下載：村里代碼檔。';

            $action = '檢查網頁結構變動「村里代碼檔」';
            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => $error,
                'action' => $action,

            );

            if($save_log){
                $db_go->saveLogging($log_data);
            }

            throw new Exception($error);
        }

        $xpath_query = '//a';
        $parsed_elements = $this->parseHtmlRawPage($html_part_content_contains_new_village, $xpath_query);

        if(is_null($parsed_elements)){
            $error = '「村里代碼檔」區塊內，找不到任何連結。';

            $action = '檢查網頁結構變動「村里代碼檔」';
            $log_data = array(
                'short_name' => __FUNCTION__,
                'name' => $name,
                'last_error' => $error,
                'action' => $action,

            );

            if($save_log){
                $db_go->saveLogging($log_data);
            }

            throw new Exception($error);
        }

        foreach ($parsed_elements as $parsed_element){

            // 11006版 -> 2021-06-01
            $txt = $parsed_element['txt'];

            // /documents/data/5/1/11006VillageBIG5.txt
            // ->
            // https://www.ris.gov.tw/documents/data/5/1/11006VillageBIG5.txt
            $href = $parsed_element['href'];
            $date = (new \TimeClass())->convertDateFromTwEra($href);
            $parsed_element['date'] = $date;
            if(trim($href) !== ''){
                $href = "https://www.ris.gov.tw" . $href;
            }
            $parsed_element['href'] = $href;

            $is_collect = false;
            preg_match("/(UNI（UTF8）)/iu", $txt, $matches);
            if(isset($matches[1])){
                $is_collect = true;
            }

            if($is_collect){

                $log_data = array(
                    'short_name' => __FUNCTION__,
                    'name' => $name,
                    'last_error' => null,
                    'action' => null,

                );

                if($save_log){
                    $db_go->saveLogging($log_data);
                }

                return $parsed_element;
            }

        }

        $error = '「村里代碼檔」區塊內，找不到連結文字是 UNI（UTF8）的連結。';

        $action = '檢查網頁結構變動「村里代碼檔」';
        $log_data = array(
            'short_name' => __FUNCTION__,
            'name' => $name,
            'last_error' => $error,
            'action' => $action,

        );

        if($save_log){
            $db_go->saveLogging($log_data);
        }
        return null;
    }


    /**
     * @param $query_data
     * @param string $tb_name
     * @param string $unique_column_name
     * @param null $return_column
     * @return mixed|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function getVillageNameGivenVillageCode($village_code = ''){
        $target_tb = $this->target_tb;

        $dbh = (new DbClass())->getDbh();
        $query = "
            SELECT `village_name`
            FROM `{$target_tb}`
            WHERE `village_code` LIKE :village_code
        ";
        $condition = array('village_code' => $village_code);

        return $dbh->fetchColumn($query, $condition);
    }


    /**
     * @return array|null
     * @throws \Simplon\Mysql\MysqlException
     */
    public function getVillageNameListContainTownName(){
        $target_tb = $this->target_tb;
        $dbh = (new DbClass())->getDbh();
        $query = "
            SELECT *
            FROM `{$target_tb}`
            WHERE CHAR_LENGTH(`village_name`) > 4
        ";
        $condition = array();

        return $dbh->fetchRowMany($query, $condition);
    }


    /**
     * @param string $html_content
     * @return array|null
     * @throws Exception
     */
    public function parseHtmlRawPage($html_content = '', $xapth_query = ''){


        $elements = $this->parseHtmlRawPageBase($html_content, $xapth_query);
        if(empty($elements)){
            return null;
        }

        $output = array();

        foreach ($elements as $element){
            $partial_html_content = $element["html"];
            $xpath_of_link = "//a/@href";
            $link_elements = $this->parseHtmlRawPage($partial_html_content, $xpath_of_link);
            $link = null;
            if(!empty($link_elements)){
                $link = $link_elements[0]["txt"];
            }
            $element["href"] = $link;
            $output[] = $element;
        }

        return $output;
    }

    /**
     * @param $html_content
     * @param $xapth_query
     * @return array|null
     */
    public function parseHtmlRawPageBase($html_content = '', $xapth_query = ''){

        //suppresses errors generated by poorly-formed xml
        libxml_use_internal_errors(true);

        // Create a new DOMDocument
        $doc = new \DOMDocument();

        $findme = '<?xml encoding="utf-8" ?>';
        $pos = strpos($html_content, $findme);
        if ($pos === false) {
            $html_content = $findme . $html_content;
        }

        // Load the HTML file
        $doc->loadHTML($html_content);

        // Create a new DOMXPath instance
        $xpath = new \DOMXPath($doc);

        // Execute the query
        $entries = $xpath->query($xapth_query);

        $output = array();
        foreach ($entries as $entry) {

            $output[] = array(
                'txt' => trim($entry->nodeValue),
                'html' => $doc->saveHTML($entry),
            );
        }

        if(empty($output)){
            return null;
        }

        return $output;
    }

    /**
     * @param string $file_content
     * @return mixed
     * @throws Exception
     */
    public function parseTownCodeFile(){

        $reader_go = new ReaderClass();
        $town_code_file_path = $this->town_code_file_path;
        if(!file_exists($town_code_file_path)){
            $error = '「省市縣市鄉鎮市區代碼」未順利下載檔案！';
            throw new Exception($error);
        }

        $file_content = file_get_contents($town_code_file_path);
        $file_content = $reader_go->parseCsvFile($file_content);
        $output = array();
        foreach ($file_content as $row){
            $row = trim($row[0]);
            $row_data = explode('=', $row);

            $this->verifyParentCodeData($row_data);

            list($town_code, $town_name) = $row_data;
            $output[] = array(
                'town_code' => $town_code,
                'town_name' => $town_name,
            );
        }

        return $output;
    }

    /**
     * @return false|string[]
     * @throws Exception
     */
    public function parseVillageCodeFile(){

        $reader_go = new ReaderClass();

        $code_file_path = $this->village_code_file_path;
        if(!is_file($code_file_path)){
            $error = '「村里代碼檔」未順利下載檔案！';
            throw new Exception($error);
        }

        $file_content = file_get_contents($code_file_path);
        $csv_data = $reader_go->parseCsvFile($file_content);
        $created_at = date("Y-m-d H:i:s", filemtime($code_file_path));

        $output = array();
        foreach ($csv_data as $row){
            list($village_code, $town_code, $village_name) = $row;
            $this->verifyVillageCodeData($row);

            $output[] = array(
                'village_code' => $village_code,
                'town_code' => $town_code,
                'village_name' => $village_name,
                'created_at' => $created_at,
            );
        }

        return $output;
    }

    /**
     * @param string $file_path
     * @param string $time_column
     * @return array
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function parseVillageHistoryFile($file_path = '', $time_column = 'added_at', $file_name = null){
        $debug = $this->debug;
        $save_log = $this->save_log;

        $reader_go = new ReaderClass();
        $db_go = new DbClass();

        // file extension: txt, doc, docx
        if(is_null($file_name)){
            $file_name = basename($file_path);
        }

        if(!is_file($file_path)){
            $error = 'The $file_path is not file: ' . $file_path;
            throw new Exception($error);
        }

        $extension = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
        if ($debug) {
            echo '$extension is: ' . print_r($extension, true) . PHP_EOL;
        }

        $name = "解析「新增村里代碼」或「刪除村里代碼」檔案";
        switch ($extension) {
            case "txt":
                $file_content = $reader_go->getFileContentFromText($file_path);
                return $this->parseVillageHistoryFileContent($file_name, $file_content, $time_column);
                break;

            case "docx":
                $file_content = $reader_go->getFileContentFromDocx($file_path);
                return $this->parseVillageHistoryFileContent($file_name, $file_content, $time_column);
                break;

            default:
                $error = '「新增村里代碼」或「刪除村里代碼」檔案非預期的 TXT (txt) 或 Word (docx) 檔案，請聯繫管理員處理。';
                $error .= '檔案名稱：' . $file_name;

                $action = null;
                $log_data = array(
                    'short_name' => __FUNCTION__,
                    'name' => $name,
                    'last_error' => $error,
                    'action' => $action,

                );

                if($save_log){
                    $db_go->saveLogging($log_data);
                }


                throw new Exception($error);
        }
    }

    /**
     * @param string $folder_path
     * @param string $time_column
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * @throws \Simplon\Mysql\MysqlException
     */
    public function parseAndSaveVillageHistoryFolder($folder_path = '', $time_column = 'added_at')
    {
        //$debug = false;
        //$debug = true;
        $debug = $this->debug;
        $save_db = $this->save_db;

        $reader_go = new ReaderClass();

        $pattern = "/^.*\.(txt|docx)$/i";
        $file_list = $reader_go->getFileList($folder_path, $pattern);
        $total_affected_count = 0;
        foreach ($file_list as $file_path){
            if ($debug) {
                echo '$file_path is: ' . print_r($file_path, true) . PHP_EOL;
            }

            $parsed_data = $this->parseVillageHistoryFile($file_path, $time_column);
            if ($debug) {
                echo '$parsed_data is: ' . print_r($parsed_data, true) . PHP_EOL;
            }

            if($save_db){
                $label = basename($file_path);
                $affected_count = $this->saveVillageHistoryRecords($parsed_data, $time_column, $label);
                $total_affected_count += $affected_count;
                if ($debug) {
                    echo '$affected_count of saveVillageHistoryRecords is: ' . print_r($affected_count, true) . PHP_EOL;
                }
            }

        }

        return $total_affected_count;
    }

    /**
     * @param string $file_name
     * @param string $file_content
     * @param string $time_column
     * @return array
     */
    public function parseVillageHistoryFileContent($file_name = '', $file_content = '', $time_column = 'added_at'){
        $debug = $this->debug;

        $return_symbol_list = array("\n","\r","\r\n");
        $file_content = str_replace($return_symbol_list,PHP_EOL, $file_content);
        $file_data = explode(PHP_EOL, $file_content);
        if ($debug) {
            echo '$file_data is: ' . print_r($file_data, true) . PHP_EOL;
        }

        // get added_at from file_name
        $time_column_value = pathinfo($file_name, PATHINFO_FILENAME);

        $output = array();
        foreach ($file_data as $row){
            $row = $this->trimString($row);

            // check if the line content end with comma symbol
            $pattern = "/(,)$/iu";
            $row = preg_replace($pattern, '', $row);
            $row = $this->trimString($row);

            // check if the line content start with 10 numbers or more
            $pattern = "/^(\d{10,})/iu";
            preg_match($pattern, $row, $matches);
            if(isset($matches[1])){
                if ($debug) {
                    echo '$row cleaned is: ' . print_r($row, true) . PHP_EOL;
                }

                // Patterns e.g.
                // 10008130016,都達村
                // 10002010003,10002010,民生里
                $patterns = [
                    'with_separator' => "/^(\d{10,})[\s,\t]*([\x{4e00}-\x{9fa5}]+)$/iu",
                    'without_separator' => "/^(\d{10,})([\x{4e00}-\x{9fa5}]+)$/iu",
                    'number1_number2_and_hans_with_separator' => "/^(\d{10,})[\s,\t]*(\d{8})[\s,\t]*([\x{4e00}-\x{9fa5}]+)$/iu"
                ];

                // Performing matches
                $matches = [];
                foreach ($patterns as $key => $pattern) {
                    $matches[$key] = $this->performRegexMatch($pattern, $row, $debug);
                }

                // Check matches and add to output array
                if(isset($matches['with_separator'][1])
                    && isset($matches['with_separator'][2])
                ) {
                    $output[] = [
                        'village_code' => $matches['with_separator'][1],
                        'city_and_village_name' => $matches['with_separator'][2],
                        $time_column => $time_column_value
                    ];
                } elseif(isset($matches['without_separator'][1])
                    && isset($matches['without_separator'][2])
                ) {
                    $output[] = [
                        'village_code' => $matches['without_separator'][1],
                        'city_and_village_name' => $matches['without_separator'][2],
                        $time_column => $time_column_value
                    ];
                } elseif(isset($matches['number1_number2_and_hans_with_separator'][1])
                    && isset($matches['number1_number2_and_hans_with_separator'][3])
                ) {
                    $output[] = [
                        'village_code' => $matches['number1_number2_and_hans_with_separator'][1],
                        'city_and_village_name' => $matches['number1_number2_and_hans_with_separator'][3],
                        $time_column => $time_column_value
                    ];
                }
            }

        }

        if(empty($output)){
            return null;
        }

        return $output;
    }

    /**
     * @throws Exception
     */
    public function parseVillageAndTownCode(){
        $parsed_code_mapping = $this->parseTownCodeFile();
        $parsed_village_code = $this->parseVillageCodeFile();

        $output = array();
        foreach ($parsed_village_code as $row){
            $town_code = $row[1];
            $town_name = null;

            if(array_key_exists($town_code, $parsed_code_mapping)){
                $town_name = $parsed_code_mapping[$town_code];
            }
            $output[] = array(
                'town_code' => $town_code,
                'town_name' => $town_name,
                'village_code' => $row[0],
                'village_name' => $row[2],
            );
        }

        return $output;
    }

    /**
     * @param $pattern
     * @param $subject
     * @param $debug
     * @return string[]
     */
    public function performRegexMatch($pattern = "", $subject = "", $debug = false) {
        preg_match($pattern, $subject, $matches);
        if ($debug) {
            echo '$matches for pattern ' . $pattern . ' is: ' . print_r($matches, true) . PHP_EOL;
        }
        return $matches;
    }


    /**
     * @param $table_data
     * @param $_GET
     * @return string
     */
    public function printTable($table_data = array(), $request_parameters = array()){
        // print heading
        $first_row = $table_data[0];
        $field_list = array_keys($first_row);

        $osm_go = new OSMClass();

        $ignore_field_lists = array('town_code', 'wikidata_id', 'last_error', 'search_snippet', 'url');
        foreach ($ignore_field_lists as $ignore_field){
            $index = array_search($ignore_field, $field_list);
            if($index !== false){
                unset($field_list[$index]);
            }
        }

        $field_list = array_values($field_list);

        $header_part = array();
        $header_part[] = <<<EOT

<th scope="col">#</th>

EOT;
        foreach ($field_list as $field_name){
            $header_part[] = <<<EOT

<th scope='col'><div class='sortable both' data-sort='{$field_name}'>{$field_name}</></th>

EOT;
        }
        $header_part = implode(PHP_EOL, $header_part);

        $content_part = array();

        $size = $this->page_size;
        $start_number = 1;
        $page_number = 1;
        if(array_key_exists('size', $request_parameters)){
            $size = $request_parameters['size'];
            $size = (int) $size;
        }


        /*
        if(array_key_exists('offset', $request_parameters)){
            $start_number = $request_parameters['offset'] + 1;
        }
        */

        if(array_key_exists('page_number', $request_parameters)){
            $page_number = $request_parameters['page_number'];
            $page_number = (int) $page_number;
        }

        if($page_number === 0){
            $page_number = 1;
        }

        $start_number = ($page_number - 1) * $size + 1;

        foreach ($table_data as $index => $row){
            $content_part[] = "<tr>";

            //$number_index = $index+1;
            $number_index = $start_number + $index;
            $content_part[] = "<th scope=\"row\">{$number_index}</th>";
            foreach ($field_list as $field_name){

                if(in_array($field_name, array('wikidata_id_candidate', '維基數據ID'))){

                    $value = $row[$field_name];
                    if(is_null($value)){
                        $content_part[] = "<td></td>";
                    }else{
                        $tooltip = $row['search_snippet'];
                        $tooltip = "前往 WikiData:" . strip_tags($tooltip);
                        $wikidata_id = "Q" . $row[$field_name];
                        //$url = "https://www.wikidata.org/wiki/{$wikidata_id}";
                        $url = $row['url'];
                        $content_part[] = "<td title=\"{$tooltip}\"><a href='{$url}' target='_blank'>". $wikidata_id . "</a></td>";
                    }

                }elseif($field_name === '經緯度'){

                    $value = $row[$field_name];
                    if(is_null($value)){
                        $content_part[] = "<td></td>";
                    }else{
                        $tooltip = '前往 OpenStreetMap';
                        $wikidata_id = "Q" . $row[$field_name];
                        //$url = "https://www.wikidata.org/wiki/{$wikidata_id}";
                        $url = $osm_go->convertUrlFromPoint($value);;
                        if(is_null($url)){
                            $content_part[] = "<td></td>";
                        }else{
                            $content_part[] = "<td title=\"{$tooltip}\"><a href='{$url}' target='_blank'>". $value . "</a></td>";
                        }

                    }

                }elseif($field_name === '開放街圖ID'){

                    $value = $row[$field_name];
                    if(is_null($value)){
                        $content_part[] = "<td></td>";
                    }else{
                        $tooltip = '前往 OpenStreetMap';

                        //$url = "https://www.openstreetmap.org/relation/4012321";
                        $url = $osm_go->convertUrlFromId($value);;
                        if(is_null($url)){
                            $content_part[] = "<td></td>";
                        }else{
                            $content_part[] = "<td title=\"{$tooltip}\"><a href='{$url}' target='_blank'>". $value . "</a></td>";
                        }

                    }

                }else{
                    $content_part[] = "<td>". $row[$field_name] . "</td>";
                }


            }
            $content_part[] = "</tr>";
        }
        $content_part = implode(PHP_EOL, $content_part);

        $output = <<<EOT
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                {$header_part}
            </tr>
        </thead>
        <tbody>
            {$content_part}
        </tbody>
    </table>
</div>

EOT;

        return $output;
    }


    /**
     * @param $request_parameters
     * @param $size_default
     * @param $total_size
     * @return string
     */
    public function printPageNumbers($request_parameters = null, $size_default = 50, $total_size = 0){
        // p1: offset=0
        // if size = 10
        //   p2: offset=10

        $current_page_number = 1;
        $offset_default = 0;
        if(isset($request_parameters)
            && array_key_exists('page_number', $request_parameters)
        ){
            $current_page_number = $request_parameters['page_number'];
            $current_page_number = (int) $current_page_number;
            $offset_default = ($current_page_number - 1) * $size_default;
        }


        $start_page_number = 1;
        $last_page_number = ceil($total_size / $size_default);

        /*
        $current_page_number = 1;
        if($offset_default > 0){
            $current_page_number = $offset_default / $size_default;
            $current_page_number ++;
        }*/

        //echo '$current_page_number: ' . $current_page_number . PHP_EOL;

        if($current_page_number > 10){
            $start_page_number = floor($current_page_number / 10) * 10;
            $start_page_number = $start_page_number + 1;
        }

        $end_page_number = $start_page_number + 9;
        if($end_page_number >= $last_page_number){
            $end_page_number = $last_page_number;
        }

        $pagination_html = array();
        $previous_page_number = $current_page_number - 1;
        if($previous_page_number <= 0){
            $previous_page_number = $last_page_number;
        }
        if($offset_default > $size_default){
            $previous_page_offset = $offset_default - $size_default;
        }else{
            $previous_page_offset = $last_page_number * $size_default - $size_default;
        }
        $pagination_html[] = "<a href='#' data-page='{$previous_page_number}' class='pagination'>&laquo;</a>";

        //echo '$start_page_number: ' . $start_page_number . PHP_EOL;
        //echo '$end_page_number: ' . $end_page_number . PHP_EOL;

        if($end_page_number > 0){
            $page_number_list = range($start_page_number, $end_page_number);
        }else{
            $page_number_list = array($start_page_number);
        }

        foreach ($page_number_list as $page_number){
            $page_number = (int) $page_number;
            $page_offset = ($size_default * $page_number) - $size_default;
            if($current_page_number === $page_number){
                $pagination_html[] = "<a class='active' href='#' data-page='{$current_page_number}' class='pagination'>{$page_number}</a>";
            }else{
                $pagination_html[] = "<a href='#' data-page='{$page_number}' class='pagination'>{$page_number}</a>";
            }

        }

        $next_page_number = $current_page_number + 1;
        if($next_page_number > $end_page_number){
            $next_page_number = 1;
            $next_page_offset = 0;
        }else{
            $next_page_offset = $offset_default + $size_default;
        }
        $pagination_html[] = "<a href='#' data-page='{$next_page_number}' class='pagination'>&raquo;</a>";

        $pagination_html = implode(PHP_EOL, $pagination_html);
        return $pagination_html;
    }



    /**
     * @throws \Simplon\Mysql\MysqlException
     */
    public function updateVillageName($village_code = '', $before_value = null, $after_value = null){
        $debug = $this->debug;
        $target_tb = $this->target_tb;
        $created_by = $this->created_by;

        if(is_null($before_value)){
            return null;
        }

        $db_go = new DbClass();
        $dbh = $db_go->getDbh();

        $conds = [
            'village_code' => $village_code,
            'village_name' => $before_value,
        ];

        $insert_data = [
            'village_name' => $after_value
        ];

        $result = $dbh->update($target_tb, $conds, $insert_data);
        if ($debug) {
            echo '$result of update is: ' . print_r($result, true) . PHP_EOL;
            echo 'data type of update is: ' . gettype($result) . PHP_EOL;
        }

        if($result){
            $insert_data = array(
                'village_code' => $village_code,
                'column_name' => 'village_name',
                'before' => $before_value,
                'after' => $after_value,
                'created_by' => $created_by,
            );
            $result_of_save_audit_log = $db_go->saveAuditLog($insert_data);
            if ($debug) {
                echo '$result_of_save_audit_log is: ' . print_r($result_of_save_audit_log, true) . PHP_EOL;
            }

            return 1;
        }

        return 0;
    }

    /**
     * @throws \Simplon\Mysql\MysqlException
     */
    public function updateVillageNamesIfContainTownName(){
        $debug = $this->debug;

        $query_result = $this->getVillageNameListContainTownName();
        if ($debug) {
            echo '$query_result is: ' . print_r($query_result, true) . PHP_EOL;
        }

        $total_affected_count = 0;
        foreach ($query_result as $row){
            $town_name = $row['town_name'];
            $village_code = $row['village_code'];
            $village_name = $row['village_name'];
            $new_village_name = $this->replaceProvinceAndTownFromVillageName($town_name, $village_name);
            if($new_village_name !== $village_name){
                if ($debug) {
                    echo '$village_name: ' . "$village_name -> $new_village_name" . PHP_EOL;
                }

                $total_affected_count += $this->updateVillageName($village_code, $village_name, $new_village_name);
            }
        }

        return $total_affected_count;
    }

    /**
     * @param string $town_name
     * @param string $replacement
     * @return array|string|string[]|null
     */
    public function replaceProvinceFromCityName($town_name = '', $replacement = ''){
        //return preg_replace("/^[\x{4e00}-\x{9fa5}]+省/iu", $replacement, $city_name);
        return preg_replace("/^[福建|臺灣]+省/iu", $replacement, $town_name);
    }

    /**
     * @param string $input
     * @param string $replacement
     * @return array|mixed|string|string[]|null
     */
    public function replaceAdministrativeName($input = '', $replacement = ''){
        $given_patterns = array();
        $given_patterns[] = "/(省)$/iu";
        $given_patterns[] = "/(區)$/iu";
        $given_patterns[] = "/(鄉)$/iu";
        $given_patterns[] = "/(鎮)$/iu";

        $given_patterns[] = "/(縣)$/iu";
        $given_patterns[] = "/(市)$/iu";

        $given_patterns[] = "/(村)$/iu";
        $given_patterns[] = "/(里)$/iu";

        foreach ($given_patterns as $pattern){
            $output = preg_replace($pattern, $replacement, $input);
        }

        return $output;
    }

    /**
     * @param string $village_name
     * @param string $town_name
     * @param string $replacement
     * @return mixed|null
     */
    public function replaceProvinceAndTownFromVillageName($town_name = '', $village_name = '', $replacement = ''){
        $debug = false;
        //$debug = $this->debug;
        $town_name_without_province = preg_replace("/^[福建|臺灣]+省/iu", $replacement, $town_name);

        $needle_list = array();
        $needle_list[] = $town_name;
        $needle_list[] = $town_name_without_province;

        // if contains 台 or 臺
        if(preg_match("/^(臺)/u", $town_name_without_province)){
            $needle_list[] = str_replace('臺', '台', $town_name_without_province);
        }

        if(preg_match("/^(台)/u", $town_name_without_province)){
            $needle_list[] = str_replace('台', '臺', $town_name_without_province);
        }
        $needle_list = array_unique($needle_list);

        if ($debug) {
            echo '$needle_list is: ' . print_r($needle_list, true) . PHP_EOL;
        }

        $output = array();
        foreach ($needle_list as $needle){
            $pattern = "/^(" . preg_quote($needle, '/'). ")/iu";
            $output[] = preg_replace($pattern, $replacement, $village_name);
        }

        $output = array_unique($output);
        if ($debug) {
            echo '$output is: ' . print_r($output, true) . PHP_EOL;
        }

        $length_list = array();
        $length_mapping = array();
        foreach ($output as $index => $row){
            $length = mb_strlen($row, 'UTF-8');
            $length_list[] = $length;
            $length_mapping[$length] = $row;
        }

        $min_length = min($length_list);
        if ($debug) {
            echo '$min_length is: ' . print_r($min_length, true) . PHP_EOL;
            echo '$length_mapping is: ' . print_r($length_mapping, true) . PHP_EOL;
        }

        return $length_mapping[$min_length];
    }

    /**
     * @param $input
     * @return string
     */
    public function trimString($input = ""){
        return trim($input, " \t\n\r\0\x0B\xC2\xA0\xE3\x80\x80");
    }

    /**
     * @param array $row_data
     * @return bool
     * @throws Exception
     */
    public function verifyVillageCodeData($row_data = array()){
        list($village_code, $town_code, $village_name) = $row_data;

        $column_list = array(
            1 => $village_code,
            2 => $town_code,
        );

        foreach ($column_list as $column_index => $column_value){
            $pattern = "/^(\d+)$/";
            preg_match($pattern, $village_code, $matches);
            if(!isset($matches[1])){
                $error = "預期第 {$column_index} 欄是數字，請檢查檔案格式是否變動！";
                throw new Exception($error);
            }
        }

        $column_index = 3;
        preg_match('/([\x{4e00}-\x{9fa5}]+)/u', $village_name, $matches);
        if(!isset($matches[1])){
            $error = "預期第 {$column_index} 欄至少包含一個中文字，請檢查檔案格式是否變動！";
            throw new Exception($error);
        }

        return true;
    }



    /**
     * @return bool
     * @throws Exception
     */
    public function saveVillageAndParentCode(){
        $code_file_path = $this->integrated_file_path;

        $db_go = new DbClass();
        $parsed_data = $db_go->getMainTableData();

        //var_dump($parsed_data);

        $file_content = json_encode($parsed_data, JSON_UNESCAPED_UNICODE);

        $is_ok_to_update = $this->isOkToUpdate($code_file_path);
        if($is_ok_to_update === false){
            return null;
        }


        if(file_exists($code_file_path)){
            rename($code_file_path, $code_file_path . '.bak');
        }

        file_put_contents($code_file_path, $file_content);
        if(!file_exists($code_file_path)){
            $error = '儲存「村里代碼檔+省市縣市鄉鎮市區代碼」發生問題！';
            throw new Exception($error);
        }

        return true;
    }


    /**
     * @param array $records
     * @param string $time_column
     * @return int|void
     * @throws \Simplon\Mysql\MysqlException
     */
    public function saveVillageHistoryRecords($records = array(), $time_column = 'added_at', $label = null){
        $debug = $this->debug;

        $db_go = new DbClass();

        if(is_null($records)){
            return 0;
        }

        if(empty($records)){
            return 0;
        }

        $total_affected_count = 0;
        foreach ($records as $record){
            if ($debug) {
                echo '$record is: ' . print_r($record, true) . PHP_EOL;
            }

            $total_affected_count += $db_go->saveVillageHistoryRecord($record, $time_column, $label);
        }

        return $total_affected_count;
    }

    public function saveWikidataIdCandidateByVillageCode($village_code = ''){
        if(trim($village_code) === ''){
            return 0;
        }


    }

    /**
     * @param array $row_data
     * @return bool
     * @throws Exception
     */
    public function verifyParentCodeData($row_data = array()){
        list($town_code, $town_name) = $row_data;

        $column_index = 1;
        $pattern = "/^([0-9|a-z]+)$/i";
        preg_match($pattern, $town_code, $matches);
        if(!isset($matches[1])){
            $error = "預期第 {$column_index} 欄是數字或英文組合，請檢查檔案格式是否變動！";
            throw new Exception($error);
        }


        $column_index = 2;
        preg_match('/([\x{4e00}-\x{9fa5}]+)/u', $town_name, $matches);
        if(!isset($matches[1])){
            $error = "預期第 {$column_index} 欄至少包含一個中文字，請檢查檔案格式是否變動！";
            throw new Exception($error);
        }

        return true;
    }
}

