<?php

require_once __DIR__ . '/load.php';

$debug = false;
$db_go = new DbClass();
$crawler_go = new CrawlerClass();

$wikidata_go = new WikidataClass();
$wikidata_go->debug = $debug;
//$crawler_go->debug = true;

echo 'start to downloadFileTownCode' . PHP_EOL;
$crawler_go->downloadFileTownCode();

echo 'start to downloadFileVillageCode' . PHP_EOL;
$crawler_go->downloadFileVillageCode();

echo 'start to downloadEntrancePage' . PHP_EOL;
$crawler_go->downloadEntrancePage();

echo 'start to downloadVillageAddedCodeFiles' . PHP_EOL;
$total_generated_files = $crawler_go->downloadVillageAddedCodeFiles();
echo '$total_generated_files of downloadVillageAddedCodeFiles: ' . print_r($total_generated_files, true) . PHP_EOL;

echo 'start to downloadVillageDeletedCodeFiles' . PHP_EOL;
$total_generated_files = $crawler_go->downloadVillageDeletedCodeFiles();
echo '$total_generated_files of downloadVillageDeletedCodeFiles: ' . print_r($total_generated_files, true) . PHP_EOL;

$total_affected_count = $db_go->saveTownCodeFromFile();
echo '$total_affected_count of saveTownCodeFromFile: ' . print_r($total_affected_count, true) . PHP_EOL;

$total_affected_count = $db_go->saveVillageCodeFromFile();
echo '$total_affected_count of saveVillageCodeFromFile: ' . print_r($total_affected_count, true) . PHP_EOL;

$total_affected_count = $db_go->saveTownCodeAndTownName();
echo '$total_affected_count of saveTownCodeAndTownName: ' . print_r($total_affected_count, true) . PHP_EOL;

$total_affected_count = $db_go->saveTownCodeFromVillageCode();
echo '$total_affected_count of saveTownCodeFromVillageCode: ' . $total_affected_count . PHP_EOL;

$result = $crawler_go->saveVillageAndParentCode();
echo '$result of saveVillageAndParentCode: ' . print_r($result, true) . PHP_EOL;

$folder_path = __DIR__ . '/files/village_new_added/';
$time_column = 'added_at';
$total_affected_count = $crawler_go->parseAndSaveVillageHistoryFolder($folder_path, $time_column);
echo '$total_affected_count of parseAndSaveVillageHistoryFolder (added): ' . print_r($result, true) . PHP_EOL;

$folder_path = __DIR__ . '/files/village_deleted/';
$time_column = 'deleted_at';
$total_affected_count = $crawler_go->parseAndSaveVillageHistoryFolder($folder_path, $time_column);
echo '$total_affected_count of parseAndSaveVillageHistoryFolder (deleted): ' . $total_affected_count . PHP_EOL;

$total_affected_count = $crawler_go->updateVillageNamesIfContainTownName();
echo '$total_affected_count of updateVillageNamesIfContainTownName: ' . $total_affected_count . PHP_EOL;

$total_affected_count = $wikidata_go->crawlAndSaveQueryResult();
echo '$total_affected_count of crawlAndSaveQueryResult: ' . $total_affected_count . PHP_EOL;


$requests = array('size' => 25);
$count = $wikidata_go->crawlApiGivenVillageNameList($requests);
echo '$count of crawlApiGivenVillageNameList: ' . print_r($count, true) . PHP_EOL;


//$requests = array('size' => 25);
$requests = array('size' => 0);
$total_affected_count = $db_go->saveWikidataCandidateEntriesData($requests);
echo '$total_affected_count of saveWikidataCandidateEntriesData: ' . print_r($total_affected_count, true) . PHP_EOL;

$ids = $db_go->getWikidataIds();
$total_affected_count = $wikidata_go->crawlApiGivenIds($ids);
echo '$total_affected_count of crawlApiGivenIds: ' . print_r($total_affected_count, true) . PHP_EOL;
